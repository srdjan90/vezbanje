package onedFullStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ODFS_ReviewProductsPage extends BasePage {


    @FindBy(how = How.XPATH, using = "//select[@id='review_overallRating']")
    private WebElement ddlHowWouldYouRateThisProduct;

    @FindBy(how = How.XPATH, using = "//textarea[@id='review_comments']")
    private WebElement txtTypeYourReview;

    @FindBy(how = How.XPATH, using = "//input[@id='review_firstName']")
    private WebElement txtDisplayName;

    @FindBy(how = How.XPATH, using = "//input[@id='review_city']")
    private WebElement txtCity;

    @FindBy(how = How.XPATH, using = "//input[@id='review_state']")
    private WebElement txtState;

    @FindBy(how = How.XPATH, using = "//input[@id='product_review_send']")
    private WebElement btnSubmitReview;


    public ODFS_ReviewProductsPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    /**
     *  METHODS
     */

    public ODFS_ReviewProductsPage choseProductRate(String text){
        chooseVisibleTextFromSelect(ddlHowWouldYouRateThisProduct, text);
        return this;
    }

    public ODFS_ReviewProductsPage enterProductReview(String text){
        typeIn(txtTypeYourReview, text);
        return this;
    }

    public ODFS_ReviewProductsPage enterDisplayName(String name){
        typeIn(txtDisplayName, name);
        return this;
    }

    public ODFS_ReviewProductsPage enterCity(String city){
        typeIn(txtCity, city);
        return this;
    }

    public ODFS_ReviewProductsPage enterState(String state){
        typeIn(txtState, state);
        return this;
    }

    public ODFS_ReviewsPage clickSubmitReview(){
        clickWhenReady(btnSubmitReview);
        return new ODFS_ReviewsPage(driver, wait);
    }
}
