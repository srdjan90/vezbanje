package onedFullStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class ODFS_CustomizePage extends BasePage {

//    @FindBy(css = "[onclick='CartAdd();']") private WebElement btnAddToCart;
//    @FindBy(css = "span[data-group-id='78']") private List<WebElement> lstItemColors;
//    @FindBy(css = "span[data-option-value='imprintcolor:yes']") private List<WebElement> lstImprintMethods;
//    @FindBy(css = ".rectangle-content [title='']:not([data-group-id='78'])") private List<WebElement> lstImprintColors;


//    Module methods

//    private ODFS_CustomizePage chooseItemColor(int colorOrdinal) {
//        clickWhenReady(lstItemColors.get(colorOrdinal));
//        return this;
//    }
//
//    private ODFS_CustomizePage chooseImprintMethod(int imprintMethodOrdinal) {
//        clickWhenReady(lstImprintMethods.get(imprintMethodOrdinal));
//        return this;
//    }
//
//    private ODFS_CustomizePage chooseImprintColor(int imprintColorOrdinal) {
//        clickWhenReady(lstImprintColors.get(imprintColorOrdinal));
//        return this;
//    }
//
//    //     Main methods
//    public ODFS_CartInterstitialPage chooseNecessaryOptionsProceedToCart(int colorOrdinal, int imprintMethodOrdinal, int imprintColorOrdinal) {
//        chooseItemColor(colorOrdinal).chooseImprintMethod(imprintMethodOrdinal)
//                                     .chooseImprintColor(imprintColorOrdinal);
//        clickWhenReady(btnAddToCart);
//        return new ODFS_CartInterstitialPage(driver,wait);
//    }
//
//    public ODFS_CartInterstitialPage proceedToCart() {
//        clickWhenReady(btnAddToCart);
//        return new ODFS_CartInterstitialPage(driver, wait);
//    }

    @FindBy(how = How.XPATH, using = "//input[@id='quantity']")
    private WebElement txtQuantity;

    @FindBy(how = How.XPATH, using = "//input[@id='comments_129_5125']")
    private WebElement txtSmall;

    @FindBy(how = How.XPATH, using = "//input[@id='comments_129_5126']")
    private WebElement txtMedium;

    @FindBy(how = How.XPATH, using = "//input[@id='comments_129_5127']")
    private WebElement txtLarge;

    @FindBy(how = How.XPATH, using = "//input[@id='comments_129_5128']")
    private WebElement txtXLarge;

    @FindBy(how = How.XPATH, using = "//input[@id='comments_129_5129']")
    private WebElement txt2XLarge;

    @FindAll(@FindBy(how = How.XPATH, using = "//div[@class='row group__78']/div[contains(@class,'col')]"))
    private List<WebElement> listColors;

    @FindBy(how = How.XPATH, using = "//textarea[@id='comments_78']")
    private WebElement txtItemColorComments;

    @FindAll(@FindBy(how = How.XPATH, using = "//div[@class='row product-child']/div[not (contains(@class,'visible'))]"))
    private List<WebElement> listDecorationMethod;

    @FindAll(@FindBy(how = How.XPATH, using = "//div[@class='row group__114']/div[contains(@class,'col')]"))
    private List<WebElement> listImprintLocation;

    @FindBy(how = How.XPATH, using = "//textarea[@id='comments' and @class='w100']")
    private WebElement txtArtworkComments;

    @FindBy(how = How.XPATH, using = "//textarea[@id='comments_114']")
    private WebElement txtImprintLocationComments;

    @FindAll(@FindBy(how = How.XPATH, using = "//div[@class='row group__13']/div[contains(@class,'col')]"))
    private List<WebElement> listImprintColor;

    @FindBy(how = How.XPATH, using = "//button[@class='btn btn-primary btn-lg w100']")
    private WebElement btnAddToCart;

    @FindBy(how = How.XPATH, using = "//button[@data-bb-handler='ok']")
    private WebElement btnModalOk;

    public ODFS_CustomizePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public ODFS_CustomizePage enterQuantity(int quantity){
        waitForSpinners();
        typeIn(txtQuantity, String.valueOf(quantity));
        waitForModals();
        clickWhenReady(btnModalOk);
        return this;
    }

    public ODFS_CustomizePage enterSmallQuantity(int smallQuantity){
        waitForSpinners();
        typeIn(txtSmall, String.valueOf(smallQuantity));
        return this;
    }

    public ODFS_CustomizePage enterMediumQuantity(int mediumQuantity){
        waitForSpinners();
        typeIn(txtMedium, String.valueOf(mediumQuantity));
        return this;
    }

    public ODFS_CustomizePage enterLargeQuantity(int largeQuantity){
        waitForSpinners();
        typeIn(txtLarge, String.valueOf(largeQuantity));
        return this;
    }

    public ODFS_CustomizePage enterXLargeQuantity(int xLargeQuantity){
        waitForSpinners();
        typeIn(txtXLarge, String.valueOf(xLargeQuantity));
        return this;
    }

    public ODFS_CustomizePage enter2XLargeQuantity(int XXLargeQuantity){
        waitForSpinners();
        typeIn(txt2XLarge, String.valueOf(XXLargeQuantity));
        return this;
    }

    public ODFS_CustomizePage choseItemColor(String colorName){
        waitForSpinners();
        for(WebElement w : listColors){
            if(getTextFromElement(w).contains(colorName)){
                clickWhenReady(w);
                break;
            }
        }
        return this;
    }

    public ODFS_CustomizePage enterItemColorComments(String comment){
        waitForSpinners();
        typeIn(txtItemColorComments, comment);
        return this;
    }

    public ODFS_CustomizePage choseDecorationMethod(String methodName){
        waitForSpinners();
        for(WebElement w : listDecorationMethod){
            if(getTextFromElement(w).contains(methodName)){
                clickWhenReady(w);
                break;
            }
        }
        return this;
    }

    public ODFS_CustomizePage enterArtworkComments(String comment){
        waitForSpinners();
        typeIn(txtArtworkComments, comment);
        return this;
    }

    public ODFS_CustomizePage choseImprintLocation(String imprintLocation){
        waitForSpinners();
        for(WebElement w : listImprintLocation){
            if(getTextFromElement(w).contains(imprintLocation)){
                clickWhenReady(w);
                break;
            }
        }
        return this;
    }

    public ODFS_CustomizePage enterImprintLocationComment(String comment){
        waitForSpinners();
        typeIn(txtImprintLocationComments, comment);
        return this;
    }

    public ODFS_CustomizePage choseImprintColor(String colorName){
        waitForSpinners();
        for(WebElement w : listImprintColor){
            if(getTextFromElement(w).contains(colorName)){
                clickWhenReady(w);
                break;
            }
        }
        return this;
    }

    public ODFS_CartInterstitialPage clickAddToCart(){
        waitForSpinners();
        clickWhenReady(btnAddToCart);
        return new ODFS_CartInterstitialPage(driver, wait);
    }

}
