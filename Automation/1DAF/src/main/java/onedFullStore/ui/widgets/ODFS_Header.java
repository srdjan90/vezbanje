/*
 * Created by ekarliuchenko on 10/15/18 4:29 PM
 */

package onedFullStore.ui.widgets;

import logic.BasePage;
import onedFullStore.ui.pages.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.testng.AssertJUnit.assertEquals;

public class ODFS_Header extends BasePage {

    //region Search & Other
    @FindBy(css = "#nav-logo") private WebElement imgNavLogo;
    @FindBy(css = "#ihsinput") private WebElement txtSearch;
    @FindBy(css = "#btnSearch") private WebElement btnSearch;
    @FindBy(css = "li.navbar-brand strong") private WebElement lblCallNumber;
    //endregion
    //region Cart
    @FindBy(css = "#nav_header span.glyphicon-shopping-cart") private WebElement ddCart;
    @FindBy(css = "#nav_header a[href='/products/custom/cart/']") private WebElement linkCart;
    @FindBy(css = "#nav_header a[href='/products/custom/quote/']") private WebElement linkQuotes;
    @FindBy(css = "#nav_header a[href='/products/custom/samples/']") private WebElement linkSamples;
    @FindBy(css = "#nav_header .cartcount") private WebElement cntrCart;
    @FindBy(css = "#nav_header .quotecount") private WebElement cntrQuote;
    @FindBy(css = "#nav_header .samplecount") private WebElement cntrSample;
    //endregion
    //region MyAccount
    @FindBy(css = "#nav_header span.glyphicon-user") private WebElement ddMyAccount;
    @FindBy(css = "#nav_header a[href*='/myaccount/signin']") private WebElement linkSignIn;
    @FindBy(css = "#nav_header a[href='/products/custom/secure/myaccount/']") private WebElement linkMyAccount;
    @FindBy(css = "#nav_header a[href='/products/custom/secure/wishlist/']") private WebElement linkMyWishLists;
    @FindBy(css = "#nav_header a[href='/products/custom/compare/']") private WebElement linkMyComparisons;
    @FindBy(css = "#nav_header a[href='/products/custom/secure/myaccount/signout.cshtml']") private WebElement linkSignOut;
    //endregion
    //region Products
    @FindBy(css = "#nav_header a[href='/products/custom/promotional-products/']") private WebElement linkProducts;
    @FindBy(css = "#nav_header h4 a") private List<WebElement> linksCategories;
    @FindBy(css = "#nav_header .container-fluid :not(h4) > a:not(.lgrey)") private List<WebElement> linksSubCategories;
    //endregion

    public ODFS_Header(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public enum Category {
        APPAREL, BAGS, DRINKWARE, ELECTRONICS, CORPORATE, GIFTS, WRITING,
        AUTOMOTIVE, BANNER_MAT_SIGN, BUTTON_STICKER_PATCH, CLOCK_WATCH, FANS, FLASHLIGHT_TOOL,
        FOOD_BEVERAGE, GOLF_PRODUCTS, HOUSEHOLD_ITEMS, JOURNALS, KEYCHAINS, LANYARD_BADGE,
        MAGNETS, NOTEPADS, OFFICE, OUTDOOR, PARTY_GAMES, SCHOOL_SPIRIT,
        STRESS_BALLS, TRAVEL_HEALTH, RETAIL_PACKAGING, WHOLESALE
    }

    public ODFS_HomePage clickLogo() {
        clickWhenReady(imgNavLogo);
        return new ODFS_HomePage(driver, wait);
    }

    public ODFS_ProductListingPage searchFor(String searchValue) {
        typeIn(txtSearch, searchValue);
        clickWhenReady(btnSearch);
        return new ODFS_ProductListingPage(driver, wait);
    }

    private ODFS_Header hoverOverProducts() {
        waitForVisible(linkProducts);
        hoverOver(linkProducts);
        return this;
    }

    public ODFS_ProductListingPage openCategory(String categoryName) {
        hoverOverProducts().
                clickWhenReady(waitForVisible(linksCategories).stream().
                        filter(webElement -> webElement.getText().trim().
                                equalsIgnoreCase(categoryName.toUpperCase())).findFirst().get());
        return new ODFS_ProductListingPage(driver, wait);
    }

    public ODFS_ProductListingPage openCategory(Category category) {
        hoverOverProducts().
                clickWhenReady(waitForVisible(linksCategories).get(category.ordinal()));
        return new ODFS_ProductListingPage(driver, wait);
    }

    public ODFS_ProductListingPage openSubCategory(String subCategoryName) {
        hoverOverProducts().
                clickWhenReady(waitForVisible(linksSubCategories).stream().
                        filter(webElement -> webElement.getText().contains(subCategoryName)).findFirst().get());
        return new ODFS_ProductListingPage(driver, wait);
    }

    private ODFS_Header hoverOverCart() {
        waitForVisible(ddCart);
        hoverOver(ddCart);
        return this;
    }

    public ODFS_CartPage openCart() {
        hoverOverCart().clickWhenReady(linkCart);
        return new ODFS_CartPage(driver, wait);
    }

    public ODFS_QuotesCartPage openQuotes() {
        hoverOverCart().clickWhenReady(linkQuotes);
        return new ODFS_QuotesCartPage(driver, wait);
    }

    public ODFS_SamplesCartPage openSamples() {
        hoverOverCart().clickWhenReady(linkSamples);
        return new ODFS_SamplesCartPage(driver, wait);
    }

    public int getCartCount() {
        return Integer.parseInt(cntrCart.getText());
    }

    public int getQuotesCount() {
        return Integer.parseInt(cntrQuote.getText());
    }

    public int getSamplesCount() {
        return Integer.parseInt(cntrSample.getText());
    }

    public ODFS_Header verifyCartCount(int expectedAmount) {
        assertEquals("Cart items amount doesn't match!", getCartCount(), expectedAmount);
        return this;
    }

    public ODFS_Header verifyQuotesCount(int expectedAmount) {
        assertEquals("Quotes amount doesn't match!", getQuotesCount(), expectedAmount);
        return this;
    }

    public ODFS_Header verifySamplesCount(int expectedAmount) {
        assertEquals("Samples amount doesn't match!", getSamplesCount(), expectedAmount);
        return this;
    }

    private ODFS_Header hoverOverMyAccount() {
        waitForVisible(ddMyAccount);
        hoverOver(ddMyAccount);
        return this;
    }

    public ODFS_SignInPage openSignIn() {
        hoverOverMyAccount().clickWhenReady(linkSignIn);
        return new ODFS_SignInPage(driver, wait);
    }

    public ODFS_MyAccountPage openMyAccount() {
        hoverOverMyAccount().clickWhenReady(linkMyAccount);
        return new ODFS_MyAccountPage(driver, wait);
    }

    public ODFS_WishlistPage openMyWishList() {
        hoverOverMyAccount().clickWhenReady(linkMyWishLists);
        return new ODFS_WishlistPage(driver, wait);
    }

    public ODFS_ComparisonsPage openMyComparisons() {
        hoverOverMyAccount().clickWhenReady(linkMyComparisons);
        return new ODFS_ComparisonsPage(driver, wait);
    }

    public ODFS_SignOutPage signOut() {
        hoverOverMyAccount().clickWhenReady(linkSignOut);
        return new ODFS_SignOutPage(driver, wait);
    }

    public String getCallNumber(){
        return waitForVisible(lblCallNumber).getText();
    }
}
