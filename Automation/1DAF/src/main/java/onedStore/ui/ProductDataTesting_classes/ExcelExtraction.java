package onedStore.ui.ProductDataTesting_classes;

import businessObjects.Product;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

public class ExcelExtraction {

    static File src = new File (".//src//main//resources//test_data//BB phase 1 data file.xlsx");
    static FileInputStream fis;
    static XSSFWorkbook wb;
    static XSSFSheet sheetName;

    /**
     * Product data lists
     */

    static ArrayList<Products> products;
    static ArrayList<ProductOptions> productOptions;
    static ArrayList<Prices> prices;
    static ArrayList<ComplexProduct> complexProducts;

    /**
     * Complex product list
     */

    static ArrayList<ProductOptions> complexProductOptions;
    static ArrayList<Prices> complexProductPrices;

    private static String getCellValue(XSSFCell cell){
        String value = "";

        if(cell == null || cell.getCellType() == CellType.BLANK || cell.getCellType() == CellType.ERROR || cell.getCellType() == CellType.FORMULA){
            value = null;
        } else {
            if(cell != null && cell.getCellType() == CellType.STRING){
                value = cell.getStringCellValue();
            }

            else if(cell != null && cell.getCellType() == CellType.NUMERIC){
                value = String.valueOf(cell.getNumericCellValue());

                if(value.contains(".")){
                    String[] split = value.split("\\.");
                    String afterDot = split[1];
                    boolean isDouble = false;

                    for(int i = 0; i < afterDot.length(); i++){
                        if(afterDot.charAt(i) != '0'){
                            isDouble = true;
                        }
                    }

                    if(!isDouble){
                        value = split[0];
                    }
                }
            }

            else if(cell != null && cell.getCellType() == CellType.BOOLEAN){
                value = String.valueOf(cell.getBooleanCellValue());
            }
        }

        return value;
    }

    public static ArrayList<Products> extractProducts () throws Exception {

        fis = new FileInputStream(src);
        wb = new XSSFWorkbook(fis);
        sheetName = wb.getSheetAt(0);
        int rowCount = sheetName.getLastRowNum();

        HashMap<String, Products> mapa = new HashMap<>();

        String productId, productName, productNumber, productWeight;

        for (int i = 1; i < rowCount + 1; i++) {
            productId = getCellValue(sheetName.getRow(i).getCell(0));
            productName = getCellValue(sheetName.getRow(i).getCell(1));
            productNumber = getCellValue(sheetName.getRow(i).getCell(2));
            productWeight = getCellValue(sheetName.getRow(i).getCell(3));

            Products product = new Products(productId, productName, productNumber, productWeight);
            if (!mapa.containsKey(productId)) {
                mapa.put(productId, product);
            }
        }

        products = new ArrayList<>();
        products.addAll(mapa.values());

        wb.close();
        fis.close();

        return products;
    }

    public static ArrayList<ProductOptions> extractProductOptions () throws Exception {

        fis = new FileInputStream(src);
        wb = new XSSFWorkbook(fis);
        sheetName = wb.getSheetAt(1);
        int rowCount = sheetName.getLastRowNum();

        productOptions = new ArrayList<>();

        String productId, productName, productNumber, groupName, optionId, optionName;

        for (int i = 1; i < rowCount + 1; i++) {
            productId = getCellValue(sheetName.getRow(i).getCell(0));
            productName = getCellValue(sheetName.getRow(i).getCell(1));
            productNumber = getCellValue(sheetName.getRow(i).getCell(2));
            groupName = getCellValue(sheetName.getRow(i).getCell(3));
            optionId = getCellValue(sheetName.getRow(i).getCell(4));
            optionName = getCellValue(sheetName.getRow(i).getCell(5));

            ProductOptions option = new ProductOptions(productId, productName, productNumber, groupName, optionId, optionName);
            productOptions.add(option);
        }

        wb.close();
        fis.close();

        return productOptions;
    }

    public static ArrayList<Prices> extractPrices () throws Exception {

        fis = new FileInputStream(src);
        wb = new XSSFWorkbook(fis);
        sheetName = wb.getSheetAt(2);
        int rowCount = sheetName.getLastRowNum();

        prices = new ArrayList<>();

        String productId, productName, productNumber, retail, lowRange, highRange;

        for (int i = 1; i < rowCount + 1; i++) {
            productId = getCellValue(sheetName.getRow(i).getCell(0));
            productName = getCellValue(sheetName.getRow(i).getCell(1));
            productNumber = getCellValue(sheetName.getRow(i).getCell(2));
            retail = getCellValue(sheetName.getRow(i).getCell(3));
            lowRange = getCellValue(sheetName.getRow(i).getCell(4));
            highRange = getCellValue(sheetName.getRow(i).getCell(5));

            Prices price = new Prices(productId, productName, productNumber, retail, lowRange, highRange);
            prices.add(price);
        }

        wb.close();
        fis.close();

        return prices;
    }

    public static ArrayList<ComplexProduct> getProducts () throws Exception {

        products = extractProducts();
        productOptions = extractProductOptions();
        prices = extractPrices();

        complexProducts = new ArrayList<>();

        for (Products p: products) {

            // Select related productOptions

            complexProductOptions = new ArrayList<>();

            for (ProductOptions prodOpt: productOptions) {
                if (prodOpt.getProductId().equals(p.getProductId())) {
                    complexProductOptions.add(prodOpt);
                }
            }

            // Select related prices

            complexProductPrices = new ArrayList<>();

            for (Prices price : prices) {
                if (price.getProductId().equals(p.getProductId())) {
                    complexProductPrices.add(price);
                }
            }

            ComplexProduct product = new ComplexProduct(p, complexProductOptions, complexProductPrices);
            complexProducts.add(product);

        }

        return complexProducts;
    }

    public static Iterator<ComplexProduct> getProductIterator () throws Exception {
        ArrayList<ComplexProduct> product = getProducts();
        return product.iterator();
    }

    public  static  void UzmiProizvode () throws Exception {

        fis = new FileInputStream(src);
        wb = new XSSFWorkbook(fis);
        sheetName = wb.getSheetAt(0);
        int rowCount = sheetName.getLastRowNum();

        for (int i = 1; i < rowCount + 1; i++) {
            System.out.println(getCellValue(sheetName.getRow(i).getCell(0)));
        }

        wb.close();
        fis.close();

    }

}
