package onedFullStore_tests.ui;

import logic.BaseTest;
import logic.Browser;
import logic.Environments;
import onedFullStore.ui.pages.ODFS_HomePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.Utils;

public class ODFS_SMOKE_LogIn extends BaseTest {

    private WebDriver driver;
    private WebDriverWait wait;
    private ODFS_HomePage homePage;

    @BeforeClass(alwaysRun = true)
    public void setUp(){
        driver = Browser.PROPERTY.getInstance();
        wait = Utils.initWaits(driver, 5, 10, 1);
        driver.get(Environments.ODFS_UI_URL);
        homePage = new ODFS_HomePage(driver, wait);
    }

    @Test(groups = {"smoke"})
    public void logIn(){
        homePage.header.openSignIn()
                .loginWithEmptyFields()
                .loginWithInvalidEmail("stefan-1910@srb.hostopia.com", "Deluxe@123")
                .loginWithInvalidPassword("stefan-191018@srb.hostopia.com", "Deluxe")
                .login("stefan-191018@srb.hostopia.com", "Deluxe@123");

    }

    @AfterClass(alwaysRun = true)
    public void tearDown(){
        driver.quit();
    }
}
