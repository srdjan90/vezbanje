/*
 * Created by ekarliuchenko on 10/16/18 9:41 PM
 */

package onedFullStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ODFS_ShippingAddressPage extends BasePage {

    @FindBy(css = "#addressLabel") private WebElement txtAddressLabel;
    @FindBy(css = "#countryID") private WebElement ddCountry;
    @FindBy(css = "#firstName") private WebElement txtFirstName;
    @FindBy(css = "#lastName") private WebElement txtLastName;
    @FindBy(css = "#company") private WebElement txtCompany;
    @FindBy(css = "#address1") private WebElement txtAddress1;
    @FindBy(css = "#address2") private WebElement txtAddress2;
    @FindBy(css = "#address3") private WebElement txtAddress3;
    @FindBy(css = "#city") private WebElement txtCity;
    @FindBy(css = "#stateID") private WebElement ddState;
    @FindBy(css = "#stateInternational") private WebElement txtState;
    @FindBy(css = "#zipCode") private WebElement txtZip;
    @FindBy(css = "#isDefaultAddress") private WebElement chbMakeDefaultAddress;
    @FindBy(css = "input[type='submit']") private WebElement btnSaveChanges;
    @FindBy(css = "input[type='submit'] ~ a") private WebElement btnCancel;
    @FindBy(css = ".modal-footer .btn-success") private WebElement btnBackToYourAddress;


    public ODFS_ShippingAddressPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public ODFS_ShippingAddressPage enterAddressLabel(String addressLabel) {
        typeIn(txtAddressLabel, addressLabel);
        return this;
    }

    public ODFS_ShippingAddressPage selectCountry(String country) {
        selectByText(ddCountry, country);
        return this;
    }

    public ODFS_ShippingAddressPage enterFirstName(String fristName) {
        typeIn(txtFirstName, fristName);
        return this;
    }

    public ODFS_ShippingAddressPage enterLastName(String lastName) {
        typeIn(txtLastName, lastName);
        return this;
    }

    public ODFS_ShippingAddressPage enterCompany(String company) {
        typeIn(txtCompany, company);
        return this;
    }

    public ODFS_ShippingAddressPage enterAddress1(String address1) {
        typeIn(txtAddress1, address1);
        return this;
    }

    public ODFS_ShippingAddressPage enterAddress2(String address2) {
        typeIn(txtAddress2, address2);
        return this;
    }

    public ODFS_ShippingAddressPage enterAddress3(String address3) {
        typeIn(txtAddress3, address3);
        return this;
    }

    public ODFS_ShippingAddressPage enterCity(String city) {
        typeIn(txtCity, city);
        return this;
    }

    public ODFS_ShippingAddressPage enterState(String state) {
        typeIn(txtState, state);
        return this;
    }

    public ODFS_ShippingAddressPage selectState(String state) {
        selectByText(ddState, state);
        return this;
    }

    public ODFS_ShippingAddressPage enterZip(String zip) {
        typeIn(txtZip, zip);
        return this;
    }

    public ODFS_ShippingAddressPage checkMakeDefailAddress() {
        if (!waitForVisible(chbMakeDefaultAddress).isSelected()) {
            chbMakeDefaultAddress.click();
        }
        return this;
    }

    public ODFS_ShippingAddressPage saveChanges(){
        clickWhenReady(btnSaveChanges);
        return this;
    }

    public ODFS_ManageShippingAddressPage backToYourAddresses(){
        clickWhenReady(btnBackToYourAddress);
        return new ODFS_ManageShippingAddressPage(driver, wait);
    }

    public ODFS_ManageShippingAddressPage cancel(){
        clickWhenReady(btnCancel);
        return new ODFS_ManageShippingAddressPage(driver, wait);
    }
}
