/*
 * Created by ekarliuchenko on 9/27/18 4:45 PM
 */

package onedStore.ui.widgets;

import logic.BasePage;
import onedStore.ui.pages.ODS_HomePage;
import onedStore.ui.pages.ODS_ProductsListingPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static utils.Consts.EMPTY_STRING;

public class ODS_Breadcrumb extends BasePage {

    @FindBy(css = ".breadcrumb a:not([title='Remove Filter'])") private List<WebElement> linksBreadCrumb;
    @FindBy(xpath = "//ol[@class='breadcrumb']//span[contains(@class, 'glyphicon-remove-sign')]/..") private WebElement linkRemoveFilter;

    public ODS_Breadcrumb(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    private enum BreadCrumb{
        ROOT, MAIN_CATEGORY, SUB_ITEM_1, SUB_ITEM_2, SUB_ITEM_3
    }

    public ODS_HomePage openRoot(){
        clickWhenReady(linksBreadCrumb.get(BreadCrumb.ROOT.ordinal()));
        return new ODS_HomePage(driver, wait);
    }

    public ODS_ProductsListingPage openParent(){
        if(!linksBreadCrumb.isEmpty() && linksBreadCrumb.size() > 2)
            clickWhenReady(linksBreadCrumb.get(linksBreadCrumb.size() - 2));
        else if (linksBreadCrumb.size() == 2) openRoot();
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage removeFilter(){
        clickWhenReady(linkRemoveFilter);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public String getBreadCrumbPath(){
        String fullPath = EMPTY_STRING;
        for(WebElement link : linksBreadCrumb){
            fullPath += link.getText() + "/";
        }
        return fullPath;
    }

}
