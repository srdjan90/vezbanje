package utils;

import org.openqa.selenium.edge.EdgeDriverService;
import org.openqa.selenium.firefox.GeckoDriverService;
import org.openqa.selenium.ie.InternetExplorerDriverService;

public final class Consts {
    private Consts() {
    }

    public static final String USER_DIR = System.getProperty("user.dir");

    public static final boolean PASSES = true;
    public static final boolean FAILS = false;

    public static final boolean SUCCESS = true;
    public static final boolean FAILURE = false;

    public static final String EMPTY_STRING = "";
    public static final String SPACE = " ";
    public static final String TAB = "\t";

    public static final String ALPHA_STRING_UPPERCASE = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    public static final String ALPHA_STRING_LOWERCASE = ALPHA_STRING_UPPERCASE.toLowerCase();
    public static final String NUMERIC_STRING = "0123456789";
    public static final String ALPHANUMERIC_STRING = ALPHA_STRING_UPPERCASE + ALPHA_STRING_LOWERCASE + NUMERIC_STRING;

}
