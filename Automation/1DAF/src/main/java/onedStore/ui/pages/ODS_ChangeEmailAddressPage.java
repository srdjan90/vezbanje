/***************************************************************************************************
 Created by vskyba on 10/9/18 2:21 PM
 **************************************************************************************************/

package onedStore.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;


import logic.BasePage;

import static utils.Utils.clickBySikuli;

public class ODS_ChangeEmailAddressPage extends BasePage {

    @FindBy(css = "#newEmail") private WebElement txtNewEmail;
    @FindBy(css = "#existingPassword") private WebElement txtCurrentPassword;
    @FindBy(css = "button[type='submit']") private WebElement btnSaveChanges;
    @FindBy(css = "a[href='/store/secure/myaccount/']") private WebElement btnCancel;
    @FindBy(css = "button[data-bb-handler='success']") private WebElement btnBackToMyAccount;
    @FindBy(css = "button[data-bb-handler='success'] ~ button") private WebElement btnClosePopUp;

    public ODS_ChangeEmailAddressPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    private ODS_ChangeEmailAddressPage typeNewEmail(String newEmail) {
        typeIn(txtNewEmail, newEmail);
        return this;
    }

    private ODS_ChangeEmailAddressPage typePassword(String currentPassword) {
        typeIn(txtCurrentPassword, currentPassword);
        return this;
    }

    private ODS_ChangeEmailAddressPage clickSaveChanges() {
        clickWhenReady(btnSaveChanges);
        return this;
    }

    private ODS_MyAccountPage clickCancel() {
        clickWhenReady(btnCancel);
        return new ODS_MyAccountPage (driver, wait);
    }

    private ODS_MyAccountPage clickBackToMyAccount() {
        clickWhenReady(btnBackToMyAccount);
        return new ODS_MyAccountPage(driver, wait);
    }

    private ODS_ChangeEmailAddressPage clickCaptcha() {
        clickBySikuli("captchaBox.png");
        return this;
    }

    public ODS_MyAccountPage changeAccountEmail(String newEmail, String currentPassword) {
        return typeNewEmail(newEmail).typePassword(currentPassword)
                                     .clickCaptcha()
                                     .clickSaveChanges()
                                     .clickBackToMyAccount();
    }
}
