package onedFullStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ODFS_ManageShippingAddressPage extends BasePage {

    @FindBy(css = "a[href='new/']") private WebElement btnNewAddress;
    @FindBy(css = "a[href='new/'] ~ a") private WebElement btnCancel;
    @FindBy(css = "a[onclick*='DeleteAddress']") private List<WebElement> linksDeleteAddress;
    @FindBy(css = ".alert-success") private WebElement msgbAlertSuccess;
    @FindBy(css = "a[onclick*='DefaultAddress']") private WebElement btnMakeDefault;

    public ODFS_ManageShippingAddressPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public ODFS_ShippingAddressPage newAddress() {
        clickWhenReady(btnNewAddress);
        return new ODFS_ShippingAddressPage(driver, wait);
    }

    public ODFS_MyAccountPage cancel() {
        clickWhenReady(btnCancel);
        return new ODFS_MyAccountPage(driver, wait);
    }

    public ODFS_ManageShippingAddressPage deleteAllAddresses() {
        if(!linksDeleteAddress.isEmpty()){
            for(WebElement link : linksDeleteAddress.subList(1, linksDeleteAddress.size())){
                clickWhenReady(link);
                waitForVisible(msgbAlertSuccess);
                waitForInvisible(msgbAlertSuccess);
            }
        }
        return this;
    }
}
