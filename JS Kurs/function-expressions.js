// setTimeout(function(){
//     console.log('I waited 2 seconds');
// }, 2000);

/*

let counter = 1;
function timeout () {
    setTimeout(function(){
        console.log('hi ' + counter++);
        timeout();
    }, 2000);
}

timeout();  // to stop the execution hit "ctrl + c"

*/

/*
 
let counter = 1;
(function timeout() {
    setTimeout(function (){
        console.log('Immediately Invoked Function Expression (IIFE) ', 'counted: ', counter++);
        timeout();
    }, 2000);
})();  // with this () at the end we execute the function without to call it in another row like above


*/









// Vezba:

let counter = 1;
(function timeout (){
    setTimeout(function (){
        console.log('Hello, counted: ', counter++);
        timeout();
    }, 2000);
})();



let x = 5;
let y = 10;
let z = x - y * x;
console.log('Result is: ', z);