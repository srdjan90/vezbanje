package TheNinjaStore_Tests;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Base.Browser;
import Base.GlobalVariables;
import Base.Report;
import TheNinjaStore_Pages.NinjaStore_GlobalVariables;
import TheNinjaStore_Pages.Navigation;
import TheNinjaStore_Pages.PLP;

public class firstTest {
	private static String productName = "iPod Classic";

	@BeforeMethod(alwaysRun = true)
	public void setUp() throws Throwable {
		Browser.url = NinjaStore_GlobalVariables.URL;
		Report.passed = true;
		GlobalVariables.siteName = "TheNinjaStore";
		GlobalVariables.testName = "TheNinjaStore_"+this.getClass().getSimpleName();
	}

	@Test
	public static void firstTest() throws Throwable {
		Browser.openBrowser();
		Navigation.verifyLogoIsPresent();
		Navigation.clickLogoFromHeader();
		Navigation.verifyAllMegaLinksArePresent();
		Navigation.enterTextInSearchBar("Hello");
		Navigation.ClickSearchButton();
		Navigation.verifyAllMegaLinksArePresent();
		Navigation.verifyDesktopsSubLinks();
		Navigation.verifyLaptopsAndNotebooksSubLinks();
		Navigation.verifyComponentsSubLinks();
		Navigation.verifyMP3PlayersSubLinks();
		Navigation.clickSublink("MP3 Players", "Show All MP3 Players");
		Navigation.verifyNumberOfItemsInShoppingCart("0");
		Navigation.verifyProductTotalInShoppingCart("$0.00");
		PLP.verifyProductImageIsPresent(productName);
		PLP.clickProductImage(productName);
		Browser.clickBackButton();
		PLP.clickProductName(productName);
		Browser.clickBackButton();
		PLP.verifyProductPrice(productName, "$122.00");
		PLP.verifyProductExTax(productName, "$100.00");
		PLP.clickAddToCartButton(productName);
		Thread.sleep(2000);
		Navigation.verifyNumberOfItemsInShoppingCart("1");
		Navigation.verifyProductTotalInShoppingCart("$122.00");
		Navigation.clickShoppingCartButton();
		Navigation.verifyProductImageInCart(productName);
		Navigation.clickProductImageIncart(productName);
		Navigation.clickShoppingCartButton();
		Navigation.verifyProductNameInCart(productName);
		Navigation.clickProductNameIncart(productName);
		Navigation.clickShoppingCartButton();
		Navigation.verifyQuantityInCartDropdown(productName, "1");
		Navigation.verifyProductTotalInCartDropdown(productName, "$122.00");
		Navigation.clickDeleteFromCartButton(productName);
		Thread.sleep(2000);
		Navigation.verifyNumberOfItemsInShoppingCart("0");
		Navigation.verifyProductTotalInShoppingCart("$0.00");
		Navigation.clickShoppingCartButton();
		Navigation.verifyCartIsEmpty();
		Navigation.clickShoppingCartButton();

		Navigation.verifyMyAccountLinkIsPresent();
		Navigation.clickMyAccountLink();
		Navigation.verifyRegisterLinkIsPresent();
		Navigation.verifyLoginLinkIsPresent();
		Navigation.clickRegisterLink();
		Navigation.clickMyAccountLink();
		Navigation.clickLoginLink();

		Navigation.verifyWishListIsPresent();
		Navigation.verifyNumberOfItemsInWishLeast("0");
		Navigation.clickWishList();

		// Browser.closeBrowser();
	}

	@AfterMethod(alwaysRun = true)
	public void tearDown() throws IOException {
		Report.writeReport(GlobalVariables.testName);
		Browser.closeBrowser();
	}
}
