package TheNinjaStore_Repository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import Base.Browser;

public class Repository {

//	public static WebElement findElementByXpath(String xPath) {
//		System.out.println("Searching for element with xPath: -- " + xPath + " --");
//    	WebElement element = Browser.driver.findElement(By.xpath(xPath));
//    	return element;
//    }

	// Navigation
	public static String navigation_HeaderLogo = "//a[text()='The Ninja Store']";
	public static String navigation_SearchBar = "//input[@name='search']";
	public static String navigation_SearchButton = "//button/i[@class='fa fa-search']";
	public static String navigation_ShoppingCartButton = "//button[@class='btn btn-inverse btn-block btn-lg dropdown-toggle']";
	public static String navigation_MyAccount = "//a[@title='My Account']";
	public static String navigation_RegisterLink = "//a[text()='Register']";
	public static String navigation_LoginLink = "//a[text()='Login']";
	public static String navigaition_WishListHeader = "//a[@id='wishlist-total']";
	public static String navigation_ShoppingCartHeader = "//a[@title='Shopping Cart']";
	public static String navigation_CheckoutHeader = "//a[@title='Checkout']";
	public static String navigation_LogoutLink = "//ul[@class='dropdown-menu dropdown-menu-right']//a[text()='Logout']";

	// Home Page

	// PLP

	// PDP

	// Register Page
	public static String registerPage_LoginPageLink = "//a[text()='login page']";
	public static String registerPage_FirstNameField = "//input[@id='input-firstname']";
	public static String registerPage_LastNameField = "//input[@id='input-lastname']";
	public static String registerPage_EmailField = "//input[@id='input-email']";
	public static String registerPage_TelephoneField = "//input[@id='input-telephone']";
	public static String registerPage_PasswordField = "//input[@id='input-password']";
	public static String registerPage_PasswordConfirmationField = "//input[@id='input-confirm']";
	public static String registerPage_ContinueButton = "//input[@value='Continue']";

	public static String registerPage_LoginButton = "//a[@class='list-group-item' and text()='Login']";
	public static String registerPage_RegisterButton = "//a[@class='list-group-item' and text()='Register']";
	public static String registerPage_ForgottenPasswordButton = "//a[@class='list-group-item' and text()='Forgotten Password']";
	public static String registerPage_MyAccountButton = "//a[@class='list-group-item' and text()='My Account']";
	public static String registerPage_AddressBookButton = "//a[@class='list-group-item' and text()='Address Book']";
	public static String registerPage_WishListButton = "//a[@class='list-group-item' and text()='Wish List']";
	public static String registerPage_OrderHistoryButton = "//a[@class='list-group-item' and text()='Order History']";
	public static String registerPage_DownloadsButton = "//a[@class='list-group-item' and text()='Downloads']";
	public static String registerPage_RecurringPaymentsButton = "//a[@class='list-group-item' and text()='Recurring payments']";
	public static String registerPage_RewardPointsButton = "//a[@class='list-group-item' and text()='Reward Points']";
	public static String registerPage_ReturnsButton = "//a[@class='list-group-item' and text()='Returns']";
	public static String registerPage_TransactionsButton = "//a[@class='list-group-item' and text()='Transactions']";
	public static String registerPage_NewsletterButton = "//a[@class='list-group-item' and text()='Newsletter']";

	public static String registerPage_EMailField = "//input[@name='email']";
	public static String registerPage_PrivacyPolicyCheckbox = "//input[@name='agree']";
	public static String registerPage_ErrorMessageBox = "//div[@class='alert alert-danger alert-dismissible']";
	public static String registerPage_FirstNameFieldErrorMessage = registerPage_FirstNameField
			+ "/following-sibling::div[@class='text-danger']";
	public static String registerPage_LastNameFieldErrorMessage = registerPage_LastNameField
			+ "/following-sibling::div[@class='text-danger']";
	public static String registerPage_EMailFieldErrorMessage = registerPage_EMailField
			+ "/following-sibling::div[@class='text-danger']";
	public static String registerPage_TelephoneFieldErrorMessage = registerPage_TelephoneField
			+ "/following-sibling::div[@class='text-danger']";
	public static String registerPage_PasswordFieldErrorMessage = registerPage_PasswordField
			+ "/following-sibling::div[@class='text-danger']";
	public static String registePage_PasswordConfirmationFieldErrorMessage = registerPage_PasswordConfirmationField
			+ "/following-sibling::div[@class='text-danger']";

	// Account Page
	public static String accountPage_Message = "//div[@id='content']";
	public static String accountPage_ContinueButton = "//a[@class='btn btn-primary']";
	public static String accountPage_PageTitle = "//h2[text()='My Account']";

	// Login Page
	public static String loginPage_ContinueButton = "//a[text()='Continue']";
	public static String loginPage_LoginButton = "//input[@value='Login']";
	public static String loginPage_EmailAddressField = "//input[@name='email']";
	public static String loginPage_PasswordField = "//input[@name='password']";
	public static String loginPage_ForgottenPasswordLink = "//div[@class='form-group']/a[contains(@href,'forgotten')]";

}
