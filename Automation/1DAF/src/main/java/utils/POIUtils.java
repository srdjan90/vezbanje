/***************************************************************************************************
 Created by vskyba on 10/9/18 4:07 PM
 **************************************************************************************************/

package utils;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.io.IOException;

public final class POIUtils {

    private POIUtils() {
    }

    public static String getCellValueAsString(Cell cell) {
        String cellValue = Consts.EMPTY_STRING;
        if (cell != null) {
            String cellAddress = cell.getAddress().toString();
            switch (cell.getCellTypeEnum()) {
                case STRING:
                    cellValue = cell.getStringCellValue();
                    System.out.println(cellAddress + ": STRING cell type: " + cellValue);
                    break;
                case NUMERIC:
                    if (DateUtil.isCellDateFormatted(cell)) {
                        cellValue = cell.getDateCellValue().toString();
                        System.out.println(cellAddress + ": DATE cell type: " + cellValue);
                        break;
                    } else {
                        cellValue = String.valueOf((int) cell.getNumericCellValue());
                        System.out.println(cellAddress + ": NUMERIC cell type: " + cellValue);
                        break;
                    }
                case BOOLEAN:
                    cellValue = cell.getBooleanCellValue() ? "TRUE" : "FALSE";
                    System.out.println(cellAddress + ": BOOLEAN cell type: " + cellValue);
                    break;
                case FORMULA:
                    cellValue = cell.getCellFormula();
                    System.out.println(cellAddress + ": FORMULA cell type: " + cellValue);
                    break;
                case BLANK:
                    System.out.println(cellAddress + ": EMPTY cell value");
                    return Consts.EMPTY_STRING;
            }
            return cellValue;
        } else {
            System.out.println("NULL Cell obj");
            return cellValue;
        }
    }



    public static Workbook getWorkBook(String filePath) {
        Workbook wb = null;
        try {
            wb = WorkbookFactory.create(new File(filePath));
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Invalid file format / retrieval didn't succeed");
            System.exit(1);
        }
        return wb;
    }

}
