package Base;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.Select;

import MyStore_Repository.Repository;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.testng.Assert.fail;

public class UI {

	public static WebElement findElementByXpath(String elementName, String xPath) throws IOException {

		System.out.println("Searching for element " + elementName + " with xPath: -- " + xPath + " --");
		WebElement element = null;
		try {
			element = Browser.driver.findElement(By.xpath(xPath));
			return element;
		} catch (Exception e) {
			System.out.println("FAIL - Element " + elementName + " by xPath -- " + xPath + " -- does not exist");
			Report.failed("FAIL - Element " + elementName + " by xPath -- " + xPath + " -- does not exist");
			fail("FAIL - Element " + elementName + " by xPath -- " + xPath + " -- does not exist");

		}

		return element;
	}

	public static void clickElement(String elementName, String xPath) throws IOException {
		WebElement element = findElementByXpath(elementName, xPath);

		if (element == null) {
			System.out.println("FAIL - " + elementName + " is not present by xPath: " + xPath);
			Report.failed("FAIL - " + elementName + " is not present by xPath: " + xPath);
			fail("FAIL - " + elementName + " is not present by xPath: " + xPath);
		} else if (element.isDisplayed()) {
			System.out.println("Pass - " + elementName + " is present by xPath: " + xPath);
			System.out.println("Clicking on " + elementName);
			element.click();
		} else {
			System.out.println("FAIL - " + elementName + " is not present by xPath: " + xPath);
			Report.failed("FAIL - " + elementName + " is not present by xPath: " + xPath);
			fail("FAIL - " + elementName + " is not present by xPath: " + xPath);
		}
	}

	public static void verifyElementIsPresent(String elementName, String xPath) throws IOException {
		WebElement element = findElementByXpath(elementName, xPath);
		if (element.isDisplayed()) {
			System.out.println("Pass - " + elementName + " is present by xPath: " + xPath);
		}
	}

	public static void verifyElementContainsPartialText(String elementName, String xPath, String partialText)
			throws Throwable {
		WebElement element = findElementByXpath(elementName, xPath);

		verifyElementIsPresent(elementName, xPath);
		if (element.getText().contains(partialText)) {
			System.out.println("Pass - " + elementName + " By xPath: " + xPath + " contains expected text: -- "
					+ partialText + " --");
		} else {
			System.out.println("FAIL - " + elementName + " By xPath: " + xPath + " does not contain expected text: -- "
					+ partialText + " --. Text being displayed is -- " + element.getText() + " --");
			Report.failed("FAIL - " + elementName + " By xPath: " + xPath + " does not contain expected text: -- "
					+ partialText + " --. Text being displayed is -- " + element.getText() + " --");
			fail("FAIL - " + elementName + " By xPath: " + xPath + " does not contain expected text: -- " + partialText
					+ " --. Text being displayed is -- " + element.getText() + " --");
		}
	}

	public static void hoverElement(String elementName, String xPath) throws IOException {
		WebElement we = findElementByXpath(elementName, xPath);
		System.out.println("Hovering on " + elementName);
		Actions action = new Actions(Browser.driver);
		action.moveToElement(we).build().perform();
	}

	public static void selectValueFromDropDown(String elementName, WebElement elementToSelect, String valueToSet) {
		System.out.println("Selecting " + valueToSet + " from " + elementName);
		new Select(elementToSelect).selectByVisibleText(valueToSet);
	}

	public static void verifyValueOptionsFromDropDown(WebElement elementToFind, String valueToVerify)
			throws IOException {
		Boolean itemFound = false;
		if (elementToFind == null) {
			System.out.println("FAIL - The element does not exist.");
			Report.failed("FAIL - The element does not exist.");
			fail("FAIL - The element does not exist.");
		} else {
			List<WebElement> alloption = new Select(elementToFind).getOptions();

			for (WebElement eachOption : alloption) {
				System.out.println("This is --" + eachOption.getText() + "--");
				if (eachOption.getText().contains(valueToVerify)) {
					System.out.println("Pass - The  " + valueToVerify + " Option has been found.");
					itemFound = true;
				}
			}
			if (itemFound == false) {
				System.out.println(
						"FAIL - The  " + valueToVerify + " Option cannot be found in Element " + elementToFind + ".");
				Report.failed("FAIL - The " + elementToFind + " does not contain option " + valueToVerify + ".");
				fail("FAIL - The " + elementToFind + " does not contain option " + valueToVerify + ".");
			}
		}
	}

	public static void enterTextIntoElement(String elementName, String xPath, String valueToEnter) throws IOException {
		WebElement element = findElementByXpath(elementName, xPath);
		System.out.println("Entering -- " + valueToEnter + " -- into -- " + elementName + " --");
		element.clear();
		element.sendKeys(valueToEnter);

	}

	public static void selectCheckBoxIfUnselected(String elementName, String xPath) throws IOException {
		WebElement element = findElementByXpath(elementName, xPath);
		if (element.isSelected()) {
			System.out.println(elementName + " is already selected - continuing");
		} else {
			element.click();
			System.out.println("Selecting " + elementName);
		}
	}

	public static void unselectCheckBoxIfSelected(String elementName, String xPath) throws IOException {
		WebElement element = findElementByXpath(elementName, xPath);
		if (!element.isSelected()) {
			System.out.println(elementName + " is already unselected - continuing");
		} else {
			element.click();
			System.out.println("Unselecting " + elementName);
		}
	}

	public static void createScreenshot(String screenshotName) throws IOException {
		File scrFile = ((TakesScreenshot) Browser.driver).getScreenshotAs(OutputType.FILE);
		// Now you can do whatever you need to do with it, for example copy somewhere
		FileHandler.copy(scrFile,
				new File("C:\\Automation_Screenshots\\" + screenshotName + GlobalVariables.time + ".png"));
	}

}
