package Base;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import MyStore_Pages.MyStore_GlobalVariables;

public class Browser {

	public static WebDriver driver;
	public static WebDriverWait wait;
	public static int defaultWaitTime = 10;
	public static int defaultImplicitWaitTime = 30;
	public static String defaultBrowser = "Chrome";
	public static String url;

	public static WebDriver openBrowser() {

		String browser = "";

		if (defaultBrowser.toLowerCase().equals("chrome")) {
			System.setProperty("webdriver.chrome.driver", "Resource//chromedriver.exe");
			driver = new ChromeDriver();
			browser = "Google Chrome";
		} else if (defaultBrowser.toLowerCase().equals("internetExplorer")) {

		} else if (defaultBrowser.toLowerCase().equals("fireFox")) {
			System.setProperty("webdriver.gecko.driver", "Resource//geckodriver.exe");
			driver = new FirefoxDriver();
			browser = "Mozila FireFox";
		} else if (defaultBrowser.toLowerCase().equals("microsoftEdge")) {

			browser = "Microsoft Edge";
		} else {
			System.out.println("Error - Wrong Browser");
		}

		System.out.println("Opening --" + url + "-- on " + browser + " browser");
		driver.manage().timeouts().implicitlyWait(defaultImplicitWaitTime, TimeUnit.SECONDS);
		wait = new WebDriverWait(driver, defaultWaitTime);

		driver.manage().window().maximize();
		driver.get(url);

		return driver;
	}

	public static void closeBrowser() {
		driver.quit();
		System.out.println("Closing Browser");
	}

	public static void clickBackButton() {
		driver.navigate().back();
	}
}
