/***************************************************************************************************
 Created by vskyba on 10/9/18 2:22 PM
 **************************************************************************************************/

package onedStore.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import logic.BasePage;
import java.util.List;

public class ODS_CustomizePage extends BasePage {

    @FindBy(css = "a h3") private WebElement lblProductName;
    @FindBy(css = "[name='quantity']") private WebElement txtQty;
    @FindBy(css = ".rcenter-sm .row .col-md-2 [type='text']") private List<WebElement> txtSizes;
    @FindBy(css = "span[data-group-id='78']") private List<WebElement> dgdItemColors;
    @FindBy(css = "textarea#comments_78") private WebElement txaColorComment;
    @FindBy(css = ".square-content span.required:not(.square)") private List<WebElement> dgdDecorationMethods;
    @FindBy(css = "[onclick='AttachArtworkSignIn()']") private WebElement btnAddArtwork;
    @FindBy(css = "[title='Add Clipart']") private WebElement btnAddClipart;
    @FindBy(css = "[placeholder='Artwork comments']") private WebElement txaArtworkComment;
    @FindBy(css = ".note-editable") private WebElement txaAddText;
    @FindBy(css = "[data-group-id='13']:not([type='hidden'])") private List<WebElement> dgdImprintColors;
    @FindBy(css = "[data-txt_gramm_id='baa82081-eb34-bdfd-ecb3-1c5a8d37bdbb']") private WebElement txaImprintColorComment;
    @FindBy(css = ".rcenter-sm .badge") private WebElement lblDeliveryDate;
    @FindBy(css = ".square-content span.square") private List<WebElement> dgdImprintLocations;

    public ODS_CustomizePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

//TODO Attach artwork methods and enums for webElements lists

    private String getProductName() {
        waitForVisible(lblProductName);
        return lblProductName.getText();
    }

    private ODS_CustomizePage typeQty(String inputText) {
        typeIn(txtQty, inputText);
        return this;
    }

    private ODS_CustomizePage typeSize(String inputText, int sizeElement) {
        typeIn(txtSizes.get(sizeElement), inputText);
        return this;
    }

    private ODS_CustomizePage chooseItemColor(int itemColorNumber) {
        clickWhenReady(dgdItemColors.get(itemColorNumber));
        return this;
    }

    private ODS_CustomizePage typeColorComment(String inputText) {
        typeIn(txaColorComment, inputText);
        return this;
    }

    private ODS_CustomizePage chooseDecorationMethod(int decorationMethodNumber) {
        clickWhenReady(dgdDecorationMethods.get(decorationMethodNumber));
        return this;
    }

    private ODS_CustomizePage typeArtworkComment(String inputText) {
        typeIn(txaArtworkComment, inputText);
        return this;
    }

    private ODS_CustomizePage typeTextComment(String inputText) {
        typeIn(txaAddText, inputText);
        return this;
    }

    private ODS_CustomizePage chooseImprintColor(int imprintColorNumber) {
        clickWhenReady(dgdImprintColors.get(imprintColorNumber));
        return this;
    }

    private ODS_CustomizePage typeImprintColorComment(String inputText) {
        typeIn(txaImprintColorComment, inputText);
        return this;
    }

    private String getDeliveryDate() {
        waitForVisible(lblDeliveryDate);
        return lblDeliveryDate.getText();
    }

    private ODS_CustomizePage chooseImprintLocation(int imprintLocationNumber) {
        clickWhenReady(dgdImprintLocations.get(imprintLocationNumber));
        return this;
    }

}
