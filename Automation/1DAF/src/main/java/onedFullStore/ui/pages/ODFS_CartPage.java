package onedFullStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ODFS_CartPage extends BasePage {

    @FindAll(@FindBy(how = How.XPATH, using = "//div[contains(@class,'panel panel-default')]//span[@class='glyphicon glyphicon-trash']")) private List<WebElement> btnRemove;
    @FindBy(how = How.XPATH, using = "//button[@data-bb-handler='confirm']") private WebElement btnOk;

    public ODFS_CartPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public ODFS_CartPage confirmProductRemove(){
        waitForModals();
        clickWhenReady(btnOk);
        return this;
    }

    public ODFS_CartPage deleteAllItems() throws InterruptedException {
        btnRemove.stream()
                .forEach(x -> {
                    waitForSpinners();
                    clickWhenReady(x);
                    confirmProductRemove();
                });
        return this;
    }
}
