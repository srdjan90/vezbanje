package onedFullStore.ui.pages;

import logic.BasePage;
import onedFullStore.ui.widgets.ODFS_Header;
import onedStore.ui.widgets.ODS_Header;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ODFS_HomePage extends BasePage {

    public ODFS_Header header;


    //    region Banners at the top
    @FindBy(css = ".carousel-inner .active") private WebElement imgActiveBanner;
    @FindBy(css = ".carousel .glyphicon-chevron-right") private WebElement chvBannerRight;
    @FindBy(css = ".carousel .glyphicon-chevron-left") private WebElement chvBannerLeft;
    @FindBy(css = ".banner-left-sm img") private WebElement imgStaticBannerRightTop;
    @FindBy(css = ".banner-right-sm img") private WebElement imgStaticBannerRightUnderTop;
    //    endregion

    //region Most Viewed
    @FindBy(css = ".jcarousel-wrapper .glyphicon-chevron-right") private WebElement chvMostVievedRight;
    @FindBy(css = ".jcarousel-wrapper .glyphicon-chevron-left") private WebElement chvMostViewedLeft;
    @FindBy(css = "#jcarousel-mostviewed img") private WebElement lstMostViewedProducts;
    //endregion

    //  region Categories widget
    @FindBy(css = "[aria-haspopup='true']") private WebElement lstProductCategories;
    //    endregion

    public ODFS_HomePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        header = new ODFS_Header(driver, wait);
    }


    private ODFS_HomePage clickActiveBanner() {
        clickWhenReady(imgActiveBanner);
        return new ODFS_HomePage(driver, wait);
    }

    private ODFS_HomePage slideBannerRight() {
        clickWhenReady(chvBannerRight);
        return new ODFS_HomePage(driver, wait);
    }

    private ODFS_HomePage slideBannerLeft() {
        clickWhenReady(chvBannerLeft);
        return new ODFS_HomePage(driver, wait);
    }

    private ODFS_HomePage clickStaticBannerRightTop() {
        clickWhenReady(imgStaticBannerRightTop);
        return new ODFS_HomePage(driver, wait);
    }

    public ODFS_HomePage clickStaticBannerRightUnderTop() {
        clickWhenReady(imgStaticBannerRightUnderTop);
        return new ODFS_HomePage(driver, wait);
    }

    public ODFS_HomePage slideMostVievedRight() {
        clickWhenReady(chvMostVievedRight);
        return new ODFS_HomePage(driver, wait);
    }

    public ODFS_HomePage slideMostViewedLeft() {
        clickWhenReady(chvMostViewedLeft);
        return new ODFS_HomePage(driver, wait);
    }

}
