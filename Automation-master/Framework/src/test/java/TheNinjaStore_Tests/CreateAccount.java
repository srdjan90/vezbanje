package TheNinjaStore_Tests;

import java.io.IOException;
import java.util.Random;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Base.Browser;
import Base.GlobalVariables;
import Base.Report;
import TheNinjaStore_Pages.AccountPage;
import TheNinjaStore_Pages.LoginPage;
import TheNinjaStore_Pages.Navigation;
import TheNinjaStore_Pages.RegisterPage;

public class CreateAccount {
	
	static Random rand = new Random(); 
	
	private static String invalidFirstName = "aVerryLongFirstNameWithOverThirtyTwoCharackters";
	private static String invalidLastName = "aVerryLongLastNameWithOverThirtyTwoCharackters";
	private static String invalidEmailAddress = "invalidmail@mailcom";
	private static String existingEmail = "test@mail.com";
	private static String validEmail = "automationmj"+rand.nextInt()+"@mailinator.com";
	private static String shortPhone = "12";
	private static String longPhone = "123456789012345678901234567890123";
	private static String invalidPassword = "12";
	private static String privacyPolicyErrorMessage = "rWarning: You must agree to the Privacy Policy!";
	private static String existingAccountErrorMessage = "Warning: E-Mail Address is already registered!";
	
	@BeforeMethod(alwaysRun = true)
    public void setUp() throws Throwable {
	Browser.url = GlobalVariables.URL;
	Report.passed = true;
	GlobalVariables.siteName = "TheNinjaStore";
	GlobalVariables.testName = "TheNinjaStore_"+this.getClass().getSimpleName();
    }
	
	@Test
    public static void CreateAccountTest() throws Throwable {
		 Browser.openBrowser();
		 Navigation.clickMyAccount();
		 Navigation.clickRegisterLink();
		 // verify Register page is opened
		 
		 // empty form error messages
		 RegisterPage.clickContinueButton();
		 RegisterPage.verifyErrorMessage(privacyPolicyErrorMessage);
		 RegisterPage.verifyEmptyFormFieldErrors();
		 // first name error message
		 RegisterPage.enterGenericInformation();
		 RegisterPage.checkPrivacyPolicyCheckBox();
		 RegisterPage.enterTextIntoFirstNameField(invalidFirstName);
		 RegisterPage.clickContinueButton();
		 RegisterPage.verifyFirstNameErrorMessage();
		 // last name error message
		 RegisterPage.enterGenericInformation();
		 RegisterPage.checkPrivacyPolicyCheckBox();
		 RegisterPage.enterTextIntoLastNameField(invalidLastName);
		 RegisterPage.clickContinueButton();
		 RegisterPage.verifyLastNameErrorMessage();
		 // Email error message
		 RegisterPage.enterGenericInformation();
		 RegisterPage.checkPrivacyPolicyCheckBox();
		 RegisterPage.enterTextIntoEMailField(invalidEmailAddress);
		 RegisterPage.clickContinueButton();
		 RegisterPage.verifyEMailErrorMessage();
		 // Short telephone error message
		 RegisterPage.enterGenericInformation();
		 RegisterPage.checkPrivacyPolicyCheckBox();
		 RegisterPage.enterTextIntoTelephoneField(shortPhone);
		 RegisterPage.clickContinueButton();
		 RegisterPage.verifyTelephoneErrorMessage();
		 // long telephone error message
		 RegisterPage.enterGenericInformation();
		 RegisterPage.checkPrivacyPolicyCheckBox();
		 RegisterPage.enterTextIntoTelephoneField(longPhone);
		 RegisterPage.clickContinueButton();
		 RegisterPage.verifyTelephoneErrorMessage();
		 // invalid password
		 RegisterPage.enterGenericInformation();
		 RegisterPage.checkPrivacyPolicyCheckBox();
		 RegisterPage.enterTextIntoPasswordField(invalidPassword);
		 RegisterPage.clickContinueButton();
		 RegisterPage.verifyPasswordErrorMessage();
		 // different confirmation password
		 RegisterPage.enterGenericInformation();
		 RegisterPage.checkPrivacyPolicyCheckBox();
		 RegisterPage.enterTextIntoPasswordConfirmationField(invalidPassword);
		 RegisterPage.clickContinueButton();
		 RegisterPage.verifyPasswordConfirmationErrorMessage();
		 // Existing account error
		 RegisterPage.enterGenericInformation();
		 RegisterPage.checkPrivacyPolicyCheckBox();
		 RegisterPage.enterTextIntoEMailField(existingEmail);
		 RegisterPage.clickContinueButton();
		 RegisterPage.verifyErrorMessage(existingAccountErrorMessage);
		 // uncheck privacy policy
		 RegisterPage.enterGenericInformation();
		 RegisterPage.uncheckPrivacyPolicyChechkBox();
		 RegisterPage.clickContinueButton();
		 RegisterPage.verifyErrorMessage(privacyPolicyErrorMessage);
		 // create account
		 RegisterPage.enterGenericInformation();
		 RegisterPage.enterTextIntoEMailField(validEmail);
		 RegisterPage.checkPrivacyPolicyCheckBox();
		 RegisterPage.clickContinueButton();
		 AccountPage.verifySuccessfullyCreatedAccountMessage();
		 AccountPage.clickContinueButton();
		 AccountPage.verifyPageTitle();
		 Navigation.clickMyAccount();
		 Navigation.clickLogoutLink();
		 AccountPage.verifySuccessfullyLoggedOffMessage();
		 Navigation.clickMyAccount();
		 Navigation.clickLoginLink();
		 LoginPage.enterTextIntoEmailField(validEmail);
		 LoginPage.enterTextIntoPasswordField(GlobalVariables.password);
		 LoginPage.clickLoginButton();
		 AccountPage.verifyPageTitle();
		 
		 System.out.println("New account email address is: " + validEmail);
	}
	
	@AfterMethod(alwaysRun = true)
    public void tearDown() throws IOException {
		Report.writeReport(GlobalVariables.testName);
    	Browser.closeBrowser();
    }

}
