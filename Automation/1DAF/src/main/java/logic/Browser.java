package logic;

import static java.util.concurrent.TimeUnit.SECONDS;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeDriverService;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.GeckoDriverService;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import utils.Consts;
import utils.Utils;

public enum Browser {
    EDGE, FIREFOX, GOOGLE_CHROME, INTERNET_EXPLORER, PROPERTY;

    private static final String DEFAULT_BROWSER = "GC";

    static {
        System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY, Consts.USER_DIR + "\\src\\main\\resources\\drivers\\chromedriver.exe");
        System.setProperty(GeckoDriverService.GECKO_DRIVER_EXE_PROPERTY, Consts.USER_DIR + "\\src\\main\\resources\\drivers\\geckodriver.exe");
        System.setProperty(InternetExplorerDriverService.IE_DRIVER_EXE_PROPERTY, Consts.USER_DIR + "\\src\\main\\resources\\drivers\\IEDriverServer.exe");
        System.setProperty(EdgeDriverService.EDGE_DRIVER_EXE_PROPERTY, Consts.USER_DIR + "\\src\\main\\resources\\drivers\\MicrosoftWebDriver.exe");
    }

    public WebDriver getInstance() {
        WebDriver driver = null;
        switch (this) {
            case GOOGLE_CHROME:
                driver = new ChromeDriver();
                break;
            case FIREFOX:
                driver = new FirefoxDriver();
                break;
            case INTERNET_EXPLORER:
                driver = new InternetExplorerDriver();
                break;
            case EDGE:
                driver = new EdgeDriver();
                break;
            case PROPERTY:
                switch (System.getProperty("ui_browser", DEFAULT_BROWSER)) {
                    case "GC":
                        driver = new ChromeDriver();
                        break;
                    case "FF":
                        driver = new FirefoxDriver();
                        break;
                    case "IE":
                        driver = new InternetExplorerDriver();
                        break;
                    case "EG":
                        driver = new EdgeDriver();
                        break;
                }
        }
        driver.manage().timeouts().setScriptTimeout(60, SECONDS);
        driver.manage().timeouts().pageLoadTimeout(60, SECONDS);
        driver.manage().window().maximize();
        return driver;
    }

}
