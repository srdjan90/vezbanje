package onedStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class ODS_EditShippingAddressPage extends BasePage {

	public ODS_EditShippingAddressPage (WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
	}

	@FindBy(id = "addressLabel") 
	private WebElement txtAddressLabel;

	@FindBy(id = "countryID") 
	private WebElement dllCountry;

	@FindBy(id = "firstName") 
	private WebElement txtFirstName;

	@FindBy(id = "lastName") 
	private WebElement txtLastName;

	@FindBy(id = "company") 
	private WebElement txtCompany;   

	@FindBy(id = "address1") 
	private WebElement txtAddress1;

	@FindBy(id = "address2") 
	private WebElement txtAddress2;

	@FindBy(id = "address3") 
	private WebElement txtAddress3;

	@FindBy(id = "city") 
	private WebElement txtCity;

	@FindBy(id = "stateID")
	private WebElement dllState;

	@FindBy(id = "zipCode") 
	private WebElement txtZipCode;

	@FindBy(id= "isDefaultAddress")
	private WebElement chbIsDefault;
	
	@FindBy(xpath = "//input[@value = 'Save Changes']")
	private WebElement btnSaveChanges; 
	
	@FindBy(xpath = "//button[contains(., 'Back to Your Addresses')]/../preceding-sibling::div[@class='modal-header']/h4")
	private WebElement lblModalTitle;
	
	@FindBy(xpath = "//button[contains(., 'Back to Your Addresses')]/../preceding-sibling::div[@class='modal-body']/div")
	private WebElement lblModalMessage;
	
	@FindBy(xpath = "//button[contains(., 'Back to Your Addresses')]")
	private WebElement btnModalBackToAddresses;
	
	@FindBy(xpath = "//button[contains(., 'Back to Your Addresses')]/following-sibling::button")
	private WebElement btnModalClose;

	//Methods

	public ODS_EditShippingAddressPage enterAddressLabel (String addressLabel) {
		typeIn(txtAddressLabel, addressLabel);
		return this;
	}

	public ODS_EditShippingAddressPage selectCountry (String country) {
		chooseValueFromSelect(dllCountry, country);
		return this;
	} 

	public ODS_EditShippingAddressPage enterFirstName (String firstName) {
		typeIn(txtFirstName, firstName);
		return this;
	}

	public ODS_EditShippingAddressPage enterLastName (String lastName) {
		typeIn(txtLastName, lastName);
		return this;
	}

	public ODS_EditShippingAddressPage enterCompany (String company) {
		typeIn(txtCompany, company);
		return this; 
	}

	public ODS_EditShippingAddressPage enterAddress1 (String address1) {
		typeIn(txtAddress1, address1);
		return this;
	}

	public ODS_EditShippingAddressPage enterAddress2 (String address2) {
		typeIn(txtAddress2, address2);
		return this;
	}

	public ODS_EditShippingAddressPage enterAddress3 (String address3) {
		typeIn(txtAddress3, address3);
		return this;
	}

	public ODS_EditShippingAddressPage enterCity (String city) {
		typeIn(txtCity, city);
		return this;
	}

	public ODS_EditShippingAddressPage selectState (String state) {
		chooseValueFromSelect(dllState, state);
		return this;
	}

	public ODS_EditShippingAddressPage enterZipCode (String zipCode) {
		typeIn(txtZipCode, zipCode);
		return this;
	}

	public ODS_EditShippingAddressPage checkDefault() {
		chbIsDefault.click();
		return this;
	} 

	public ODS_EditShippingAddressPage clickSaveChanges() {
		clickWhenReady(btnSaveChanges);
		return this;
	}
	
	public ODS_EditShippingAddressPage verifySuccessMessageAppears() {
		waitForVisible(lblModalTitle);
		Assert.assertEquals(lblModalTitle.getText(), "Success!");
		waitForVisible(lblModalMessage);
		Assert.assertEquals(lblModalMessage.getText(), "Your changes have been saved");
		return this;
	}
	
	public ODS_ManageShippingAddressesPage clickModalBackToAddressesButton() {
		clickWhenReady(btnModalBackToAddresses);
		return new ODS_ManageShippingAddressesPage(driver, wait);
	}
	
	public ODS_ManageShippingAddressesPage clickModalCloseButton() {
		clickWhenReady(btnModalClose);
		return new ODS_ManageShippingAddressesPage(driver, wait);
	}
}
