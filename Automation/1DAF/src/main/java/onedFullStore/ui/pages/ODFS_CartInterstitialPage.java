/*
 * Created by ekarliuchenko on 10/17/18 5:05 PM
 */

package onedFullStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

public class ODFS_CartInterstitialPage extends BasePage {

    @FindBy(css = "a[href='/products/custom/secure/checkout/']") private WebElement btnCheckout;
    @FindBy(css = "a[href='/products/custom/cart/']") private WebElement btnViewYourCart;
    @FindBy(css = ".center a[href='/products/custom/']") private WebElement btnCountinueShopping;
    @FindBy(css = ".marg0 a") private WebElement linkItemAdded;
    @FindBy(css = ".marg0 ~span:nth-of-type(1)") private WebElement cntrQuantity;
    @FindBy(css = ".marg0 ~span:nth-of-type(2)") private WebElement cntrProductSubtotal;
    @FindBy(css = ".marg0 ~strong") private WebElement cntrReadyToCheckout;

    public ODFS_CartInterstitialPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public ODFS_CheckoutPage proceedToCheckout() {
        clickWhenReady(btnCheckout);
        return new ODFS_CheckoutPage(driver, wait);
    }

    public ODFS_CartPage viewCart(){
        clickWhenReady(btnViewYourCart);
        return new ODFS_CartPage(driver, wait);
    }

    public ODFS_HomePage continueShopping(){
        clickWhenReady(btnCountinueShopping);
        return new ODFS_HomePage(driver, wait);
    }

    public String getItemName(){
        return waitForVisible(linkItemAdded).getText();
    }

    public ODFS_CartInterstitialPage verifyItemAddedName(String expectedName){
        assertEquals("The name doesn't match!", getItemName(), expectedName);
        return this;
    }

    public int getQuantity(){
        return Integer.parseInt(waitForVisible(cntrQuantity).getText());
    }

    public ODFS_CartInterstitialPage verifyQuantityAdded (int expectedQuantity){
        assertEquals("The quantity doesn't match!", getQuantity(), expectedQuantity);
        return this;
    }

    public double getProductSubtotal(){
        return Double.parseDouble(cntrProductSubtotal.getText().substring(1));
    }

    public ODFS_CartInterstitialPage verifySubtotal (double expectedSubtotal){
        assertEquals("The product subtotal doesn't match!", getProductSubtotal(), expectedSubtotal);
        return this;
    }
}
