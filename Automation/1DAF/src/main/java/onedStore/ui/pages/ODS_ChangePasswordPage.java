/***************************************************************************************************
 Created by vskyba on 9/27/18 3:59 PM
 **************************************************************************************************/

package onedStore.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import logic.BasePage;

public class ODS_ChangePasswordPage extends BasePage {

    //  region Change password form
    @FindBy(css = "#existingPassword") private WebElement txtExistingPassword;
    @FindBy(css = "#newPassword") private WebElement txtNewPassword;
    @FindBy(css = "#confirmPassword") private WebElement txtConfirmPassword;
    @FindBy(css = ".col-sm-8 [type='submit']") private WebElement btnSaveChanges;
    @FindBy(css = "a.btn-default") private WebElement btnCancel;
    //  endregion

    //  region Success pop up
    @FindBy(css = ".btn-success") private WebElement btnBackToMyAcc;
    @FindBy(css = "button.btn-default") private WebElement btnClose;
    @FindBy(css = ".bootbox-close-button") private WebElement btnCloseXtype;
    //  endregion

    public ODS_ChangePasswordPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    //  Module methods
    private ODS_ChangePasswordPage typeExistingPassword(String inputText) {
        typeIn(txtExistingPassword, inputText);
        return this;
    }

    private ODS_ChangePasswordPage typeNewPassword(String inputText) {
        typeIn(txtNewPassword, inputText);
        return this;
    }

    private ODS_ChangePasswordPage typeConfirmPassword(String inputText) {
        typeIn(txtConfirmPassword, inputText);
        return this;
    }

    private ODS_ChangePasswordPage clickSaveChanges() {
        clickWhenReady(btnSaveChanges);
        return this;
    }

    private ODS_MyAccountPage clickBackToMyAcc() {
        clickWhenReady(btnBackToMyAcc);
        return new ODS_MyAccountPage(driver, wait);
    }

    private ODS_MyAccountPage clickCancel() {
        clickWhenReady(btnCancel);
        return new ODS_MyAccountPage(driver, wait);
    }

    //  Main methods

    public ODS_MyAccountPage changePassword(String existingPassword, String newPassword) {
        typeExistingPassword(existingPassword).typeNewPassword(newPassword)
                                              .typeConfirmPassword(newPassword)
                                              .clickSaveChanges().clickBackToMyAcc();
        return new ODS_MyAccountPage(driver, wait);
    }

}
