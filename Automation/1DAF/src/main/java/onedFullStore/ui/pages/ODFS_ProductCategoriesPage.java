/*
 * Created by ekarliuchenko on 10/15/18 5:53 PM
 */

package onedFullStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ODFS_ProductCategoriesPage extends BasePage {
    public ODFS_ProductCategoriesPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }
}
