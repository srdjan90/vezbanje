package onedStore.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import logic.BasePage;
import org.testng.Assert;

public class ODS_CreateAccountPage extends BasePage{

	public ODS_CreateAccountPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
	}

	/**
	 * Elements
	 */

	@FindBy(xpath = "//input[@name='hdn_email']")
	private WebElement txtEmail;

	@FindBy(xpath = "//div[@class='recaptcha-checkbox-checkmark']")
	private WebElement chbRecaptcha;

	@FindBy(xpath = "//iframe[@role='presentation']")
	private WebElement fraRepactcha;

	@FindBy(xpath = "//button[@id='next-step']")
	private WebElement btnNext;

	@FindBy(xpath = "//button[@id='editEmail']")
	private WebElement btnChangeEmail;

	@FindBy(xpath = "//input[@id='txt_newpassword']")
	private WebElement txtPassword;

	@FindBy(xpath = "//input[@id='txt_confirmpassword']")
	private WebElement txtConfirmPassword;

	@FindBy(xpath = "//input[@id='txt_billing_firstname']")
	private WebElement txtFirstName;

	@FindBy(xpath = "//input[@id='txt_billing_lastname']")
	private WebElement txtLastName;

	@FindBy(xpath = "//input[@id='txt_billing_phone']")
	private WebElement txtPhone;

	@FindBy(xpath = "//select[@id='sel_billing_country']")
	private WebElement ddlBillingCountry;

	@FindBy(xpath = "//input[@id='txt_billing_company']")
	private WebElement txtBillingCompany;

	@FindBy(xpath = "//select[@id='sel_industry']")
	private WebElement ddlBillingIndustry;

	@FindBy(xpath = "//input[@id='txt_billing_address1']")
	private WebElement txtBillingAddress1;

	@FindBy(xpath = "//input[@id='txt_billing_address2']")
	private WebElement txtBillingAddress2;

	@FindBy(xpath = "//input[@id='txt_billing_address3']")
	private WebElement txtBillingAddress3;

	@FindBy(xpath = "//input[@id='txt_billing_city']")
	private WebElement txtBillingCity;

	@FindBy(xpath = "//select[@id='sel_billing_state']")
	private WebElement ddlBillingState;

	@FindBy(xpath = "//input[@id='txt_billing_zip']")
	private WebElement txtBillingZipCode;

	@FindBy(xpath = "//button[contains(.,'Same as billing')]")
	private WebElement btnSameAsBilling;

	@FindBy(xpath = "//input[@id='txt_shipping_address_label']")
	private WebElement txtShippingAddressLabel;

	@FindBy(xpath = "//select[@id='sel_shipping_country']")
	private WebElement ddlShippingCountry;

	@FindBy(xpath = "//input[@id='txt_shipping_company']")
	private WebElement txtShippingCompany;

	@FindBy(xpath = "//input[@id='txt_shipping_address1']")
	private WebElement txtShippingAddress1;

	@FindBy(xpath = "//input[@id='txt_shipping_address2']")
	private WebElement txtShippingAddress2;

	@FindBy(xpath = "//input[@id='txt_shipping_address3']")
	private WebElement txtShippingAddress3;

	@FindBy(xpath = "//input[@id='txt_shipping_city']")
	private WebElement txtShippingCity;

	@FindBy(xpath = "//input[@id='txt_shipping_zip']")
	private WebElement txtShippingZipCode;

	@FindBy(xpath = "//select[@id='sel_shipping_state']")
	private WebElement ddlShippingState;

	@FindBy(xpath = "//button[@id='btn_continue']")
	private WebElement btnCreateAccount;

	/**
	 * Methods
	 */

	public ODS_CreateAccountPage enterEmailAddress(String email){
		typeIn(txtEmail, email);
		return this;
	}

	public ODS_CreateAccountPage clickRecaptcha(){
		clickWhenReady(chbRecaptcha);
		//setAttribute(fraRepactcha, "src", Utils.generateRecaptchaKey(fraRepactcha.getAttribute("src")));
		return this;
	}

	public ODS_CreateAccountPage clickNext(){
		clickWhenReady(btnNext);
		return this;
	}

	public ODS_CreateAccountPage clickChange(){
		clickWhenReady(btnChangeEmail);
		return this;
	}

	public ODS_CreateAccountPage enterPassword(String password){
		typeIn(txtPassword, password);
		return this;
	}

	public ODS_CreateAccountPage enterConfirmPassword(String confirmPassword){
		typeIn(txtConfirmPassword, confirmPassword);
		return this;
	}

	public ODS_CreateAccountPage enterFirstName(String firstName){
		typeIn(txtFirstName, firstName);
		return this;
	}

	public ODS_CreateAccountPage enterLastName(String lastName){
		typeIn(txtLastName, lastName);
		return this;
	}

	public ODS_CreateAccountPage enterPhoneNumber(String phoneNumber){
		typeIn(txtPhone, phoneNumber);
		return this;
	}

	public ODS_CreateAccountPage selectBillingCountry(String value){
		chooseValueFromSelect(ddlBillingCountry, value);
		return this;
	}

	public ODS_CreateAccountPage enterBillingCompany(String company){
		typeIn(txtBillingCompany, company);
		return this;
	}

	public ODS_CreateAccountPage selectBillingIndustry(String value){
		chooseValueFromSelect(ddlBillingIndustry, value);
		return this;
	}

	public ODS_CreateAccountPage enterBillingAddress1(String address1){
		typeIn(txtBillingAddress1, address1);
		return this;
	}

	public ODS_CreateAccountPage enterBillingAddress2(String address2){
		typeIn(txtBillingAddress2, address2);
		return this;
	}

	public ODS_CreateAccountPage enterBillingAddress3(String address3){
		typeIn(txtBillingAddress3, address3);
		return this;
	}

	public ODS_CreateAccountPage enterBillingCity(String city){
		typeIn(txtBillingCity, city);
		return this;
	}

	public ODS_CreateAccountPage selectBillingState(String state){
		chooseValueFromSelect(ddlBillingState, state);
		return this;
	}

	public ODS_CreateAccountPage enterBillingZipCode(String zipCode){
		typeIn(txtBillingZipCode, zipCode);
		return this;
	}

	public ODS_CreateAccountPage clickSameAsBilling(){
		clickWhenReady(btnSameAsBilling);
		Assert.assertEquals(getTextFromElement(txtBillingCompany), getTextFromElement(txtShippingCompany), "Billing and Shipping company names are not the same. Test Failed!");
		Assert.assertEquals(getTextFromElement(txtBillingAddress1), getTextFromElement(txtShippingAddress1), "Billing and Shipping first addresses are not the same. Test Failed!");
		Assert.assertEquals(getTextFromElement(txtBillingAddress2), getTextFromElement(txtShippingAddress2), "Billing and Shipping second addresses are not the same. Test Failed!");
		Assert.assertEquals(getTextFromElement(txtBillingAddress3), getTextFromElement(txtShippingAddress3), "Billing and Shipping third addresses are not the same. Test Failed!");
		Assert.assertEquals(getTextFromElement(txtBillingCity), getTextFromElement(txtShippingCity), "Billing and Shipping city names are not the same. Test Failed!");
		Assert.assertEquals(getTextFromElement(txtBillingZipCode), getTextFromElement(txtShippingZipCode), "Billing and Shipping Zip codes are not the same. Test Failed!");
		return this;
	}

	public ODS_CreateAccountPage enterShippingAddressLabel(String label){
		typeIn(txtShippingAddressLabel, label);
		return this;
	}

	public ODS_CreateAccountPage selectShippingCountry(String value){
		chooseValueFromSelect(ddlShippingCountry, value);
		return this;
	}

	public ODS_CreateAccountPage enterShippingCompany(String shippintCompany){
		typeIn(txtShippingCompany, shippintCompany);
		return this;
	}

	public ODS_CreateAccountPage enterShippingAddress1(String address1){
		typeIn(txtShippingAddress1, address1);
		return this;
	}

	public ODS_CreateAccountPage enterShippingAddress2(String address2){
		typeIn(txtShippingAddress2, address2);
		return this;
	}

	public ODS_CreateAccountPage enterShippingAddress3(String address3){
		typeIn(txtShippingAddress3, address3);
		return this;
	}

	public ODS_CreateAccountPage enterShippingCity(String city){
		typeIn(txtShippingCity, city);
		return this;
	}

	public ODS_CreateAccountPage enterShippingZipCode(String zipCode){
		typeIn(txtShippingZipCode, zipCode);
		return this;
	}

	public ODS_CreateAccountPage selectShippingState(String value){
		chooseValueFromSelect(ddlShippingState, value);
		return this;
	}

	public ODS_MyAccountPage clickCreateAccount(){
		clickWhenReady(btnCreateAccount);
		return new ODS_MyAccountPage(driver, wait);
	}
}
