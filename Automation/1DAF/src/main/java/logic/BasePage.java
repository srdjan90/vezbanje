package logic;


import java.util.List;
import onedStore.ui.widgets.ODS_Footer;
import onedStore.ui.widgets.ODS_Header;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/* WebElements naming convention:
+----------+----------------------------+--------+-----------------+
| Category |      UI/Control type       | Prefix |     Example     |
+----------+----------------------------+--------+-----------------+
| Basic    | Button                     | btn    | btnExit         |
| Basic    | Check box                  | chb    | chbReadOnly     |
| Basic    | Combo box                  | cbo    | cboEnglish      |
| Basic    | Common dialog              | dlg    | dlgFileOpen     |
| Basic    | Counter                    | cntr   | cntrCartItems   |
| Basic    | Date picker                | dtp    | dtpPublished    |
| Basic    | Dropdown List / Select tag | dd     | ddlCountry      |
| Basic    | Form                       | form   | formEntry       |
| Basic    | Frame                      | fra    | fraLanguage     |
| Basic    | Image                      | img    | imgIcon         |
| Basic    | Label                      | lbl    | lblHelpMessage  |
| Basic    | Links/Anchor Tags          | link   | linkForgotPwd   |
| Basic    | List box                   | list   | listPolicyCodes |
| Basic    | Menu                       | menu   | menuFileOpen    |
| Basic    | Message Box                | msgb   | msgbWrongEmail  |
| Basic    | Radio button / group       | rdo    | rdoGender       |
| Basic    | RichTextBox                | rtf    | rtfReport       |
| Basic    | Table                      | tbl    | tblCustomer     |
| Basic    | TabStrip                   | tab    | tabOptions      |
| Basic    | Text Area                  | txa    | txaDescription  |
| Basic    | Text Box                   | txt    | txtLastName     |
| Complex  | Chevron                    | chv    | chvProtocol     |
| Complex  | Data grid                  | dgd    | dgdTitles       |
| Complex  | Data list                  | dbl    | dblPublisher    |
| Complex  | Directory list box         | dir    | dirSource       |
| Complex  | Drive list box             | drv    | drvTarget       |
| Complex  | File list box              | fil    | filSource       |
| Complex  | Panel/Fieldset             | pnl    | pnlGroup        |
| Complex  | ProgressBar                | prg    | prgLoadFile     |
| Complex  | Slider                     | sld    | sldScale        |
| Complex  | Spinner                    | spn    | spnPages        |
| Complex  | StatusBar                  | sta    | staDateTime     |
| Complex  | Timer                      | tmr    | tmrAlarm        |
| Complex  | Toolbar                    | tlb    | tlbActions      |
| Complex  | TreeView                   | tre    | treOrganization |
+----------+----------------------------+--------+-----------------+*/

public abstract class BasePage {

	protected WebDriver driver;
    protected WebDriverWait wait;

    public BasePage(WebDriver driver, WebDriverWait wait) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
        this.wait = wait;
    }

    private void waitForJSToLoad() {
        wait.until(WebDriver -> ((JavascriptExecutor) driver).executeScript("return document.readyState")
                .equals("complete"));
    }

    public void waitForSpinners(){
        wait.until(ExpectedConditions.or(
                ExpectedConditions.attributeToBe(By.xpath("/html/body"), "class",  "")
                //ExpectedConditions.attributeToBe(By.xpath("/html/body"), "class",  "modal-open")
        ));
    }

    public void waitForModals(){
        wait.until(ExpectedConditions.or(
                //ExpectedConditions.attributeToBe(By.xpath("/html/body"), "class",  "")
                ExpectedConditions.attributeToBe(By.xpath("/html/body"), "class",  "modal-open")
        ));
    }


    private void waitForJQueryToLoad() {
        wait.until(WebDriver -> (Long) ((JavascriptExecutor) driver).executeScript("return jQuery.active") == 0);
    }

    protected WebElement waitForVisible(WebElement element) {
        waitForJSToLoad();
        wait.until(ExpectedConditions.visibilityOf(element));
        return element;
    }

    protected List<WebElement> waitForVisible(List<WebElement> elements) {
        waitForJSToLoad();
        for (WebElement element : elements) {
            wait.until(ExpectedConditions.visibilityOf(element));
        }
        return elements;
    }

    protected WebElement waitForInvisible(WebElement element) {
        waitForJSToLoad();
        wait.until(ExpectedConditions.invisibilityOf(element));
        return element;
    }

    public void highlightElement(WebElement element){
        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor)driver).executeScript( "element = arguments[0];" +
                                                                "original_style = element.getAttribute('style');" +
                                                                "element.setAttribute('style', original_style + \"; background: lightblue; border: 3px dashed black;\");" +
                                                                "setTimeout(function(){" +
                                                                "element.setAttribute('style', original_style);" +
                                                                "}, 60);", element);
        }
    }

    protected WebElement clickWhenReady(WebElement element) {
        waitForVisible(element);
        wait.until(ExpectedConditions.elementToBeClickable(element))
                .click();
        return element;
    }

    protected WebElement typeIn(WebElement element, String inputText) {
        waitForVisible(element);
        element.clear();
        element.sendKeys(inputText);
        return element;
    }

    protected boolean isAttribtuePresent(WebElement element, String attribute) {
        return element.getAttribute(attribute) != null;
    }

    protected WebElement hoverOver(WebElement element) {
        Actions actions = new Actions(driver);
        actions.moveToElement(element)
                .build()
                .perform();
        return element;
    }

    protected void dragAndDrop (WebElement sourceElement, WebElement destinationElement){
        Actions actions = new Actions(driver);
        actions.dragAndDrop(sourceElement, destinationElement).build().perform();
    }

    protected void chooseValueFromSelect(WebElement element, String value) {
		waitForVisible(element);
		Select select = new Select(element);
		select.selectByValue(value);
	}
    
    protected void chooseFromSelectByText(WebElement element, String visibleText) {
		waitForVisible(element);
		Select select = new Select(element);
		select.selectByVisibleText(visibleText);
    }

	protected void chooseVisibleTextFromSelect(WebElement element, String text){
        waitForVisible(element);
        Select select = new Select(element);
        select.selectByVisibleText(text);
    }

    protected String getTextFromElement(WebElement element) {
		return waitForVisible(element).getText();
	}

    public void setAttribute(WebElement element, String attName, String attValue) {
        ((JavascriptExecutor)driver).executeScript("arguments[0].setAttribute(arguments[1], arguments[2]);",
                element, attName, attValue);
    }

    protected void selectByText(WebElement dd, String optionText){
        Select select = new Select(waitForVisible(dd));
        select.selectByVisibleText(optionText);
    }

    protected void selectByValue(WebElement dd, String optionValue){
        Select select = new Select(waitForVisible(dd));
        select.selectByValue(optionValue);
    }

    protected List<WebElement> getSelectOptions(WebElement dd){
        Select select = new Select(waitForVisible(dd));
        return select.getOptions();
    }
}
