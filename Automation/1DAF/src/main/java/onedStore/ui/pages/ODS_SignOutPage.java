package onedStore.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import logic.BasePage;

public class ODS_SignOutPage extends BasePage{

	public ODS_SignOutPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
		
	}
	
	/**
	 * Elements
	 */
	
	@FindBy(xpath = "//a[contains(.,'Continue')]")
	private WebElement btnContinue;
	
	@FindBy(xpath = "//a[contains(.,'Sign In')]")
	private WebElement btnSignIn;
	
	@FindBy(xpath = "//div[@class='alert alert-warning']")
	private WebElement lblSingOutWarning;
	
	/**
	 * Methods
	 */
	
	public ODS_HomePage clickContinue() {
		clickWhenReady(btnContinue);
		return new ODS_HomePage(driver, wait);
	}
	
	public ODS_SignInPage clickSignIn() {
		clickWhenReady(btnSignIn);
		return new ODS_SignInPage(driver, wait);
	}
	
	public ODS_SignOutPage isLoaded() {
		Assert.assertEquals(getTextFromElement(lblSingOutWarning), "You have been successfully signed out", "Unsuccessful Sign out. Test failed!");
		return this;
	}

}
