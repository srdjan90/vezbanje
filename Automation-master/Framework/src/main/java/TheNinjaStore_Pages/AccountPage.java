package TheNinjaStore_Pages;

import java.io.IOException;

import Base.UI;
import TheNinjaStore_Repository.Repository;

public class AccountPage {
	public static void verifyMessage(String message) throws Throwable {
		UI.verifyElementContainsPartialText("Account Message", Repository.accountPage_Message, message);
	}
	
	public static void clickContinueButton() throws IOException {
		UI.clickElement("Continue Button", Repository.accountPage_ContinueButton);
	}
	
	public static void verifyPageTitle() throws IOException {
		UI.verifyElementIsPresent("My Account Page", Repository.accountPage_PageTitle);
	}
	
	public static void verifySuccessfullyLoggedOffMessage() throws Throwable {
		UI.verifyElementContainsPartialText("My Account Message", Repository.accountPage_Message, "You have been logged off your account. It is now safe to leave the computer.");
	}
	
	public static void verifySuccessfullyCreatedAccountMessage() throws Throwable {
		UI.verifyElementContainsPartialText("My Account Message", Repository.accountPage_Message, "Congratulations! Your new account has been successfully created!");
	}
}
