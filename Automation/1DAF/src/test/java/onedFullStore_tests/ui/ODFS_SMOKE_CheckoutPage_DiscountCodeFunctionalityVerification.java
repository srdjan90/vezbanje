package onedFullStore_tests.ui;

import logic.BaseTest;
import logic.Browser;
import logic.Environments;
import onedFullStore.ui.pages.ODFS_HomePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static utils.Utils.initWaits;

public class ODFS_SMOKE_CheckoutPage_DiscountCodeFunctionalityVerification extends BaseTest {

    private WebDriver driver;
    private WebDriverWait wait;
    private ODFS_HomePage homePage;

    @BeforeClass(alwaysRun = true)
    public void setUp(){
        driver = Browser.PROPERTY.getInstance();
        wait = initWaits(driver, 10, 30, 1);
        driver.get(Environments.ODFS_UI_URL);
        homePage = new ODFS_HomePage(driver, wait);
    }

    @Test
    public void discountCodeFunctionalityVerification() throws Exception{
        homePage.header.openSignIn()
                .login("stefan-191018@srb.hostopia.com", "Deluxe@123")
                .header.openCart()
                .deleteAllItems();
//                .header.searchFor("shirts")
//                .clickProductByProductId("5736")
//                .selectProductColor("Yellow, Cornsilk")
//                .clickAddToCart()
//                .enterLargeQuantity(12)
//                .choseItemColor("Blue, Carolina")
//                .choseDecorationMethod("1 Color Screen Print - 1 Location")
//                .choseImprintLocation("Back - Center")
//                .choseImprintColor("Black")
//                .clickAddToCart()
//                .proceedToCheckout()
//                .applyTESTORDERCoupon()
//                .applyDiscount("kc656")
//                .applyInvalidDiscount("dasadas");
    }

    @AfterClass(alwaysRun = true)
    public void tearDown(){
        driver.quit();
    }
}
