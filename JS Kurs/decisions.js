/*
let count = 3;
if (count == 4) {
    console.log('Count is 4');
} else if (count > 4) {
    console.log('Count is greater than 4');
} else if (count < 4) {
    console.log('Count is less than 4');
} else {
    console.log('Count is NOT 4');  // This will never be executed, because all conditions have been taken above.
};
*/

// It will execute all cases:

// let hero = 'superman';
// switch (hero) {
//     case 'superman':
//         console.log('super strenght');
//         console.log('x-ray vision');
//     case 'batman':
//         console.log('intelligance');
//         console.log('fighting skills');
//     default:
//         console.log('member of JLA');
// }


// It will execute only the case that match and then will exit from the flow:

// let hero = 'batman';
// switch (hero) {
//     case 'superman':
//         console.log('super strenght');
//         console.log('x-ray vision');
//         break;
//     case 'batman':
//         console.log('intelligance');
//         console.log('fighting skills');
//         break;
//     default:
//         console.log('member of JLA');
// }



// To change all to lowercase letters and than to do the match as above.

// let hero = 'Batman';
// switch (hero.toLowerCase()) {
//     case 'superman':
//         console.log('super strenght');
//         console.log('x-ray vision');
//         break;
//     case 'batman':
//         console.log('intelligance');
//         console.log('fighting skills');
//         break;
//     default:
//         console.log('member of JLA');
// }


// Ternary operator:

// let a = 1, b = '1';
// let result = (a == b) ? 'equal' : 'inequal';
// console.log(result);

// console.log((a == b) ? 'equal' : 'inequal');     // insted of "result" we can check like this as well 



// let a = 1, b = '1';
// let result = (a === b) ? 'equal' : 'inequal';   // checking the data type and the equality
// console.log(result);


// let a = 1, b = '1';
// let result = (a != b) ? 'not equal' : 'equal';  // not eaual sign !=
// console.log(result);


// let a = 1, b = '1';
// let result = (a !== b) ? 'not equal' : 'equal';   // strictly not eaual !==
// console.log(result);