/*
 * Created by ekarliuchenko on 9/27/18 5:28 PM
 */

package onedStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.stream.Collectors;


import static org.testng.AssertJUnit.assertTrue;

public class ODS_ProductsListingPage extends BasePage {

    @FindBy(css = ".resultsGrid h5 > a") private List<WebElement> linksProducts;
    @FindBy(css = "#filters div[id*='fh']") private List<WebElement> lblFilters;
    @FindBy(css = "#filters div[id*='Group'] a") private List<WebElement> linksFilters;

    public ODS_ProductsListingPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public ODS_ProductPage openFirstProduct() {
        clickWhenReady(linksProducts.get(0));
        return new ODS_ProductPage(driver, wait);
    }

    public ODS_ProductPage openProduct(String productName){
        clickWhenReady(linksProducts.stream().filter(webElement -> webElement.getText().equals(productName)).findFirst().orElse(null));
        return new ODS_ProductPage(driver, wait);
    }

    public ODS_ProductsListingPage checkSearchedProduct(String searchedProduct) {
        assertTrue("There's no searched product retrieved in the list!", linksProducts.stream().
                anyMatch(webElement -> webElement.getText().equals(searchedProduct)));
        return this;
    }

    public ODS_ProductsListingPage expandAllFilters(){
        for (WebElement element : waitForVisible(lblFilters)){
            clickWhenReady(element);
        }
        return this;
    }

    public ODS_ProductsListingPage expandFilters(String filterName){
        clickWhenReady(lblFilters.stream().filter(webElement -> webElement.getText().equals(filterName)).findFirst().orElse(null));
        return this;
    }

    public ODS_ProductsListingPage applyFilter(String filterName){
        clickWhenReady(linksFilters.stream().filter(webElement -> webElement.getText().equals(filterName)).findFirst().orElse(null));
        return this;
    }

    public List<String> getProductNames() {
        return linksProducts.stream().map(WebElement::getText).collect(Collectors.toList());
    }

    public String getFirstProductName() {
        return waitForVisible(linksProducts.get(0)).getText();
    }
}
