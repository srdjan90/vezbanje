package TheNinjaStore_Pages;

import java.io.IOException;

import Base.GlobalVariables;
import Base.UI;
import TheNinjaStore_Repository.Repository;

public class RegisterPage {

	public static void enterTextIntoFirstNameField(String valueToEnter) throws IOException {
		UI.enterTextIntoElement("First Name Field", Repository.registerPage_FirstNameField, valueToEnter);
	}
	
	public static void enterTextIntoLastNameField(String valueToEnter) throws IOException {
		UI.enterTextIntoElement("Last Name Field", Repository.registerPage_LastNameField, valueToEnter);
	}
	
	public static void enterTextIntoEmailField(String valueToEnter) throws IOException {
		UI.enterTextIntoElement("Email Field", Repository.registerPage_EmailField, valueToEnter);
	}
	
	public static void enterTextIntoTelephoneField(String valueToEnter) throws IOException {
		UI.enterTextIntoElement("Telephone Field", Repository.registerPage_TelephoneField, valueToEnter);
	}
	
	public static void enterTextIntoPasswordField(String valueToEnter) throws IOException {
		UI.enterTextIntoElement("Password Field", Repository.registerPage_PasswordField, valueToEnter);
	}
	
	public static void enterTextIntoConfirmPasswordField(String valueToEnter) throws IOException {
		UI.enterTextIntoElement("Confirm Password Field", Repository.registerPage_PasswordConfirmationField, valueToEnter);
	}
	
	public static void clickContinueButton() throws IOException {
		UI.clickElement("Continue Button", Repository.registerPage_ContinueButton);
	}
	
	public static void clickLoginButton() throws IOException {
		UI.clickElement("Login Button", Repository.registerPage_LoginButton);
	}
	
	public static void clickRegisterButton() throws IOException {
		UI.clickElement("Register Button", Repository.registerPage_RegisterButton);
	}
	
	public static void clickForgottenPasswordButton() throws IOException {
		UI.clickElement("Forgotten Password Button", Repository.registerPage_ForgottenPasswordButton);
	}
	
	public static void clickMyAccountButton() throws IOException {
		UI.clickElement("My Account Button", Repository.registerPage_MyAccountButton);
	}
	
	public static void clickAddressBookButton() throws IOException {
		UI.clickElement("Address BookButton", Repository.registerPage_AddressBookButton);
	}
	
	public static void clickWishListButton() throws IOException {
		UI.clickElement("Wish List Button", Repository.registerPage_WishListButton);
	}
	
	public static void clickOrderHistoryButton() throws IOException {
		UI.clickElement("Order History Button", Repository.registerPage_OrderHistoryButton);
	}
	
	public static void clickDownloadsButton() throws IOException {
		UI.clickElement("Downloads Button", Repository.registerPage_DownloadsButton);
	}
	
	public static void clickRecurringPaymentsButton() throws IOException {
		UI.clickElement("Recurring payments Button", Repository.registerPage_RecurringPaymentsButton);
	}
	
	public static void clickRewardPointsButton() throws IOException {
		UI.clickElement("Reward Points Button", Repository.registerPage_RewardPointsButton);
	}
	
	public static void clickReturnsButton() throws IOException {
		UI.clickElement("Returns Button", Repository.registerPage_ReturnsButton);
	}
	
	public static void clickTransactionsButton() throws IOException {
		UI.clickElement("Transactions Button", Repository.registerPage_TransactionsButton);
	}
	
	public static void clickNewsletterButton() throws IOException {
		UI.clickElement("Newsletter Button", Repository.registerPage_NewsletterButton);
	}
	
	public static void verifyErrorMessage(String errorMessage) throws Throwable {
		UI.verifyElementContainsPartialText("Error Box", Repository.registerPage_ErrorMessageBox, errorMessage);
	}
	
	public static void verifyFirstNameErrorMessage() throws Throwable {
		UI.verifyElementContainsPartialText("First Name Field Error Message", Repository.registerPage_FirstNameFieldErrorMessage, "First Name must be between 1 and 32 characters!");
	}
	
	public static void verifyLastNameErrorMessage() throws Throwable {
		UI.verifyElementContainsPartialText("Last Name Field Error Message", Repository.registerPage_LastNameFieldErrorMessage, "Last Name must be between 1 and 32 characters!");
	}
	
	public static void checkPrivacyPolicyCheckBox() throws IOException {
		UI.selectCheckBoxIfUnselected("Privacy Policy CheckBox", Repository.registerPage_PrivacyPolicyCheckbox);
	}
	
	public static void uncheckPrivacyPolicyChechkBox() throws IOException {
		UI.unselectCheckBoxIfSelected("Privacy Policy CheckBox", Repository.registerPage_PrivacyPolicyCheckbox);
	}
	
	public static void verifyEmptyFormFieldErrors() throws Throwable {
		verifyFirstNameErrorMessage();
		verifyLastNameErrorMessage();
		verifyEMailErrorMessage();
		verifyTelephoneErrorMessage();
		verifyPasswordErrorMessage();
	}
	
	public static void enterGenericInformation() throws IOException {
		enterTextIntoFirstNameField(GlobalVariables.firstName);
		enterTextIntoLastNameField(GlobalVariables.lastName);
		enterTextIntoEMailField(GlobalVariables.eMail);
		enterTextIntoTelephoneField(GlobalVariables.telephoneNumber);
		enterTextIntoPasswordField(GlobalVariables.password);
		enterTextIntoPasswordConfirmationField(GlobalVariables.password);
	}
	
	public static void enterTextIntoEMailField(String value) throws IOException {
		UI.enterTextIntoElement("E-Mail Field", Repository.registerPage_EMailField, value);
	}
	
	public static void enterTextIntoPasswordConfirmationField(String value) throws IOException {
		UI.enterTextIntoElement("Password Confirmation Field", Repository.registerPage_PasswordConfirmationField, value);
	}
	
	public static void verifyEMailErrorMessage() throws Throwable {
		UI.verifyElementContainsPartialText("E-Mail Field Error Message", Repository.registerPage_EMailFieldErrorMessage, "E-Mail Address does not appear to be valid!");
	}
	
	public static void verifyTelephoneErrorMessage() throws Throwable {
		UI.verifyElementContainsPartialText("Telephone Field Error Message", Repository.registerPage_TelephoneFieldErrorMessage, "Telephone must be between 3 and 32 characters!");
	}
	
	public static void verifyPasswordErrorMessage() throws Throwable {
		UI.verifyElementContainsPartialText("Password Field Error Message", Repository.registerPage_PasswordFieldErrorMessage, "Password must be between 4 and 20 characters!");
	}
	
	public static void verifyPasswordConfirmationErrorMessage() throws Throwable {
		UI.verifyElementContainsPartialText("Password Field Error Message", Repository.registePage_PasswordConfirmationFieldErrorMessage, "Password confirmation does not match password!");
	}
	
}
