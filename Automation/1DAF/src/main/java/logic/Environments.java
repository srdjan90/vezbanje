package logic;

public final class Environments {

	private Environments(){}

	public static final String DEFAULT_UI_ENV = "prod";
	private static final String OD_DEFAULT_UI_ENV = DEFAULT_UI_ENV;
	private static final String ODS_DEFAULT_UI_ENV = DEFAULT_UI_ENV;
	private static final String ODFS_DEFAULT_UI_ENV = DEFAULT_UI_ENV;

	private static final String[][] ODEnvs = {
			{"https://1d-dev.deluxe.com"},     //dev
			{"https://1d-stage.deluxe.com"},   //stage | default
			{"https://1d-prod.deluxe.com"},    //prod
	};

	private static final String[][] ODSEnvs = {
			{"https://1dbeta.deluxe.com/store/"},  //dev
			{"https://1drc.deluxe.com/store/"},    //stage | default
			{"https://www.deluxe.com/store/"},     //prod
	};

	private static final String[][] ODFSEnvs = {
			{"https://1dfullstorebeta.deluxe.com/products/custom/"},  	//dev
			{"https://1dfullstorerc.deluxe.com/products/custom/"},   	//stage | default
			{"https://www.deluxe.com/products/custom/"},    			//prod
	};

	public static final String OD_UI_URL = ODEnvs[getUiEnvIndex()][0];
	public static final String ODS_UI_URL = ODSEnvs[getUiEnvIndex()][0];
	public static final String ODFS_UI_URL = ODFSEnvs[getUiEnvIndex()][0];

	private static int getUiEnvIndex() {
		switch (System.getProperty("ui_env", DEFAULT_UI_ENV)) {
			case "dev":
				return 0;
			case "stage":
				return 1;
			case "prod":
				return 2;
			default:
				return 1;
		}
	}
}
