package onedFullStore.ui.pages;

import logic.BasePage;
import onedStore.ui.pages.ODS_MyAccountPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ODFS_MyAccountPage extends BasePage {


    @FindBy(how = How.XPATH, using = "//a[contains(@href,'profileemail')]")
    private WebElement btnChangeEmailAddress;

    @FindBy(how = How.XPATH, using = "//a[contains(@href,'profilepassword')]")
    private WebElement btnChangePassword;

    @FindBy(how = How.XPATH, using = "//a[contains(@href,'addresses')]")
    private WebElement btnShippingAddress;

    @FindBy(how = How.XPATH, using = "//a[contains(@href,'profileinfo')]")
    private WebElement btnBillingInformation;

    @FindBy(how = How.XPATH, using = "//a[contains(@href,'artwork')]")
    private WebElement btnYourImages;

    @FindBy(how = How.XPATH, using = "//a[contains(@href,'wishlist') and contains(.,'Your Wish Lists')]")
    private WebElement btnYourWishLists;

    @FindBy(how = How.XPATH, using = "//a[contains(@href,'reviews') and contains(.,'Your Product Reviews')]")
    private WebElement btnYourProductPreviews;

    @FindBy(how = How.XPATH, using = "//a[contains(.,'Open Orders')]")
    private WebElement tabOpenOrders;

    @FindBy(how = How.XPATH, using = "//a[contains(.,'Completed Orders')]")
    private WebElement tabCompletedOrders;

    @FindBy(how = How.XPATH, using = "//a[contains(.,'Samples') and contains(@href,'javascript')]")
    private WebElement tabSamples;

    @FindBy(how = How.XPATH, using = "//input[@id='q']")
    private WebElement txtProductName;

    @FindBy(how = How.XPATH, using = "//input[@id='order_id']")
    private WebElement txtOrderId;

    @FindBy(how = How.XPATH, using = "//input[@id='begin_date']")
    private WebElement txtBeginDate;

    @FindBy(how = How.XPATH, using = "//input[@id='end_date']")
    private WebElement txtEndDate;

    @FindBy(how = How.XPATH, using = "//input[@type='submit' and @value='Search']")
    private WebElement btnSearch;


    public ODFS_MyAccountPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }



    public ODFS_ChangeYourEmailAddressPage clickChangeEmailAddress(){
        clickWhenReady(btnChangeEmailAddress);
        return new ODFS_ChangeYourEmailAddressPage(driver, wait);
    }

    public ODFS_ChangeYourPasswordPage clickChangePassword(){
        clickWhenReady(btnChangePassword);
        return new ODFS_ChangeYourPasswordPage(driver, wait);
    }

    public ODFS_ManageShippingAddressPage clickShippingAddresses(){
        clickWhenReady(btnShippingAddress);
        return new ODFS_ManageShippingAddressPage(driver, wait);
    }

    public ODFS_ManageContactInformationPage clickBillingInformation(){
        clickWhenReady(btnBillingInformation);
        return new ODFS_ManageContactInformationPage(driver, wait);
    }

    public ODFS_YourImagesAndProofsPage clickYourImages(){
        clickWhenReady(btnYourImages);
        return new ODFS_YourImagesAndProofsPage(driver, wait);
    }

    public ODFS_WishlistPage clickYourWishLists(){
        clickWhenReady(btnYourWishLists);
        return new ODFS_WishlistPage(driver, wait);
    }

    public ODFS_ReviewsPage clickYourProductReview(){
        clickWhenReady(btnYourProductPreviews);
        return new ODFS_ReviewsPage(driver, wait);
    }

    public ODFS_MyAccountPage clickOpentOrders(){
        clickWhenReady(tabOpenOrders);
        return this;
    }

    public ODFS_MyAccountPage clickCompletedOrders(){
        clickWhenReady(tabCompletedOrders);
        return this;
    }

    public ODFS_MyAccountPage clickSamples(){
        clickWhenReady(tabSamples);
        return this;
    }

    public ODFS_MyAccountPage enterProductName(String productName){
        typeIn(txtProductName, productName);
        return this;
    }

    public ODFS_MyAccountPage enterOrderId(String orderId){
        typeIn(txtOrderId, orderId);
        return this;
    }

    public ODFS_MyAccountPage enterBeginDate(String beginDate){
        typeIn(txtBeginDate, beginDate);
        return this;
    }

    public ODFS_MyAccountPage enterEndDate(String endDate){
        typeIn(txtEndDate, endDate);
        return this;
    }

    public ODFS_MyAccountPage clickSearch(){
        clickWhenReady(btnSearch);
        return this;
    }

}
