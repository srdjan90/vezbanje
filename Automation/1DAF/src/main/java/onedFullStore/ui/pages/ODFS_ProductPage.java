package onedFullStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;

public class ODFS_ProductPage extends BasePage {


    @FindBy(how = How.XPATH, using = "//h1[@itemprop='name']")
    private WebElement lblProductName;

    @FindBy(how = How.XPATH, using = "//div[@class='col-sm-7']/strong")
    private WebElement lblProductID;

    @FindBy(how = How.XPATH, using = "//div[@class='col-sm-7']/span[@class='productNumber']")
    private WebElement lblProductSku;

    @FindBy(how = How.XPATH, using = "//button[@data-id='sel_item_color']")
    private WebElement btnChooseColor;

    @FindAll(@FindBy(how = How.XPATH, using = "//ul[@class='dropdown-menu inner']/li[not(@class)]/a"))
    private List<WebElement> ddlProductColors;

    @FindBy(how = How.XPATH, using = "//button[contains(.,'Design & Buy') and @class='btn btn-primary btn-lg btn-block']")
    private WebElement btnDesignAndBuy;

    @FindBy(how = How.XPATH, using = "//button[contains(.,'Add to Cart') and @class='btn btn-default btn-block']")
    private WebElement btnAddToCart;

    @FindBy(how = How.XPATH, using = "//button[contains(.,'Quick Estimate')]")
    private WebElement btnQuickEstimate;

    @FindBy(how = How.XPATH, using = "//button[@id='button-quote']")
    private WebElement btnRequestQuote;

    @FindBy(how = How.XPATH, using = "//button[@id='button-wishlist-yes-log']")
    private WebElement btnSaveToWishlist;

    @FindBy(how = How.XPATH, using = "//button[contains(@onclick,'AddToComparisons')]")
    private WebElement btnCompareProduct;

    @FindBy(how = How.XPATH, using = "//div[@class='text-center hidden-xs']/a/span[@class='glyphicon glyphicon-envelope']")
    private WebElement btnEmail;

    @FindBy(how = How.XPATH, using = "//div[@class='text-center hidden-xs']/a/span[@class='glyphicon glyphicon-print']")
    private WebElement btnPrint;

    @FindBy(how = How.XPATH, using = "//button[@id='zoom_btn']")
    private WebElement linkClickImageToZoom;

    @FindBy(how = How.XPATH, using = "//ul[@class='nav nav-pills nav-justified']/li/a[contains(.,'Description')]")
    private WebElement linkDescription;

    @FindBy(how = How.XPATH, using = "//ul[@class='nav nav-pills nav-justified']/li/a[contains(.,'Colors & Options')]")
    private WebElement linkColorAndOptions;

    @FindBy(how = How.XPATH, using = "//ul[@class='nav nav-pills nav-justified']/li/a[contains(.,'Additional Information')]")
    private WebElement linkAdditionalInformation;

    @FindBy(how = How.XPATH, using = "//ul[@class='nav nav-pills nav-justified']/li/a[contains(.,'FAQs')]")
    private WebElement linkFAQs;

    @FindBy(how = How.XPATH, using = "//ul[@class='nav nav-pills nav-justified']/li/a[contains(.,'Reviews')]")
    private WebElement linkReviews;

    @FindBy(how = How.XPATH, using = "//input[@id='questionStr']")
    private WebElement txtQuestionAndAnswer;

    @FindBy(how = How.XPATH, using = "//span[contains(.,'Ask Question') and not(@class)]")
    private WebElement btnAskQuestion;

    public ODFS_ProductPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    /**
     *  METHODS
     */

    public ODFS_ProductPage verifyProductName(String productName){
        Assert.assertEquals(getTextFromElement(lblProductName), productName, "Product Name is not correct on ProductPage. Test Failed!");
        return this;
    }

    public ODFS_ProductPage verifyProductID(int productID){
        Assert.assertEquals(getTextFromElement(lblProductID).substring(6), String.valueOf(productID), "ProductID is not correct on ProductPage. Test Failed!");
        return this;
    }

    public ODFS_ProductPage verifyProductSKU(String productSKU){
        Assert.assertEquals(getTextFromElement(lblProductSku).substring(4), productSKU, "ProductSKU in not correct on ProductPage. Test Failed!");
        return this;
    }

    private ODFS_ProductPage clickProductColorsDropdown(){
        clickWhenReady(btnChooseColor);
        return this;
    }

    private ODFS_ProductPage choseProductColor(String colorName){
        Actions builder = new Actions(driver);
        for(WebElement w : ddlProductColors){
            if(getTextFromElement(w).contains(colorName)){
                builder.moveToElement(w).build().perform();
                clickWhenReady(w);
                break;
            }
        }
        return this;
    }

    public ODFS_ProductPage selectProductColor(String colorName){
        this.clickProductColorsDropdown()
                .choseProductColor(colorName);

        return this;
    }

    public ODFS_DesignPage clickDesignAndBuy(){
        clickWhenReady(btnDesignAndBuy);
        return new ODFS_DesignPage(driver, wait);
    }

    public ODFS_CustomizePage clickAddToCart(){
        clickWhenReady(btnAddToCart);
        return new ODFS_CustomizePage(driver, wait);
    }

    public ODFS_ProductPage clickQuickEstimate(){
        clickWhenReady(btnQuickEstimate);
        return this;
    }

    public ODFS_QuoteCustomizePage clickRequestQuote(){
        clickWhenReady(btnRequestQuote);
        return new ODFS_QuoteCustomizePage(driver, wait);
    }

    public ODFS_ProductPage clickSaveToWishList(){
        clickWhenReady(btnSaveToWishlist);
        return this;
    }

    public ODFS_ProductPage clickCompareProduct(){
        clickWhenReady(btnCompareProduct);
        return this;
    }

    public ODFS_ProductPage clickEmailButton(){
        clickWhenReady(btnEmail);
        return this;
    }

    public ODFS_ProductPage clickPrintButton(){
        clickWhenReady(btnPrint);
        return this;
    }

    public ODFS_ProductPage clickImageToZoom(){
        clickWhenReady(linkClickImageToZoom);
        return this;
    }

    public ODFS_ProductPage clickDescriptionLink(){
        clickWhenReady(linkDescription);
        return this;
    }

    public ODFS_ProductPage clickColorAndOptionsLink(){
        clickWhenReady(linkColorAndOptions);
        return this;
    }

    public ODFS_ProductPage clickAdditionalInformation(){
        clickWhenReady(linkAdditionalInformation);
        return this;
    }

    public ODFS_ProductPage clickFAQsLink(){
        clickWhenReady(linkFAQs);
        return this;
    }

    public ODFS_ProductPage enterQuestion(String question){
        typeIn(txtQuestionAndAnswer, question);
        return this;
    }

    public ODFS_ProductPage clickAskQuestion(){
        clickWhenReady(btnAskQuestion);
        return this;
    }


}
