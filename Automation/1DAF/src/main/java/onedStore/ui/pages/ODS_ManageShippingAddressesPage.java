package onedStore.ui.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import logic.BasePage;

public class ODS_ManageShippingAddressesPage extends BasePage{

	public ODS_ManageShippingAddressesPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
		// TODO Auto-generated constructor stub
	}

	@FindBy(xpath = "//a[contains(.,'New Address')]")
	WebElement btnAddAddress;

	@FindBy(xpath = "//a[contains(.,'Back to my account')]")
	WebElement lnkBackToAccount;

	@FindBy(xpath = "//a[contains(.,'Make Default Address')]")
	WebElement btnMakeDefault;

	@FindAll(@FindBy(xpath="//div[contains(@id, 'aid')]"))
	List<WebElement> savedAddresses;

	public ODS_ManageShippingAddressesPage clickDeleteAddress(String addressName) {
		for (WebElement e : savedAddresses) {
			if (e.findElement(By.xpath("./div/strong")).getText().contains(addressName)) {
				e.findElement(By.xpath("./div/strong/following-sibling::a[contains(.,'Delete')]")).click();
			}
		}
		return this;
	}

	public ODS_EditShippingAddressPage clickEditAddress(String addressName) {
		for (WebElement e : savedAddresses) {
			if (e.findElement(By.xpath("./div/strong")).getText().contains(addressName)) {
				e.findElement(By.xpath("./div/strong/following-sibling::a[contains(.,'Edit')]")).click();
			}
		}
		return new ODS_EditShippingAddressPage(driver, wait);
	}

}
