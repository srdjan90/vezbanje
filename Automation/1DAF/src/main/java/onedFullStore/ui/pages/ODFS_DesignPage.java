package onedFullStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ODFS_DesignPage extends BasePage {


    @FindBy(how = How.XPATH, using = "//button[@type='submit' and contains(.,'Add to Cart')]")
    private WebElement btnAddToCart;

    @FindBy(how = How.XPATH, using = "//button[@class='ihd-add-image btn btn-success btn-block']")
    private WebElement btnAddArtwork;

    @FindBy(how = How.XPATH, using = "//button[@class='ihd-add-clipart btn btn-success btn-block']")
    private WebElement btnAddClipart;

    @FindBy(how = How.XPATH, using = "//button[@class='ihd-add-text btn btn-success btn-block']")
    private WebElement btnAddText;

    @FindBy(how = How.XPATH, using = "//div[contains(@class,'hidden')]/div[@class='btn-group bootstrap-select ihd-items-wrapper w100 show-tick']/button")
    private WebElement btnColorsDropdown;

    @FindAll(@FindBy(how = How.XPATH, using = "//ul[@class='dropdown-menu inner' and @style='max-height: 260px; overflow-y: auto;']/li/a/div"))
    private List<WebElement> ddlColors;

    @FindBy(how = How.XPATH, using = "//a[@id='volume_pricing_btn']")
    private WebElement btnViewPricing;

    @FindBy(how = How.XPATH, using = "//input[@name='comments_129_5125']")
    private WebElement txtSmallSize;

    @FindBy(how = How.XPATH, using = "//input[@name='comments_129_5126']")
    private WebElement txtMediumSize;

    @FindBy(how = How.XPATH, using = "//input[@name='comments_129_5127']")
    private WebElement txtLargeSize;

    @FindBy(how = How.XPATH, using = "//input[@name='comments_129_5128']")
    private WebElement txtExtraLargeSize;

    @FindBy(how = How.XPATH, using = "//input[@name='comments_129_5129']")
    private WebElement txtTwoExtraLarge;

    @FindAll(@FindBy(how = How.XPATH, using = "//li[@class='list-group-item']//button[@class='btn dropdown-toggle btn-default']"))
    private WebElement btnDecorationMethod;

    @FindBy(how = How.XPATH, using = "//li[@class='list-group-item']//ul/li/a")
    private List<WebElement> ddlDecorationMethod;

    @FindBy(how = How.XPATH, using = "//textarea[@id='comments_13']")
    private WebElement txtImprintColorComments;

    @FindBy(how = How.XPATH, using = "//li[@class='list-group-item']/textarea[@id='comments']")
    private WebElement txtSpecialInstructions;

    @FindBy(how = How.XPATH, using = "//input[@id='clipart_query']")
    private WebElement txtClipartSearchField;

    @FindBy(how = How.XPATH, using = "//div[@class='input-group-btn']/button")
    private WebElement btnClipartSearch;

    @FindAll(@FindBy(how = How.XPATH, using = "//div[@class='modal-body']//div[@class='row']/div"))
    private List<WebElement> listCliparts;

    @FindBy(how = How.XPATH, using = "//button[@class='btn btn-primary btn-block ihd-done-editing']")
    private WebElement btnDoneEditing;

    @FindBy(how = How.XPATH, using = "//div[@class='panel panel-primary ihd-edit-elements nodisplay hidden-xs hidden-sm']//div[@class='btn-group bootstrap-select form-control ihd-color-select']")
    private WebElement btnTextColorOptions;

    @FindAll(@FindBy(how = How.XPATH, using = "//div[contains(@class,'dropdown-menu') and contains(@style,'532')]/ul/li//div"))
    private List<WebElement> ddlTextColorOptions;

    @FindBy(how = How.XPATH, using = "//div[@class='panel panel-primary ihd-edit-elements nodisplay hidden-xs hidden-sm']//textarea")
    private WebElement txtYourTextGoesHere;

    @FindBy(how = How.XPATH, using = "//input[@id='upload_cart_id']")
    private WebElement formArtworkUpload;


    public ODFS_DesignPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }


    public ODFS_DesignPage selectProductColor(String colorName){
        waitForSpinners();
        clickWhenReady(btnColorsDropdown);
        for(WebElement w : ddlColors){
            if(getTextFromElement(w).contains(colorName)){
                Actions builder = new Actions(driver);
                builder.moveToElement(w).build().perform();
                clickWhenReady(w);
                break;
            }
        }
        return this;
    }

    public ODFS_DesignPage clickAddArtwork(){
        waitForSpinners();
        clickWhenReady(btnAddArtwork);
        return this;
    }

    public ODFS_DesignPage clickAddClipart(){
        waitForSpinners();
        clickWhenReady(btnAddClipart);
        return this;
    }

    public ODFS_DesignPage clickAddText(){
        waitForSpinners();
        clickWhenReady(btnAddText);
        return this;
    }

    public ODFS_CartInterstitialPage clickAddToCart(){
        waitForSpinners();
        clickWhenReady(btnAddToCart);
        return new ODFS_CartInterstitialPage(driver, wait);
    }

    public ODFS_DesignPage clickViewPricing(){
        waitForSpinners();
        clickWhenReady(btnViewPricing);
        return this;
    }

    public ODFS_DesignPage setSmallSizeQuantity(int smallSizeQuantity){
        waitForSpinners();
        typeIn(txtSmallSize, String.valueOf(smallSizeQuantity));
        return this;
    }

    public ODFS_DesignPage setMediumSizeQuantity(int mediumSizeQuantity){
        waitForSpinners();
        typeIn(txtMediumSize, String.valueOf(mediumSizeQuantity));
        return this;
    }

    public ODFS_DesignPage setLargeSizeQuantity(int largeSizeQuantity){
        waitForSpinners();
        typeIn(txtLargeSize, String.valueOf(largeSizeQuantity));
        return this;
    }

    public ODFS_DesignPage setExtraLargeSizeQuantity(int extraLargeSizeQuantity){
        waitForSpinners();
        typeIn(txtExtraLargeSize, String.valueOf(extraLargeSizeQuantity));
        return this;
    }

    public ODFS_DesignPage setTwoExtraLargeSizeQuantity(int twoExtraLargeSizeQuantity){
        waitForSpinners();
        typeIn(txtTwoExtraLarge, String.valueOf(twoExtraLargeSizeQuantity));
        return this;
    }

    public ODFS_DesignPage setDecorationMethod(String decorationMethodName){
        waitForSpinners();
        clickWhenReady(btnDecorationMethod);
        for(WebElement w : ddlDecorationMethod){
            if(getTextFromElement(w).contains(decorationMethodName)){
                Actions builder = new Actions(driver);
                builder.moveToElement(w).build().perform();
                clickWhenReady(w);
                break;
            }
        }
        return this;
    }

    public ODFS_DesignPage enterImprintColorComments(String comments){
        waitForSpinners();
        typeIn(txtImprintColorComments, comments);
        return this;
    }

    public ODFS_DesignPage enterSpecialInstructions(String specialInstructions){
        waitForSpinners();
        typeIn(txtSpecialInstructions, specialInstructions);
        return this;
    }

    public ODFS_DesignPage clickDoneEdtigin(){
        clickWhenReady(btnDoneEditing);
        return this;
    }

    public ODFS_DesignPage addClipart(String clipartName) throws Exception {
        clickAddClipart();
        waitForModals();
        typeIn(txtClipartSearchField, clipartName);
        clickWhenReady(btnClipartSearch);
        Thread.sleep(1000);
        clickWhenReady(listCliparts.get(0));
        waitForSpinners();
        clickDoneEdtigin();
        return this;
    }

    public ODFS_DesignPage addText(String text, String textColor) throws Exception {
        waitForSpinners();
        clickWhenReady(btnAddText);
        clickWhenReady(btnTextColorOptions);
        for(WebElement w : ddlTextColorOptions){
            if(getTextFromElement(w).contains(textColor)){
                Actions builder = new Actions(driver);
                builder.moveToElement(w).build().perform();
                clickWhenReady(w);
                break;
            }
        }
        waitForSpinners();
        typeIn(txtYourTextGoesHere, text);
        clickWhenReady(btnDoneEditing);
        return this;
    }

    public ODFS_DesignPage addText(String text){
        waitForSpinners();
        clickWhenReady(btnAddText);
        typeIn(txtYourTextGoesHere, text);
        clickWhenReady(btnDoneEditing);
        return this;
    }

//    public ODFS_DesignPage addArtwork(){
//        waitForSpinners();
//        clickWhenReady(btnAddArtwork);
//        waitForModals();
//        //typeIn(formArtworkUpload, ".//src//main//resources//images//deluxelogo.png");
//        setAttribute(formArtworkUpload, "type", "visible");
//        formArtworkUpload.sendKeys(".//src//main//resources//images//deluxelogo.png");
//        formArtworkUpload.sendKeys(Keys.RETURN);
//        return this;
//    }

}
