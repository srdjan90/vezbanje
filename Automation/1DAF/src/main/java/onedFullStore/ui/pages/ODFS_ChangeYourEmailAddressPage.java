package onedFullStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ODFS_ChangeYourEmailAddressPage extends BasePage {

    public ODFS_ChangeYourEmailAddressPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @FindBy(css = "#newEmail") private WebElement txtNewEmail;
    @FindBy(css = "#existingPassword") private WebElement txtExistingPassword;
    @FindBy(css = "[value='Save Changes']") private WebElement btnSaveChanges;
    @FindBy(css = ".btn-default") private WebElement btnCancel;

    private ODFS_ChangeYourEmailAddressPage typeNewEmail(String inputText) {
        typeIn(txtNewEmail, inputText);
        return this;
    }

    private ODFS_ChangeYourEmailAddressPage typeExistingPassword(String inputText) {
        typeIn(txtExistingPassword, inputText);
        return this;
    }

    private ODFS_ChangeYourEmailAddressPage clickSaveChanges() {
        clickWhenReady(btnSaveChanges);
        return new ODFS_ChangeYourEmailAddressPage(driver, wait);
    }

    private ODFS_ChangeYourEmailAddressPage clickCancel() {
        clickWhenReady(btnCancel);
        return new ODFS_ChangeYourEmailAddressPage(driver, wait);
    }

    public ODFS_ChangeYourEmailAddressPage changeEmailAddress(String newEmail, String existingPassword) {
        typeNewEmail(newEmail).typeExistingPassword(existingPassword)
                              .clickSaveChanges();
        return this;
    }

}
