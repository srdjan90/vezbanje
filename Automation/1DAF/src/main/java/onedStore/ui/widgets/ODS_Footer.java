/*
 * Created by ekarliuchenko on 9/27/18 2:55 PM
 */

package onedStore.ui.widgets;

import onedStore.ui.pages.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import logic.BasePage;

import java.util.List;

public class ODS_Footer extends BasePage {

    @FindBy(css = ".global-footer .equal_col:first-child a")
    private List<WebElement> linksLearnAndSupport;
    @FindBy(css = ".global-footer .equal_col:nth-child(2) a")
    private List<WebElement> linksCustomProducts;
    @FindBy(css = ".contact_us a")
    private List<WebElement> lblsContactUs;

    public ODS_Footer(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    private enum LearnAndSupport {
        YOUR_ACCOUNT, CUSTOMER_CARE, FAQs, NO_RISK_GUARANTEE, TERMS_AND_CONDITIONS
    }

    private enum CustomProducts {
        PROMOTIONAL_PRODUCTS, APPAREL, MARKETING_MATERIALS
    }

    private enum ContactUs {
        PHONE, EMAIL_FIRST, EMAIL_SECOND
    }

    public ODS_MyAccountPage clickYourAccount() {
        clickWhenReady(linksLearnAndSupport.get(LearnAndSupport.YOUR_ACCOUNT.ordinal()));
        return new ODS_MyAccountPage(driver, wait);
    }

    public ODS_HelpPage clickCustomerCare() {
        clickWhenReady(linksLearnAndSupport.get(LearnAndSupport.CUSTOMER_CARE.ordinal()));
        return new ODS_HelpPage(driver, wait);
    }

    public ODS_HelpPage clickFAQs() {
        clickWhenReady(linksLearnAndSupport.get(LearnAndSupport.FAQs.ordinal()));
        return new ODS_HelpPage(driver, wait);
    }

    public ODS_ProductCategoriesPage clickPromotionalProducts() {
        clickWhenReady(linksCustomProducts.get(CustomProducts.PROMOTIONAL_PRODUCTS.ordinal()));
        return new ODS_ProductCategoriesPage(driver, wait);
    }

    public ODS_ProductsListingPage clickApparel() {
        clickWhenReady(linksCustomProducts.get(CustomProducts.APPAREL.ordinal()));
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage clickMarketingMaterials() {
        clickWhenReady(linksCustomProducts.get(CustomProducts.APPAREL.ordinal()));
        return new ODS_ProductsListingPage(driver, wait);
    }

    public String getContactUsPhone() {
        return waitForVisible(lblsContactUs.get(ContactUs.PHONE.ordinal())).getText();
    }

    public String getContactUsEmailFirst() {
        return waitForVisible(lblsContactUs.get(ContactUs.EMAIL_FIRST.ordinal())).getText();
    }

    public String getContactUsEmailSecond() {
        return waitForVisible(lblsContactUs.get(ContactUs.EMAIL_SECOND.ordinal())).getText();
    }
}
