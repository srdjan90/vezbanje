/*
 * Created by ekarliuchenko on 10/17/18 5:12 PM
 */

package onedFullStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ODFS_ResetPasswordPage extends BasePage {

    @FindBy(css = "#email") private WebElement txtEmail;
    @FindBy(css = "#form_reset .btn-primary") private WebElement btnResetPassword;
    @FindBy(css = "/products/custom/") private WebElement btnContinue;
    @FindBy(how = How.XPATH, using = "//div[@class='rc-anchor-center-item rc-anchor-checkbox-holder']")
    private WebElement checkboxCapture;


    public ODFS_ResetPasswordPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public ODFS_ResetPasswordPage enterEmail(String email){
        typeIn(txtEmail, email);
        return this;
    }

    public ODFS_ResetPasswordPage resetPassword(){
        clickWhenReady(btnResetPassword);
        return this;
    }


    public ODFS_ResetPasswordPage clickContinue(){
        clickWhenReady(btnContinue);
        return this;
    }

    public ODFS_ResetPasswordPage WithoutEnteringEmail () {
        txtEmail.clear();
        resetPassword();
        if (txtEmail.getAttribute("class").contains("error")) {
            System.out.println("Email polje nema error u klasi");
        }
        return this;
    }

    public ODFS_ResetPasswordPage ClickOnCapture () {
        clickWhenReady(checkboxCapture);
        return this;
    }

//    public ODFS_ResetPasswordPage WithInvalidEmail (String invalidEmail) {
//        this.enterEmail(invalidEmail)
//            .ClickOnCapture()                 //Nije moguce zbog Capture-a
//            .btnResetPassword.click();
//        return this;
//    }


}
