package onedStore.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;

import logic.BasePage;
import org.testng.Assert;


public class ODS_CheckoutPage extends BasePage {

	public ODS_CheckoutPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
		
	}

	/**
	 * Elements
	 */

	@FindBy(how = How.XPATH, using = "//a[contains(@href,'/store/cart')]")
	private WebElement btnBackToCart;

	@FindBy(how = How.XPATH, using = "//a[contains(.,'Edit')]")
	private WebElement linkEdit;

	@FindBy(how = How.XPATH, using = "//a[contains(.,' Choose a saved shipping address')]")
	private WebElement linkChooseASavedShippingAddress;

	@FindBy(how = How.XPATH, using = "//a[contains(.,' Add a new shipping address')]")
	private WebElement linkAddANewShippingAddress;

	@FindBy(how = How.XPATH, using = "//select[@id='sel_buying_reason']")
	private WebElement ddlSelectBuyingReason;

	@FindBy(how = How.XPATH, using = "//input[@id='txt_couponcode']")
	private WebElement txtDiscountCode;

	@FindBy(how = How.XPATH, using = "//button[@name='btn_coupon']")
	private WebElement btnApplyDiscount;

	@FindBy(how = How.XPATH, using = "//button[contains(.,'Add Credit Card')]")
	private WebElement btnAddCreditCard;

	@FindBy(how = How.XPATH, using = "//button[@class='btn btn-primary btn-lg w100']")
	private WebElement btnSubmitOrder;

	@FindBy(how = How.XPATH, using = "//button[contains(.,'Ship to this address')]")
	private WebElement btnShipToThisAddress;

	@FindBy(how = How.XPATH, using = "//a[contains(.,'Same as billing')]")
	private WebElement linkSameAsBilling;

	@FindBy(how = How.XPATH, using = "//div[@id='address_msg']")
	private WebElement lblShippingAddressUpdated;

	@FindBy(how = How.XPATH, using = "//select[@id='sel_shipping_country']")
	private WebElement ddlShippingAddressCountry;

	@FindBy(how = How.XPATH, using = "//input[@name='txt_shipping_firstname']")
	private WebElement txtFirstName;

	@FindBy(how = How.XPATH, using = "//input[@name='txt_shipping_lastname']")
	private WebElement txtLastName;

	@FindBy(how = How.XPATH, using = "//input[@name='txt_shipping_company']")
	private WebElement txtCompany;

	@FindBy(how = How.XPATH, using = "//input[@name='txt_shipping_address1']")
	private WebElement txtAddress1;

	@FindBy(how = How.XPATH, using = "//input[@name='txt_shipping_address2']")
	private WebElement txtAddress2;

	@FindBy(how = How.XPATH, using = "//input[@name='txt_shipping_address3']")
	private WebElement txtAddress3;

	@FindBy(how = How.XPATH, using = "//input[@name='txt_shipping_city']")
	private WebElement txtCity;

	@FindBy(how = How.XPATH, using = "//select[@id='sel_shipping_state']")
	private WebElement ddlState;

	@FindBy(how = How.XPATH, using = "//input[@name='txt_shipping_zip']")
	private WebElement txtZipCode;

	@FindBy(how = How.XPATH, using = "//input[@id='chk_shipping_save']")
	private WebElement chbSaveThisAddress;

	@FindBy(how = How.XPATH, using = "//input[@name='cardholder_name']")
	private WebElement txtCardholderName;

	@FindBy(how = How.XPATH, using = "//input[@name='credit-card-number']")
	private WebElement txtCardNumber;

	@FindBy(how = How.XPATH, using = "//input[@name='expiration-month']")
	private WebElement txtExpirationMonth;

	@FindBy(how = How.XPATH, using = "//input[@name='expiration-year']")
	private WebElement txtExpirationYear;

	@FindBy(how = How.XPATH, using = "//input[@name='cvv']")
	private WebElement txtCvv;

	@FindBy(how = How.XPATH, using = "//input[@name='postal-code']")
	private WebElement txtCardZipCode;

	@FindBy(how = How.XPATH, using = "//button[contains(.,'Cancel')]")
	private WebElement btnCancel;

	@FindBy(how = How.XPATH, using = "//button[contains(.,'Add Card')]")
	private WebElement btnAddCard;

	/**
	 * Methods
	 */

	public ODS_CartPage clickBackToCart(){
		clickWhenReady(btnBackToCart);
		return new ODS_CartPage(driver, wait);
	}

	public ODS_CheckoutPage clickEditAddress(){
		clickWhenReady(linkEdit);
		return this;
	}

	public ODS_CheckoutPage clickChooseASavedShippingAddress(){
		clickWhenReady(linkChooseASavedShippingAddress);
		return this;
	}

	public ODS_CheckoutPage clickAddANewShippingAddress(){
		clickWhenReady(linkAddANewShippingAddress);
		return this;
	}

	public ODS_CheckoutPage selectBuyingReason(String value){
		chooseValueFromSelect(ddlSelectBuyingReason, value);
		return this;
	}

	public ODS_CheckoutPage enterDiscountCode(String discountCode){
		typeIn(txtDiscountCode, discountCode);
		return this;
	}

	public ODS_CheckoutPage clickApplyDiscount(){
		clickWhenReady(btnApplyDiscount);
		return this;
	}

	public ODS_CheckoutPage clickAddCreditCart(){
		clickWhenReady(btnAddCreditCard);
		return this;
	}

	public ODS_OrderConfirmationPage clickSubmitOrder(){
		clickWhenReady(btnSubmitOrder);
		return new ODS_OrderConfirmationPage(driver, wait);
	}

	public ODS_CheckoutPage clickShipToThisAddress(){
		clickWhenReady(btnShipToThisAddress);
		return this;
	}

	public ODS_CheckoutPage clickSameAsBilling(){
		clickWhenReady(linkSameAsBilling);
		Assert.assertEquals(getTextFromElement(lblShippingAddressUpdated), "SHIPPING ADDRESS UPDATED", "Shipping address hasn't been successfully updated. Test Failed!");
		return this;
	}

	public ODS_CheckoutPage selectShippingCountry(String value){
		chooseValueFromSelect(ddlShippingAddressCountry, value);
		return this;
	}

	public ODS_CheckoutPage enterFirstName(String firstName){
		typeIn(txtFirstName, firstName);
		return this;
	}

	public ODS_CheckoutPage enterLastName(String lastName){
		typeIn(txtLastName, lastName);
		return this;
	}

	public ODS_CheckoutPage enterCompany(String company){
		typeIn(txtCompany, company);
		return this;
	}

	public ODS_CheckoutPage enterAddress1(String address1){
		typeIn(txtAddress1, address1);
		return this;
	}

	public ODS_CheckoutPage enterAddress2(String address2){
		typeIn(txtAddress2, address2);
		return this;
	}

	public ODS_CheckoutPage enterAddress3(String address3){
		typeIn(txtAddress3, address3);
		return this;
	}

	public ODS_CheckoutPage enterCity(String city){
		typeIn(txtCity, city);
		return this;
	}

	public ODS_CheckoutPage selectState(String value){
		chooseValueFromSelect(ddlState, value);
		return this;
	}

	public ODS_CheckoutPage enterZipCode(String zipCode){
		typeIn(txtZipCode, zipCode);
		return this;
	}

	public ODS_CheckoutPage checkSaveThisAddress(){
		clickWhenReady(chbSaveThisAddress);
		return this;
	}

	public ODS_CheckoutPage enterCardholderName(String cardholderName){
		typeIn(txtCardholderName, cardholderName);
		return this;
	}

	public ODS_CheckoutPage enterCardNumber(String cardNumber){
		driver.switchTo().frame("braintree-hosted-field-number");
		typeIn(txtCardNumber, cardNumber);
		driver.switchTo().defaultContent();
		return this;
	}

	public ODS_CheckoutPage enterCardExpirationMonth(String month){
		driver.switchTo().frame("braintree-hosted-field-expirationMonth");
		typeIn(txtExpirationMonth, month);
		driver.switchTo().defaultContent();
		return this;
	}

	public ODS_CheckoutPage enterCardExpirationYear(String year){
		driver.switchTo().frame("braintree-hosted-field-expirationYear");
		typeIn(txtExpirationYear, year);
		driver.switchTo().defaultContent();
		return this;
	}

	public ODS_CheckoutPage enterCVV(String cvv){
		driver.switchTo().frame("braintree-hosted-field-cvv");
		typeIn(txtCvv, cvv);
		driver.switchTo().defaultContent();
		return this;
	}

	public ODS_CheckoutPage enterCardZipCode(String zipCode){
		driver.switchTo().frame("braintree-hosted-field-postalCode");
		typeIn(txtCardZipCode, zipCode);
		driver.switchTo().defaultContent();
		return this;
	}

	public ODS_CheckoutPage clickCancel(){
		clickWhenReady(btnCancel);
		return this;
	}

	public ODS_CheckoutPage clickAddCard(){
		clickWhenReady(btnAddCard);
		return this;
	}
}
