/***************************************************************************************************
 Created by vskyba on 9/26/18 7:06 PM
 **************************************************************************************************/

package onedStore.ui.pages;

import onedStore.ui.widgets.ODS_Footer;
import onedStore.ui.widgets.ODS_Header;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import logic.BasePage;

import java.util.List;

public class ODS_HomePage extends BasePage {

    public ODS_Header header;
    public ODS_Footer footer;

    //region    Top banner
    @FindBy(css = ".item.active .img-responsive") private WebElement imgActiveBanner;
    @FindBy(css = ".glyphicon-chevron-right") private WebElement btnMainBannerRight;
    @FindBy(css = ".glyphicon-chevron-left") private WebElement btnMainBannerLeft;
    //endregion

    //region    Shop by category
    @FindBy(css = ".categories a") private List<WebElement> lstCategories;
    @FindBy(css = ".categories [href='/store/apparel/']") private WebElement lnkApparel;
    @FindBy(css = ".categories [href='/store/bags/']") private WebElement lnkBags;
    @FindBy(css = ".categories [href='/store/drinkware/']") private WebElement lnkDrinkware;
    @FindBy(css = ".categories [href='/store/electronics-computers/']") private WebElement lnkElectronics;
    @FindBy(css = ".categories [href='/store/corporate-gifts/']") private WebElement lnkCorporateGifts;
    @FindBy(css = ".categories [href='/store/writing-instruments/']") private WebElement lnkWriting;
    @FindBy(css = ".categories [href='/store/automotive-items/']") private WebElement lnkAutomotive;
    @FindBy(css = ".categories [href='/store/banners-signs/']") private WebElement lnkBannerMatSign;
    @FindBy(css = ".categories [href='/store/buttons-stickers/']") private WebElement lnkButtonStickerPatch;
    @FindBy(css = ".categories [href='/store/clocks-watches/']") private WebElement lnkClockWatch;
    @FindBy(css = ".categories [href='/store/fans/']") private WebElement lnkFans;
    @FindBy(css = ".categories [href='/store/flashlights-knives-tools/']") private WebElement lnkFlashlightTool;
    @FindBy(css = ".categories [href='/store/food-drink/']") private WebElement lnkFoodBeverage;
    @FindBy(css = ".categories [href='/store/golf-products/']") private WebElement lnkGolfProducts;
    @FindBy(css = ".categories [href='/store/household/']") private WebElement lnkHouseholdItems;
    @FindBy(css = ".categories [href='/store/journals-organizers-portfolios/']") private WebElement lnkJournals;
    @FindBy(css = ".categories [href='/store/keychains/']") private WebElement lnkKeychains;
    @FindBy(css = ".categories [href='/store/lanyards-badges-bracelets/']") private WebElement lnkLanyardBadge;
    @FindBy(css = ".categories [href='/store/magnets/']") private WebElement lnkMagnets;
    @FindBy(css = ".categories [href='/store/notepads/']") private WebElement lnkNotepads;
    @FindBy(css = ".categories [href='/store/office/']") private WebElement lnkOffice;
    @FindBy(css = ".categories [href='/store/outdoor-sporting-goods/']") private WebElement lnkOutdoor;
    @FindBy(css = ".categories [href='/store/party-supplies-toys-games/']") private WebElement lnkPartyGames;
    //endregion

    //region    Favorites
    @FindBy(css = ".quarters .text_container") private List<WebElement> lstFavoritesCategories;
    //endregion

    public ODS_HomePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        header = new ODS_Header(driver, wait);
        footer = new ODS_Footer(driver, wait);
    }

    public ODS_ProductsListingPage clickActiveBanner() {
        clickWhenReady(imgActiveBanner);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_HomePage clickBannerRight() {
        clickWhenReady(btnMainBannerRight);
        return new ODS_HomePage(driver, wait);
    }

    public ODS_HomePage clickBannerLeft() {
        clickWhenReady(btnMainBannerLeft);
        return new ODS_HomePage(driver, wait);
    }

    public ODS_ProductsListingPage openApparel() {
        clickWhenReady(lnkApparel);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openBags() {
        clickWhenReady(lnkBags);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openDrinkware() {
        clickWhenReady(lnkDrinkware);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openElectronics() {
        clickWhenReady(lnkElectronics);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openCorporateGifts() {
        clickWhenReady(lnkCorporateGifts);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openWritingInstruments() {
        clickWhenReady(lnkWriting);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openAutomotiveItems() {
        clickWhenReady(lnkAutomotive);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openBannerMatSign() {
        clickWhenReady(lnkBannerMatSign);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openButtonStickerPatch() {
        clickWhenReady(lnkButtonStickerPatch);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openClockWatch() {
        clickWhenReady(lnkClockWatch);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openFans() {
        clickWhenReady(lnkFans);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openFlashlightTool() {
        clickWhenReady(lnkFlashlightTool);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openFoodBeverage() {
        clickWhenReady(lnkFoodBeverage);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openGolfProducts() {
        clickWhenReady(lnkGolfProducts);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openHouseholdItems() {
        clickWhenReady(lnkHouseholdItems);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openJournals() {
        clickWhenReady(lnkJournals);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openKeychains() {
        clickWhenReady(lnkKeychains);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openLanyardBadge() {
        clickWhenReady(lnkLanyardBadge);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openMagnets() {
        clickWhenReady(lnkMagnets);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openNotepads() {
        clickWhenReady(lnkNotepads);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openOffice() {
        clickWhenReady(lnkOffice);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openOutdoor() {
        clickWhenReady(lnkOutdoor);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openPartyGames() {
        clickWhenReady(lnkPartyGames);
        return new ODS_ProductsListingPage(driver, wait);
    }

}
