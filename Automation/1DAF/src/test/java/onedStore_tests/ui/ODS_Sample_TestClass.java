package onedStore_tests.ui;

import onedStore.ui.pages.ODS_CartInterstitialPage;
import onedStore.ui.pages.ODS_CheckoutPage;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import logic.Browser;
import logic.Environments;
import onedStore.ui.pages.ODS_MyAccountPage;
import onedStore.ui.pages.ODS_SignOutPage;
import utils.Utils;
import logic.BaseTest;

import java.util.ArrayList;

public class ODS_Sample_TestClass extends BaseTest{

	WebDriver driver;
	WebDriverWait wait;
	ODS_CheckoutPage checkoutPage;
	ODS_CartInterstitialPage cartInterstitialPage;
	
	@BeforeClass(alwaysRun = true)
	private void beforeClass() {
		driver = Browser.PROPERTY.getInstance();
		wait = Utils.initWaits(driver, 5, 10, 1);
		driver.get(Environments.ODS_UI_URL);
		logger.info(Environments.ODS_UI_URL + " URL is opened.");
		checkoutPage = new ODS_CheckoutPage(driver, wait);
		cartInterstitialPage = new ODS_CartInterstitialPage(driver, wait);
	}
	
	
	@Test(groups = {"smoke"})
	public void test01() throws Exception {

		//	Sing in flyout
//		WebElement signInFlyOut = driver.findElement(By.xpath("//ul[@class='nav navbar-nav navbar-right']/li[4]/a"));
//		signInFlyOut.click();
//		Thread.sleep(500);
		//	email input field
//		WebElement emailInputField = driver.findElement(By.xpath("//input[@id='email']"));
//		emailInputField.clear();
//		emailInputField.sendKeys("stefan-180918@srb.hostopia.com");
//		//	password input field
//		WebElement passwordInputFiled = driver.findElement(By.xpath("//input[@id='password']"));
//		passwordInputFiled.clear();
//		passwordInputFiled.sendKeys("Deluxe@123");
//		//	sing in button
//		WebElement btnSignIn = driver.findElement(By.xpath("//button[@type='submit']"));
//		btnSignIn.click();
//		Thread.sleep(1000);
		//	search input field
		WebElement txtSearchInputFiled = driver.findElement(By.xpath("//input[@id='ihsinput']"));
		txtSearchInputFiled.clear();
		txtSearchInputFiled.sendKeys("shirts");
		txtSearchInputFiled.sendKeys(Keys.RETURN);
		Thread.sleep(1000);
		//	click on search result
		ArrayList<WebElement> searchResults = (ArrayList<WebElement>) driver.findElements(By.xpath("//div[@class='resultsGrid']"));
		searchResults.stream()
				.filter(s -> s.getText().toLowerCase().contains("22256".toLowerCase()))
				.findFirst()
				.ifPresent(WebElement::click);
		//searchResults.get(1).click();
//		Thread.sleep(1000);
//		//	click add to cart
//		WebElement btnAddToCart = driver.findElement(By.xpath("//a[@id='button-customize' and contains(.,'Add To Cart')]"));
//		btnAddToCart.click();
//		Thread.sleep(1000);
//		//	insert size quantity
//		WebElement txtSmall = driver.findElement(By.xpath("//input[@name='comments_129_5125']"));
//		txtSmall.clear();
//		txtSmall.sendKeys("12");
//		//	choose item color
//		ArrayList<WebElement> imgColorOptions = (ArrayList<WebElement>) driver.findElements(By.xpath("//div[@class='row group_78']//div[@class='rectangle-box']"));
//		imgColorOptions.get(0).click();
//		//	choose imprint location
//		ArrayList<WebElement> imgImptintLocation = (ArrayList<WebElement>) driver.findElements(By.xpath("//div[@class='row group_114']//div[@class='square-content']"));
//		imgImptintLocation.get(0).click();
//		//	choose imprint color
//		ArrayList<WebElement> imgImprintColor = (ArrayList<WebElement>) driver.findElements(By.xpath("//div[@class='row group_13']//div[@class='rectangle-content']"));
//		imgImprintColor.get(0).click();
//		//	click add to cart
//		WebElement btnAddToCart_Customize = driver.findElement(By.xpath("//button[contains(@onclick,'CartAdd')]"));
//		btnAddToCart_Customize.click();
//		Thread.sleep(1000);
//		//	click buy blank items
//		WebElement btnBuyBlank = driver.findElement(By.xpath("//button[@data-bb-handler='Buy Blank']"));
//		btnBuyBlank.click();
////		cartInterstitialPage.clickCheckout()
////				.clickAddANewShippingAddress()
////				.selectShippingCountry("1")
////				.enterFirstName("Stefan")
////				.enterLastName("Aleksic")
////				.enterCompany("Deluxe")
////				.enterAddress1("3660 VICTORIA ST N")
////				.enterCity("SHOREVIEW")
////				.selectState("1")
////				.enterZipCode("55126")
////				.enterDiscountCode("TESTORDER2017")
////				.clickApplyDiscount()
////				.clickAddCreditCart()
////				.enterCardholderName("American Express");

	}
	
	@AfterClass(alwaysRun = true)
	private void tearDown() {
		driver.quit();
	}
}
