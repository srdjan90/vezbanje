package onedStore.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import logic.BasePage;
import onedStore.ui.widgets.ODS_Header;

public class ODS_MyAccountPage extends BasePage{
	
	public ODS_Header header;

	public ODS_MyAccountPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
		header = new ODS_Header(driver, wait);
	}
	
	/**
	 * Elements
	 */
	
	@FindBy(xpath = "//li/a[contains(@href,'profileemail')]")
	private WebElement btnChangeYourEmailAddress;
	
	@FindBy(xpath = "//li/a[contains(@href,'profilepassword')]")
	private WebElement btnChangeYourPassword;
	
	@FindBy(xpath = "//li/a[contains(@href,'addresses')]")
	private WebElement btnManageShippingAddress;
	
	@FindBy(xpath = "//li/a[contains(@href,'profileinfo')]")
	private WebElement btnManageBillingInformation;
	
	@FindBy(xpath = "//a[contains(.,'Open')]")
	private WebElement btnOpen;
	
	@FindBy(xpath = "//a[contains(.,'Completed')]")
	private WebElement btnCompleted;
	
	@FindBy(xpath = "//a[contains(.,'Samples')]")
	private WebElement btnSamples;
	
	@FindBy(xpath = "//input[@name='q']")
	private WebElement txtProductName;
	
	@FindBy(xpath = "//input[@name='order_id']")
	private WebElement txtOrderID;
	
	@FindBy(xpath = "//input[@name='begin_date']")
	private WebElement txtBeginDate;
	
	@FindBy(xpath = "//input[@name='end_date']")
	private WebElement txtEndDate;
	
	@FindBy(xpath = "//input[@value='Search']")
	private WebElement btnSearch;
	

	/**
	 * Methods
	 */
	
	public ODS_ChangeEmailAddressPage clickChangeYourEmailAddress() {
		clickWhenReady(btnChangeYourEmailAddress);
		return new ODS_ChangeEmailAddressPage(driver, wait);
	}
	
	public ODS_ChangePasswordPage clickChangeYourPassword() {
		clickWhenReady(btnChangeYourPassword);
		return new ODS_ChangePasswordPage(driver, wait);
	}
	
	public ODS_ManageShippingAddressesPage clickManageShippingAddresses() {
		clickWhenReady(btnManageShippingAddress);
		return new ODS_ManageShippingAddressesPage(driver, wait);
	}
	
	public ODS_ManageBillingInformationPage clickManageBillingInformation() {
		clickWhenReady(btnManageBillingInformation);
		return new ODS_ManageBillingInformationPage(driver, wait);
	}
	
	public ODS_MyAccountPage clickOpen() {
		clickWhenReady(btnOpen);
		return this;
	}
	
	public ODS_MyAccountPage clickCompleted() {
		clickWhenReady(btnCompleted);
		return this;
	}
	
	public ODS_MyAccountPage clickSamples() {
		clickWhenReady(btnSamples);
		return this;
	}
	
	public ODS_MyAccountPage clickSearch() {
		clickWhenReady(btnSearch);
		return this;
	}
	
	public ODS_MyAccountPage enterProductName(String productName) {
		typeIn(txtProductName, productName);
		return this;
	}
	
	public ODS_MyAccountPage enterOrderID(String orderID) {
		typeIn(txtOrderID, orderID);
		return this;
	}
	
	public ODS_MyAccountPage enterBeginDate(String beginDate) {
		typeIn(txtBeginDate, beginDate);
		return this;
	}
	
	public ODS_MyAccountPage enterEndDate(String endDate) {
		typeIn(txtEndDate, endDate);
		return this;
	}
	
	

}
