package onedStore.ui.ProductDataTesting_classes;

import java.util.ArrayList;

public class ComplexProduct {

    private Products product;
    private ArrayList<ProductOptions> productOptions;
    private ArrayList<Prices> prices;

    public ComplexProduct(Products product, ArrayList<ProductOptions> productOptions, ArrayList<Prices> prices) {
        this.product = product;
        this.productOptions = productOptions;
        this.prices = prices;
    }

    public Products getProduct() {
        return product;
    }

    public void setProduct(Products product) {
        this.product = product;
    }

    public ArrayList<ProductOptions> getProductOptions() {
        return productOptions;
    }

    public void setProductOptions(ArrayList<ProductOptions> productOptions) {
        this.productOptions = productOptions;
    }

    public ArrayList<Prices> getPrices() {
        return prices;
    }

    public void setPrices(ArrayList<Prices> prices) {
        this.prices = prices;
    }
}
