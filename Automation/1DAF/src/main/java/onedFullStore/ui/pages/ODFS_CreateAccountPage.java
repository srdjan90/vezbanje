/*
 * Created by ekarliuchenko on 10/16/18 12:17 PM
 */

package onedFullStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ODFS_CreateAccountPage extends BasePage {

    @FindBy(css = "#hdn_email") private WebElement txtEmail;
    @FindBy(css = "#next-step") private WebElement btnNext;
    @FindBy(css = "#txt_newpassword") private WebElement txtPassword;
    @FindBy(css = "#txt_confirmpassword") private WebElement txtConfirmPassword;
    @FindBy(css = "#editEmail") private WebElement btnChangeEmail;
    @FindBy(css = "#btn_continue") private WebElement btnCreateAccount;
    @FindBy(css = ".alert-info a[href='/products/custom/kb/privacy-policy/']") private WebElement linkPrivacyPolicy;
    @FindBy(css = "button[onclick='updateShippingAddressBilling()']") private WebElement btnSameAsBilling;

    //region AccountData
    @FindBy(css = "#txt_billing_firstname") private WebElement txtBillingFirstName;
    @FindBy(css = "#txt_billing_lastname") private WebElement txtBillingLastName;
    @FindBy(css = "#txt_billing_phone") private WebElement txtBillingPhone;
    @FindBy(css = "#sel_billing_country") private WebElement ddBillingCountry;
    @FindBy(css = "#sel_industry") private WebElement ddIndustry;
    @FindBy(css = "#txt_billing_company") private WebElement txtBillingCompany;
    @FindBy(css = "#txt_billing_address1") private WebElement txtBillingAddress1;
    @FindBy(css = "#txt_billing_address2") private WebElement txtBillingAddress2;
    @FindBy(css = "#txt_billing_address3") private WebElement txtBillingAddress3;
    @FindBy(css = "#txt_billing_city") private WebElement txtBillingCity;
    @FindBy(css = "#sel_billing_state") private WebElement ddBillingState;
    @FindBy(css = "#txt_billing_zip") private WebElement txtBillingZip;
    //endregion
    //region ShippingInfo
    @FindBy(css = "#txt_shipping_label") private WebElement txtAddressLabel;
    @FindBy(css = "#sel_shipping_country") private WebElement ddShippingCountry;
    @FindBy(css = "#txt_shipping_company") private WebElement txtShippingCompany;
    @FindBy(css = "#txt_shipping_address1") private WebElement txtShippingAddress1;
    @FindBy(css = "#txt_shipping_address2") private WebElement txtShippingAddress2;
    @FindBy(css = "#txt_shipping_address3") private WebElement txtShippingAddress3;
    @FindBy(css = "#txt_shipping_city") private WebElement txtShippingCity;
    @FindBy(css = "#sel_shipping_state") private WebElement ddShippingState;
    @FindBy(css = "#txt_shipping_zip") private WebElement txtShippingZip;
    //endregion

    public ODFS_CreateAccountPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public ODFS_CreateAccountPage enterEmail(String email) {
        typeIn(txtEmail, email);
        return this;
    }

    public ODFS_CreateAccountPage confirmEmail(){
        clickWhenReady(btnNext);
        return this;
    }

    public ODFS_CreateAccountPage typeInPassword(String password) {
        typeIn(txtPassword, password);
        return this;
    }

    public ODFS_CreateAccountPage typeInConfirmPassword(String confirmPassword) {
        typeIn(txtConfirmPassword, confirmPassword);
        return this;
    }

    public ODFS_CreateAccountPage enterPasswords(String password) {
        return typeInPassword(password).typeInConfirmPassword(password);
    }

    public ODFS_CreateAccountPage enterBillingFirstName(String firstName) {
        typeIn(txtBillingFirstName, firstName);
        return this;
    }

    public ODFS_CreateAccountPage enterBillingLastName(String lastName) {
        typeIn(txtBillingLastName, lastName);
        return this;
    }

    public ODFS_CreateAccountPage enterBillingPhone(String phoneNumber) {
        typeIn(txtBillingPhone, phoneNumber);
        return this;
    }

    public ODFS_CreateAccountPage selectBillingCountry(String countryOption) {
        selectByText(ddBillingCountry, countryOption);
        return this;
    }

    public ODFS_CreateAccountPage selectIndustry(String industryOption) {
        selectByText(ddIndustry, industryOption);
        return this;
    }

    public ODFS_CreateAccountPage enterBillingCompany(String companyName) {
        typeIn(txtBillingCompany, companyName);
        return this;
    }

    public ODFS_CreateAccountPage enterBillingAddress1(String address1) {
        typeIn(txtBillingAddress1, address1);
        return this;
    }

    public ODFS_CreateAccountPage enterBillingAddress2(String address2) {
        typeIn(txtBillingAddress2, address2);
        return this;
    }

    public ODFS_CreateAccountPage enterBillingAddress3(String address3) {
        typeIn(txtBillingAddress3, address3);
        return this;
    }

    public ODFS_CreateAccountPage enterBillingCity(String city) {
        typeIn(txtBillingCity, city);
        return this;
    }

    public ODFS_CreateAccountPage selectBillingState (String state) {
        selectByText(ddBillingState, state);
        return this;
    }

    public ODFS_CreateAccountPage enterBillingZip (String zip) {
        typeIn(txtBillingZip, zip);
        return this;
    }

    public ODFS_CreateAccountPage makeShippingSameAsBilling(){
        clickWhenReady(btnSameAsBilling);
        return this;
    }

    public ODFS_HomePage clickCreateAccount(){
        clickWhenReady(btnCreateAccount);
        return new ODFS_HomePage(driver, wait);
    }

    public ODFS_HomePage createAccount(String email, String password, String firstName, String lastName,
                                       String phone, String country, String industry, String company,
                                       String address1, String city, String state, String zip){
        return
        enterEmail(email).
        confirmEmail().
        enterPasswords(password).
        enterBillingFirstName(firstName).
        enterBillingLastName(lastName).
        enterBillingPhone(phone).
        selectBillingCountry(country).
        selectIndustry(industry).
        enterBillingCompany(company).
        enterBillingAddress1(address1).
        enterBillingCity(city).
        selectBillingState(state).
        enterBillingZip(zip).
        clickCreateAccount();
    }

    public ODFS_CreateAccountPage enterAddressLabel(String addressLabel){
        typeIn(txtAddressLabel, addressLabel);
        return this;
    }

    public ODFS_CreateAccountPage selectShippingCountry(String country){
        selectByText(ddShippingCountry, country);
        return this;
    }

    public ODFS_CreateAccountPage enterShippingCompany(String company){
        typeIn(txtShippingCompany, company);
        return this;
    }

    public ODFS_CreateAccountPage enterShippingAddress1(String address1){
        typeIn(txtShippingAddress1, address1);
        return this;
    }

    public ODFS_CreateAccountPage enterShippingAddress2(String address2){
        typeIn(txtShippingAddress2, address2);
        return this;
    }

    public ODFS_CreateAccountPage enterShippingAddress3(String address3){
        typeIn(txtShippingAddress3, address3);
        return this;
    }

    public ODFS_CreateAccountPage enterShippingCity(String city){
        typeIn(txtShippingCity, city);
        return this;
    }

    public ODFS_CreateAccountPage selectState(String state){
        selectByText(ddShippingState, state);
        return this;
    }

    public ODFS_CreateAccountPage enterShippingZip(String zip){
        typeIn(txtShippingZip, zip);
        return this;
    }



}
