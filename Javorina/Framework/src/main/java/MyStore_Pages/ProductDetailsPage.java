package MyStore_Pages;

import java.io.IOException;

import Base.UI;
import MyStore_Repository.Repository;

public class ProductDetailsPage {
	
	public static void verifyProductImageIsPresent(String productName) throws IOException {
		UI.verifyElementIsPresent("Product Image", "//img[@id='bigpic' and @title='"+productName+"']");
	}
	
	public static void clickProductImage(String productName) throws IOException {
		UI.clickElement("Product Image", "//img[@id='bigpic' and @title='"+productName+"']");
	}
	
	public static void verifyLargeProductImageIsPresent() throws IOException {
		UI.verifyElementIsPresent("Image Preview", Repository.productImageLarge);
	}

}
