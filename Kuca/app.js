function sabiranje() {
    let a = parseInt(document.formaSabiranja.x.value);
    let b = parseInt(document.formaSabiranja.y.value);
    let ukupno = 0;
    errorMessageCheck = false;

// ####################################################################
    // Ispitivanje za prvu vrednost:
    if (isNaN(a) == true) {
        errorMessageCheck = true;
        m = 'X ili Y nije broj!';
        document.getElementById('p1').innerHTML = m;
    } else if (a == '') {
        errorMessageCheck = true;
        m = 'Popuniti sva polja!';
        document.getElementById('p1').innerHTML = m;
    }

    ukupno = a + b;
    document.formaSabiranja.rezultat.value = ukupno;
}



