package onedStore.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import logic.BasePage;

public class ODS_OrderConfirmationPage extends BasePage{

	public ODS_OrderConfirmationPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
	}
	 
	@FindBy(xpath="//a[contains(., 'Continue Shopping')]")
	private WebElement linkContinueShopping;
	
	@FindBy(xpath="//a[contains(., 'View Order')]")
	private WebElement linkViewOrder;
	
	@FindBy(xpath="//a[contains(., 'Go to My Account')]")
	private WebElement linkGoToMyAccount;
	
	public ODS_HomePage clickContinueShopping() {
		clickWhenReady(linkContinueShopping);
		return new ODS_HomePage(driver, wait);
	}
	
	public ODS_ViewOrderPage clickViewOrder() {
		clickWhenReady(linkViewOrder);
		return new ODS_ViewOrderPage(driver, wait);
	}
	
	public ODS_MyAccountPage clickGoToMyAccount() {
		clickWhenReady(linkGoToMyAccount);
		return new ODS_MyAccountPage(driver, wait);
	}
	
}
