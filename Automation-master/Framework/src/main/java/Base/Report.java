package Base;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import TheNinjaStore_Pages.NinjaStore_GlobalVariables;

public class Report {
	
	static ExtentHtmlReporter reporter = new ExtentHtmlReporter("./Reports/AutomationReport_"+GlobalVariables.time+".html");
	static ExtentReports extent = new ExtentReports();
	static ExtentTest logger;
	public static boolean passed = true;
	public static String exp;
	
	public static void failed(String explanation)  {
		Report.passed = false;
		Report.exp = explanation;
		//logger.log(Status.FAIL, explanation);
		try {
			UI.createScreenshot(GlobalVariables.testName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//logger.fail(explanation, MediaEntityBuilder.createScreenCaptureFromPath("C:\\Automation_Screenshots\\"+GlobalVariables.testName+GlobalVariables.time+".png").build());
		try {
			logger.fail(explanation).addScreenCaptureFromPath("C:\\Automation_Screenshots\\"+GlobalVariables.testName+GlobalVariables.time+".png");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void passed(String testName) {
		//logger.log(Status.PASS, testName+" - Passed");
		logger.pass(testName+" - Passed");
	}
	
	public static void writeReport(String testName) throws IOException {	
		extent.attachReporter(reporter);
		logger =  extent.createTest(testName+"_Test");
		reporter.setAppendExisting(true);
		
		if(passed == true) {
			Report.passed(testName);
		} else {
			Report.failed(exp);
		}
		
		extent.flush();
	}

}
