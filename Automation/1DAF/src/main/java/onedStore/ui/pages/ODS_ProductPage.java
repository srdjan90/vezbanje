package onedStore.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;

import logic.BasePage;
import org.testng.Assert;

import java.util.List;

public class ODS_ProductPage extends BasePage{

	public ODS_ProductPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
		
	}
	
	/**
	 * Elements
	 */
	
	@FindBy(xpath = "//a[@id='button-customize' and contains(.,'Design It!')]")
	private WebElement btnDesignIt;
	
	@FindBy(xpath = "//a[@id='button-customize' and contains(.,'Add To Cart')]")
	private WebElement btnAddToCart;
	
	@FindBy(xpath = "//input[@name='quantity']")
	private WebElement txtQuantity;
	
	@FindBy(xpath = "//button[@class='btn btn-default' and not(@href)]")
	private WebElement btnUpdate;
	
	@FindBy(xpath = "//span[@class='glyphicon glyphicon-chevron-up']")
	private WebElement btnArrowUp;
	
	@FindBy(xpath = "//span[@class='glyphicon glyphicon-chevron-down']")
	private WebElement btnArrowDown;
	
	@FindBy(xpath = "//button[@id='zoom_btn']")
	private WebElement linkClickImageToZoom;
	
	@FindBy(xpath = "//span[@class='glyphicon glyphicon-remove']")
	private WebElement btnCloseZoomPrevew;
	
	@FindBy(xpath = "//span[@id='productSubTotal']")
	private WebElement lblProductSubtotal;
	
	@FindBy(xpath = "//span[@id='productTotal']")
	private WebElement lblProductTotal;
	
	@FindBy(xpath = "//div[@class='container-fluid']/strong")
	private WebElement lblItemNumber;
	
	@FindBy(xpath = "//span[@class='productNumber' and @style]/span[@itemprop='productID']")
	private WebElement lblSkuNumber;

	@FindBy(how = How.XPATH, using = "//div[@class='hidden-xs hidden-sm']/h1")
	private WebElement lblProductName;

	@FindBy(how = How.XPATH, using = "//ol[@class='breadcrumb']")
	private WebElement linkBreadcrumb;

	@FindAll(@FindBy(how = How.XPATH, using = "//div[@class='col-md-1 hidden-xs hidden-sm']//li/a/img"))
	private List<WebElement> imgProductColorThumbnails;

	@FindBy(how = How.XPATH, using = "//div[@class='col-md-6 text-center']/a/img")
	private WebElement imgCentralProductImage;

	@FindBy(how = How.XPATH, using = "//span[@itemprop='description']")
	private WebElement lblProductDescription;

	@FindAll(@FindBy(how = How.XPATH, using = "//table[@class='table']/thead/tr/th/a"))
    private List<WebElement> linksProductQuantites;
	
	/**
	 * Methods
	 */
	
	public ODS_DesignerPage clickDesignIt() {
		clickWhenReady(btnDesignIt);
		return new ODS_DesignerPage(driver, wait);
	}
	
	public ODS_CustomizePage clickAddToCart() {
		clickWhenReady(btnAddToCart);
		return new ODS_CustomizePage(driver, wait);
	}
	
	public ODS_ProductPage enterQuantity(String quantity) {
		typeIn(txtQuantity, quantity);
		return this;
	}
	
	public ODS_ProductPage clickUpdate() {
		clickWhenReady(btnUpdate);
		return this;
	}
	
	public ODS_ProductPage clickImgThumbnailsUpArrow() {
		clickWhenReady(btnArrowUp);
		return this;
	}
	
	public ODS_ProductPage clickImgThumbnailsDownArrow() {
		clickWhenReady(btnArrowDown);
		return this;
	}
	
	public ODS_ProductPage clickImageToZoom() {
		clickWhenReady(linkClickImageToZoom);
		return this;
	}
	
	public ODS_ProductPage closeImageZoomPreview() {
		clickWhenReady(btnCloseZoomPrevew);
		return this;
	}

	public ODS_ProductPage verifyProductName(String productName){
		Assert.assertEquals(getTextFromElement(waitForVisible(lblProductName)), productName, "Product name is not correct. Test Failed!");
		return this;
	}

	public ODS_ProductPage verifyProductBreadcrumb(String productBreadcrumb){
		Assert.assertEquals(getTextFromElement(waitForVisible(linkBreadcrumb)), productBreadcrumb, "Product breadcrumb is not correct. Test Failed!");
		return this;
	}

	public ODS_ProductPage chooseProductColor(String colorName) throws InterruptedException {

		String imgThumbnailLinkFull = "";
		String link = "";

		while(!imgProductColorThumbnails.get(0).isDisplayed()){
			clickWhenReady(btnArrowUp);
		}

		for(int i = 0; i < imgProductColorThumbnails.size(); i++){
			if(imgProductColorThumbnails.get(i).getAttribute("alt").toLowerCase().contains(colorName.toLowerCase())) {
				while(!imgProductColorThumbnails.get(i).isDisplayed()){
					clickWhenReady(btnArrowDown);
				}
				clickWhenReady(imgProductColorThumbnails.get(i));
				imgThumbnailLinkFull = imgProductColorThumbnails.get(i).findElement(By.xpath("..")).getAttribute("onclick");
				String[] split = imgThumbnailLinkFull.split(",");
				for (String s : split) {
					if (s.contains("ihfiles")) {
						link = s;
					}
				}
				link = link.substring(1, link.length() - 1);
				Assert.assertEquals(imgCentralProductImage.getAttribute("src"), link, "Central image URL is not correct. Test Failed!");
			}
		}
		return this;
	}

	public ODS_ProductPage verifyProductDescription(String productDescription){
		Assert.assertEquals(getTextFromElement(lblProductDescription), productDescription, "Product description is not correct. Test Failed!");
		return this;
	}

	public ODS_ProductPage chooseProductQuantity(String qty){
	    for(WebElement w : linksProductQuantites){
	        if(getTextFromElement(w).contains(qty)){
	            clickWhenReady(w);
	            break;
            }
        }
	    return this;
    }




}
