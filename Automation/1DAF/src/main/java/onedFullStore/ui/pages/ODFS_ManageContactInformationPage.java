package onedFullStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ODFS_ManageContactInformationPage extends BasePage {

    @FindBy(css = "#sel_billing_country") private WebElement ddCountry;
    @FindBy(css = "#txt_billing_firstname") private WebElement txtFirstName;
    @FindBy(css = "#txt_billing_lastname") private WebElement txtLastName;
    @FindBy(css = "#txt_billing_company") private WebElement txtCompany;
    @FindBy(css = "#txt_billing_address1") private WebElement txtAddress1;
    @FindBy(css = "#txt_billing_address2") private WebElement txtAddress2;
    @FindBy(css = "#txt_billing_address3") private WebElement txtAddress3;
    @FindBy(css = "#txt_billing_city") private WebElement txtCity;
    @FindBy(css = "#txt_billing_state_international") private WebElement txtState;
    @FindBy(css = "#sel_billing_state") private WebElement ddState;
    @FindBy(css = "#txt_billing_zip") private WebElement txtZip;
    @FindBy(css = "#txt_billing_phone") private WebElement txtPhone;
    @FindBy(css = "input[type='submit']") private WebElement btnSaveChanges;
    @FindBy(css = "input[type='submit'] ~ a") private WebElement btnCancel;
    @FindBy(css = ".modal-footer .btn-success") private WebElement btnBackToYourAccount;
    
    public ODFS_ManageContactInformationPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public ODFS_ManageContactInformationPage selectCountry(String country){
        selectByText(ddCountry, country);
        return this;
    }

    public ODFS_ManageContactInformationPage enterFirstName(String firstName){
        typeIn(txtFirstName, firstName);
        return this;
    }

    public ODFS_ManageContactInformationPage enterLastName(String lastName){
        typeIn(txtLastName, lastName);
        return this;
    }

    public ODFS_ManageContactInformationPage enterCompany(String company){
        typeIn(txtCompany, company);
        return this;
    }

    public ODFS_ManageContactInformationPage enterAddress1(String address1){
        typeIn(txtAddress1, address1);
        return this;
    }

    public ODFS_ManageContactInformationPage enterAddress2(String address2){
        typeIn(txtAddress2, address2);
        return this;
    }

    public ODFS_ManageContactInformationPage enterAddress3(String address3){
        typeIn(txtAddress3, address3);
        return this;
    }

    public ODFS_ManageContactInformationPage enterCity(String city){
        typeIn(txtCity, city);
        return this;
    }

    public ODFS_ManageContactInformationPage enterState(String state){
        typeIn(txtState, state);
        return this;
    }

    public ODFS_ManageContactInformationPage selectState(String state){
        selectByText(ddState, state);
        return this;
    }

    public ODFS_ManageContactInformationPage enterZip(String zip){
        typeIn(txtZip, zip);
        return this;
    }

    public ODFS_ManageContactInformationPage enterPhone(String phone){
        typeIn(txtPhone, phone);
        return this;
    }

    public ODFS_ManageContactInformationPage saveChanges(){
        clickWhenReady(btnSaveChanges);
        return this;
    }

    public ODFS_ManageContactInformationPage cancel(){
        clickWhenReady(btnCancel);
        return this;
    }

    public ODFS_MyAccountPage backToMyAccount(){
        clickWhenReady(btnBackToYourAccount);
        return new ODFS_MyAccountPage(driver, wait);
    }
}
