/***************************************************************************************************
 Created by vskyba on 10/12/18 10:59 PM
 **************************************************************************************************/

package onedFullStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class ODFS_CheckoutPage extends BasePage {
    
    @FindBy(css = ".loadmask-msg") private WebElement body;
    @FindBy(css = "#txt_couponcode") private WebElement txtPromoCode;
    @FindBy(css = "#btn_coupon") private WebElement btnApplyDiscount;
    @FindBy(css = "[data-type_pay='creditcard']") private WebElement btnPayByCreditCard;
    @FindBy(how = How.XPATH, using = "//div[@id='coupon_testorder']") private WebElement lblTestOrderCouponApplied;
    @FindBy(how = How.XPATH, using = "//div[@id='couponcodetxt']") private WebElement lblCouponApplied;
    @FindBy(how = How.XPATH, using = "//button[@data-bb-handler='ok']") private WebElement btnOk;


    public ODFS_CheckoutPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

//    Module methods

    private ODFS_CheckoutPage typePromoCode(String inputText){
        typeIn(txtPromoCode, inputText);
        return this;
    }

    private ODFS_CheckoutPage clickApplyDiscount() {
        clickWhenReady(btnApplyDiscount);
        return new ODFS_CheckoutPage(driver, wait);
    }

    private ODFS_CheckoutPage verifyAppliedTestOrderCoupon(){
        if(!getTextFromElement(lblTestOrderCouponApplied).toLowerCase().contains("".toLowerCase())){
            Assert.fail("TEST ORDER coupon has not been applied. Test Failed!");
        }
        return this;
    }

    private ODFS_CheckoutPage verifyAppliedCoupon(){
        waitForSpinners();
        if(!getTextFromElement(lblCouponApplied).toLowerCase().contains("Your discount has been applied.".toLowerCase())){
            Assert.fail("Coupon has not been applied. Test Failed!");
        }
        return this;
    }

    private ODFS_CheckoutPage clickOk(){
        clickWhenReady(btnOk);
        return this;
    }

    private ODFS_CheckoutPage verifyInvalidDiscount(){
        waitForModals();
        clickOk();
        return this;
    }



//    Main Methods

    public ODFS_CheckoutPage applyDiscount (String promoСode){
//        waitForVisible(body);
//        waitForInvisible(body);
        waitForSpinners();
        typePromoCode(promoСode).clickApplyDiscount()
                .verifyAppliedCoupon();
        return this;
    }

    public ODFS_CheckoutPage applyTESTORDERCoupon(){
        //waitForSpinners();
        typePromoCode("TESTORDER2017")
                .clickApplyDiscount()
                .verifyAppliedTestOrderCoupon();
        return this;
    }

    public ODFS_CheckoutPage applyInvalidDiscount(String invalidDiscount){
        typePromoCode(invalidDiscount)
                .clickApplyDiscount()
                .verifyInvalidDiscount();
        return this;
    }


}
