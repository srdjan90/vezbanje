package logic;


import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.testng.annotations.BeforeSuite;
import org.testng.internal.Utils;




public abstract class BaseTest {
	
	/**
	 * Setting log4j file name property
	 */
	
	static {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH.mm");
        System.setProperty("current.date.time", dateFormat.format(new Date()));
        PropertyConfigurator.configure("./src/main/resources/log4j_ui.properties");
	}
	
	
	public static Logger logger;

	@BeforeSuite(alwaysRun = true)
	public void setUp() {
		logger = LogManager.getLogger(this.getClass());
	}
	
	
	
	
	
}
