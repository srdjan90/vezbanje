package MyStore_Pages;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import Base.Browser;
import Base.UI;
import MyStore_Repository.Repository;

public class SearchResultPage {
	
	public static void hoverSearchResult(String productName) throws IOException {
		UI.hoverElement(productName, "//div[@class='product-image-container']//img[@title='"+productName+"']");
	}
	
	public static void selectColor(String color, String productName) throws IOException {
		UI.clickElement("Color", "//div[@class='product-container' and contains(.,'"+productName+"')]//ul[@class='color_to_pick_list clearfix']/li/a[contains(@href,'"+color+"')]");
	}

}
