package TheNinjaStore_Pages;

import static org.testng.Assert.fail;

import java.io.IOException;

import org.openqa.selenium.WebElement;

import Base.UI;
import TheNinjaStore_Repository.Repository;

public class Navigation {

	public static void verifyLogoIsPresent() throws IOException {
		Base.UI.verifyElementIsPresent("Header Logo", Repository.navigation_HeaderLogo);
	}
	
	public static void clickLogoFromHeader() throws IOException {
		Base.UI.clickElement("Header Logo", Repository.navigation_HeaderLogo);
	}
	
	public static void enterTextInSearchBar(String valueToEnter) throws IOException {
		UI.enterTextIntoElement("Search Bar", Repository.navigation_SearchBar, valueToEnter);
	}
	
	public static void ClickSearchButton() throws IOException {
		UI.clickElement("Search Button", Repository.navigation_SearchButton);
	}
	
	public static void clickShoppingCartButton() throws IOException {
		UI.clickElement("Shopping Cart Button", Repository.navigation_ShoppingCartButton);
	}
	
	public static void verifyNumberOfItemsInShoppingCart(String numberOfItemsInCart) throws IOException {
		WebElement temp = UI.findElementByXpath("Shoping Cart number",Repository.navigation_ShoppingCartButton);
		if (temp.getText().contains(numberOfItemsInCart+" item(s)")) {
			System.out.println("Pass - cart contains " + numberOfItemsInCart + " number of items");
		} else {
			System.out.println("Fail - expected number of items was " + numberOfItemsInCart + " but was -- " + temp.getText() + " --");
			fail("Fail - expected number of items was " + numberOfItemsInCart + " but was -- " + temp.getText() + " --");
		}
	}
	
	public static void verifyProductTotalInShoppingCart(String productTotal) throws IOException {
		WebElement temp = UI.findElementByXpath("Product Total", Repository.navigation_ShoppingCartButton);
		if (temp.getText().contains(productTotal)) {
			System.out.println("Pass - product total is " + productTotal);
		} else {
			System.out.println("Fail - expected product total was " + productTotal + " but was -- " + temp.getText() + " --");
			fail("Fail - expected product total was " + productTotal + " but was -- " + temp.getText() + " --");
		}
	}
	
	public static void verifyProductImageInCart(String productName) throws IOException {
		UI.verifyElementIsPresent(productName+" image", "//ul[@class='dropdown-menu pull-right']//tr//img[@alt='"+productName+"']");
	}
	
	public static void clickProductImageIncart(String productName) throws IOException {
		UI.clickElement(productName+" image", "//ul[@class='dropdown-menu pull-right']//tr//img[@alt='"+productName+"']");
	}
	
	public static void verifyProductNameInCart(String productName) throws IOException {
		UI.verifyElementIsPresent(productName+" name", "//ul[@class='dropdown-menu pull-right']//td[@class='text-left']/a[text()='"+productName+"']");
	}
	
	public static void clickProductNameIncart(String productName) throws IOException {
		UI.clickElement(productName+" name", "//ul[@class='dropdown-menu pull-right']//td[@class='text-left']/a[text()='"+productName+"']");
	}
	
	public static void verifyQuantityInCartDropdown(String productName, String expectedQiantity) throws Throwable {
		UI.verifyElementContainsPartialText(productName, "//ul[@class='dropdown-menu pull-right']//tr[contains(.,'"+productName+"')]/td[contains(.,'x')]", "x "+expectedQiantity);
	}
	
	public static void verifyProductTotalInCartDropdown(String productName, String expectedProductTotal) throws Throwable {
		UI.verifyElementContainsPartialText(productName, "//ul[@class='dropdown-menu pull-right']//tr[contains(.,'"+productName+"')]/td[contains(.,'$')]", expectedProductTotal);
	}
	
	public static void verifyMegaLinkIsPresent(String megaLinkName) throws IOException {
		UI.verifyElementIsPresent(megaLinkName, "//ul[@class='nav navbar-nav']/li/a[contains(normalize-space(),'"+megaLinkName+"')]");
	}
	
	public static void clickDeleteFromCartButton(String productName) throws IOException {
		UI.clickElement("Delete Button", "//ul[@class='dropdown-menu pull-right']//tr[contains(.,'"+productName+"')]/td/button");
	}
	
	public static void verifyCartIsEmpty() throws Throwable {
		UI.verifyElementContainsPartialText("Shopping Cart Button", "//ul[@class='dropdown-menu pull-right']", "Your shopping cart is empty!");
	}
	
	public static void verifyMyAccountLinkIsPresent() throws IOException {
		UI.verifyElementIsPresent("MyAccount Link", Repository.navigation_MyAccount);
	}
	
	public static void clickMyAccountLink() throws IOException {
		UI.clickElement("MyAccount Link", Repository.navigation_MyAccount);
	}
	
	public static void verifyRegisterLinkIsPresent() throws IOException {
		UI.verifyElementIsPresent("Register link", Repository.navigation_RegisterLink);
	}
	
	public static void clickRegisterLink() throws IOException {
		UI.clickElement("Register link", Repository.navigation_RegisterLink);
	}
	
	public static void verifyLoginLinkIsPresent() throws IOException {
		UI.verifyElementIsPresent("Login link", Repository.navigation_LoginLink);
	}
	
	public static void clickLoginLink() throws IOException {
		UI.clickElement("Login link", Repository.navigation_LoginLink);
	}
	
	public static void verifyWishListIsPresent() throws IOException {
		UI.verifyElementIsPresent("Header Wish List", Repository.navigaition_WishListHeader);
	}
	
	public static void verifyNumberOfItemsInWishLeast(String numberOfItemsInWishList) throws Throwable {
		UI.verifyElementContainsPartialText("Header Wish List", Repository.navigaition_WishListHeader, "Wish List ("+numberOfItemsInWishList+")");
	}
	
	public static void clickWishList() throws IOException {
		UI.clickElement("Header Wish List", Repository.navigaition_WishListHeader);
	}
	
	public static void verifyShoppingCartLinkIsPresent() throws IOException {
		UI.verifyElementIsPresent("Header Shopping Cart link", Repository.navigation_ShoppingCartHeader);
	}
	
	public static void clickShoppingCartLink() throws IOException {
		UI.clickElement("Header Shopping Cart link", Repository.navigation_ShoppingCartHeader);
	}
	
	public static void verifyCheckoutLinkIspResent() throws IOException {
		UI.verifyElementIsPresent("Header Checkout Link", Repository.navigation_CheckoutHeader);
	}
	
	public static void clickCheckoutLink() throws IOException {
		UI.clickElement("Header Checkout Link", Repository.navigation_CheckoutHeader);
	}
	
	
	public static void verifyAllMegaLinksArePresent() throws IOException {
		verifyMegaLinkIsPresent("Desktops");
		verifyMegaLinkIsPresent("Laptops & Notebooks");
		verifyMegaLinkIsPresent("Components");
		verifyMegaLinkIsPresent("Tablets");
		verifyMegaLinkIsPresent("Software");
		verifyMegaLinkIsPresent("Phones & PDAs");
		verifyMegaLinkIsPresent("Cameras");
		verifyMegaLinkIsPresent("MP3 Players");
	}
	
	public static void mouseHoverMegaLink(String megaLinkName) throws IOException {
		UI.hoverElement(megaLinkName, "//ul[@class='nav navbar-nav']/li/a[contains(normalize-space(),'"+megaLinkName+"')]");
	}
	
	public static void verifySublinkIsPresent(String megaLink, String subLink) throws IOException {
		mouseHoverMegaLink(megaLink);
		
		//UI.verifyElementIsPresent(subLink, "//div[@class='dropdown-inner']//a[contains(normalize-space(),'"+subLink+"')]");
		UI.verifyElementIsPresent(subLink, "//div[@class='dropdown-menu']//a[contains(normalize-space(),'"+subLink+"')]");
	}
	
	public static void clickSublink(String megaLink, String subLink) throws IOException {
		mouseHoverMegaLink(megaLink);
		//UI.clickElement(subLink, "//div[@class='dropdown-inner']//a[contains(normalize-space(),'\"+subLink+\"')]");
		UI.clickElement(subLink, "//div[@class='dropdown-menu']//a[contains(normalize-space(),'"+subLink+"')]");
	}
	
	public static void verifyDesktopsSubLinks() throws IOException {
		verifySublinkIsPresent("Desktops", "PC");
		verifySublinkIsPresent("Desktops", "Mac");
		verifySublinkIsPresent("Desktops", "Show All Desktops");
	}
	
	public static void verifyLaptopsAndNotebooksSubLinks() throws IOException {
		verifySublinkIsPresent("Laptops & Notebooks", "Macs");
		verifySublinkIsPresent("Laptops & Notebooks", "Windows");
		verifySublinkIsPresent("Laptops & Notebooks", "Show All Laptops & Notebooks");
	}
	
	public static void verifyComponentsSubLinks() throws IOException {
		verifySublinkIsPresent("Components", "Mice and Trackballs");
		verifySublinkIsPresent("Components", "Monitors");
		verifySublinkIsPresent("Components", "Printers");
		verifySublinkIsPresent("Components", "Scanners");
		verifySublinkIsPresent("Components", "Web Cameras");
		verifySublinkIsPresent("Components", "Show All Components");
	}
	
	public static void verifyMP3PlayersSubLinks() throws IOException {
		verifySublinkIsPresent("MP3 Players", "test 11");
		verifySublinkIsPresent("MP3 Players", "test 12");
		verifySublinkIsPresent("MP3 Players", "test 15");
		verifySublinkIsPresent("MP3 Players", "test 16");
		verifySublinkIsPresent("MP3 Players", "test 17");
		verifySublinkIsPresent("MP3 Players", "test 18");
		verifySublinkIsPresent("MP3 Players", "test 19");
		verifySublinkIsPresent("MP3 Players", "test 20");
		verifySublinkIsPresent("MP3 Players", "test 21");
		verifySublinkIsPresent("MP3 Players", "test 22");
		verifySublinkIsPresent("MP3 Players", "test 23");
		verifySublinkIsPresent("MP3 Players", "test 24");
		verifySublinkIsPresent("MP3 Players", "test 4");
		verifySublinkIsPresent("MP3 Players", "test 5");
		verifySublinkIsPresent("MP3 Players", "test 6");
		verifySublinkIsPresent("MP3 Players", "test 7");
		verifySublinkIsPresent("MP3 Players", "test 8");
		verifySublinkIsPresent("MP3 Players", "test 9");
		verifySublinkIsPresent("MP3 Players", "Show All MP3 Players");
	}
	
	public static void clickMyAccount() throws IOException  {
		UI.clickElement("My Account", Repository.navigation_MyAccount);
	}
	
	public static void clickLogoutLink() throws IOException {
		UI.clickElement("Logout Link", Repository.navigation_LogoutLink);
	}
	
}
