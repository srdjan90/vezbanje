package onedFullStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ODFS_SignOutPage extends BasePage {


    @FindBy(how = How.XPATH, using = "//a[@href='/products/custom/' and @class]")
    private WebElement btnContinueShopping;

    @FindBy(how = How.XPATH, using = "//a[contains(@href,'signin')]")
    private WebElement btnSignIn;

    public ODFS_SignOutPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public ODFS_SignInPage clickSignIn(){
        clickWhenReady(btnSignIn);
        return new ODFS_SignInPage(driver, wait);
    }

    public ODFS_HomePage clickContinueShopping(){
        clickWhenReady(btnContinueShopping);
        return new ODFS_HomePage(driver, wait);
    }

}
