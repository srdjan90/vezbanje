package Base;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class GlobalVariables {
	
	public static String URL = "http://tutorialsninja.com/demo/index.php?route=common/home";

	public static String firstName = "Marko";
	public static String lastName = "Tester";
	public static String telephoneNumber = "1112223333";
	public static String eMail = "automationtestmj1@mailinator.com";
	public static String password = "temptest1234";
	public static String testName;
	public static String siteName;
	public static String time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd-HH-mm-ss"));

}
