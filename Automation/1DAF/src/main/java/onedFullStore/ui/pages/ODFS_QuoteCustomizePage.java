package onedFullStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ODFS_QuoteCustomizePage extends BasePage {
    public ODFS_QuoteCustomizePage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    @FindBy(how = How.XPATH, using = "//input[@id='quantity']")
    private WebElement txtQuantity;


}
