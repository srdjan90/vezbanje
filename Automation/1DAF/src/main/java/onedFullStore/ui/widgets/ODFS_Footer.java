package onedFullStore.ui.widgets;

import logic.BasePage;
import onedFullStore.ui.pages.ODFS_HelpPage;
import onedFullStore.ui.pages.ODFS_MyAccountPage;
import onedFullStore.ui.pages.ODFS_ProductCategoriesPage;
import onedFullStore.ui.pages.ODFS_ProductListingPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ODFS_Footer extends BasePage {

    @FindBy(css = ".global-footer .equal_col:first-child a") private List<WebElement> linksLearnAndSupport;
    @FindBy(css = ".global-footer .equal_col:nth-child(2) a") private List<WebElement> linksCustomProducts;
    @FindBy(css = ".contact_us a") private List<WebElement> lblsContactUs;

    public ODFS_Footer(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    private enum LearnAndSupport {
        YOUR_ACCOUNT, CUSTOMER_CARE, FAQs, NO_RISK_GUARANTEE, TERMS_AND_CONDITIONS
    }

    private enum CustomProducts {
        PROMOTIONAL_PRODUCTS, APPAREL, MARKETING_MATERIALS
    }

    private enum ContactUs {
        PHONE, EMAIL_FIRST, EMAIL_SECOND
    }

    public ODFS_MyAccountPage clickYourAccount() {
        clickWhenReady(linksLearnAndSupport.get(LearnAndSupport.YOUR_ACCOUNT.ordinal()));
        return new ODFS_MyAccountPage(driver, wait);
    }

    public ODFS_HelpPage clickCustomerCare() {
        clickWhenReady(linksLearnAndSupport.get(LearnAndSupport.CUSTOMER_CARE.ordinal()));
        return new ODFS_HelpPage(driver, wait);
    }

    public ODFS_HelpPage clickFAQs() {
        clickWhenReady(linksLearnAndSupport.get(LearnAndSupport.FAQs.ordinal()));
        return new ODFS_HelpPage(driver, wait);
    }

    public ODFS_ProductCategoriesPage clickPromotionalProducts() {
        clickWhenReady(linksCustomProducts.get(CustomProducts.PROMOTIONAL_PRODUCTS.ordinal()));
        return new ODFS_ProductCategoriesPage(driver, wait);
    }

    public ODFS_ProductListingPage clickApparel() {
        clickWhenReady(linksCustomProducts.get(CustomProducts.APPAREL.ordinal()));
        return new ODFS_ProductListingPage(driver, wait);
    }

    public ODFS_ProductListingPage clickMarketingMaterials() {
        clickWhenReady(linksCustomProducts.get(CustomProducts.APPAREL.ordinal()));
        return new ODFS_ProductListingPage(driver, wait);
    }

    public String getContactUsPhone() {
        return waitForVisible(lblsContactUs.get(ContactUs.PHONE.ordinal())).getText();
    }

    public String getContactUsEmailFirst() {
        return waitForVisible(lblsContactUs.get(ContactUs.EMAIL_FIRST.ordinal())).getText();
    }

    public String getContactUsEmailSecond() {
        return waitForVisible(lblsContactUs.get(ContactUs.EMAIL_SECOND.ordinal())).getText();
    }
}
