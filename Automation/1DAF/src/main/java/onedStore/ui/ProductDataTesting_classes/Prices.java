package onedStore.ui.ProductDataTesting_classes;

public class Prices {

    private String productId;
    private String productName;
    private String productNumber;
    private String retail;
    private String lowRange;
    private String highRange;

    public Prices(String productId, String productName, String productNumber, String retail, String lowRange, String highRange) {
        this.productId = productId;
        this.productName = productName;
        this.productNumber = productNumber;
        this.retail = retail;
        this.lowRange = lowRange;
        this.highRange = highRange;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public String getRetail() {
        return retail;
    }

    public void setRetail(String retail) {
        this.retail = retail;
    }

    public String getLowRange() {
        return lowRange;
    }

    public void setLowRange(String lowRange) {
        this.lowRange = lowRange;
    }

    public String getHighRange() {
        return highRange;
    }

    public void setHighRange(String highRange) {
        this.highRange = highRange;
    }
}
