package onedFullStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class ODFS_SignInPage extends BasePage {


    @FindBy(how = How.XPATH, using = "//input[@id='email']")
    private WebElement txtEmail;

    @FindBy(how = How.XPATH, using = "//input[@id='password']")
    private WebElement txtPassword;

    @FindBy(how = How.XPATH, using = "//a[contains(.,'Sign In')]")
    private WebElement btnSignIn;

    @FindBy(how = How.XPATH, using = "//a[contains(.,'Forgot your password')]")
    private WebElement linkForgotYourPassword;

    @FindBy(how = How.XPATH, using = "//a[contains(@href,'createaccount')]")
    private WebElement btnCreateAnAccount;

    @FindBy(how = How.XPATH, using = "//div[@class='alert alert-warning red center']")
    private WebElement lblErrorMessage;

    public ODFS_SignInPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    /**
     *  METHODS
     */


    public ODFS_SignInPage enterEmail(String email){
        typeIn(txtEmail, email);
        return this;
    }

    public ODFS_SignInPage enterPassword(String password){
        typeIn(txtPassword, password);
        return this;
    }

    public ODFS_CreateAccountPage clickCreateAnAccount(){
        clickWhenReady(btnCreateAnAccount);
        return new ODFS_CreateAccountPage(driver, wait);
    }

    public ODFS_ResetPasswordPage clickForgotYourPassword(){
        clickWhenReady(linkForgotYourPassword);
        return new ODFS_ResetPasswordPage(driver, wait);
    }

    public ODFS_SignInPage verifyErrorMessage(){
        if(!getTextFromElement(lblErrorMessage).contains("Sorry, the email address and password you entered was not recognized")){
            Assert.fail("Error message isn't appeared after entering invalid email.");
        }
        return this;
    }

    public ODFS_HomePage clickSingInButton(){
        clickWhenReady(btnSignIn);
        return new ODFS_HomePage(driver, wait);
    }

    public ODFS_HomePage login(String email, String password){
        this.enterEmail(email)
                .enterPassword(password)
                .clickSingInButton();
        return new ODFS_HomePage(driver, wait);
    }

    public ODFS_SignInPage loginWithInvalidEmail(String invalidEmail, String password){
        enterEmail(invalidEmail)
                .enterPassword(password)
                .clickSingInButton();
        this.verifyErrorMessage();
        return this;
    }

    public ODFS_SignInPage loginWithInvalidPassword(String email, String invalidPassword){
        this.enterEmail(email)
                .enterPassword(invalidPassword)
                .clickSingInButton();
        this.verifyErrorMessage();
        return this;
    }

    public ODFS_SignInPage loginWithEmptyFields () {
        txtEmail.clear();
        txtPassword.clear();
        clickSingInButton();
        if(!txtEmail.getAttribute("class").contains("error5")){
            System.out.println("Email polje nema error u klasi");
        }
        if(!txtPassword.getAttribute("class").contains("error5")){
            System.out.println("Password polje nema error u klasi");
        }
        return this;
    }
}
