/***************************************************************************************************
 Created by vskyba on 9/26/18 7:05 PM
 **************************************************************************************************/

package onedStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ODS_ResetPasswordPage extends BasePage {
    public ODS_ResetPasswordPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }
}
