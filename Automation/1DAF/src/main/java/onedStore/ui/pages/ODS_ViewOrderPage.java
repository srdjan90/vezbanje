package onedStore.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import logic.BasePage;

public class ODS_ViewOrderPage extends BasePage {

	public ODS_ViewOrderPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
	}
	
	@FindBy(xpath="//h1[contains(.,'Order #')]")
	private WebElement lblOrderNumber;
	
	@FindBy(xpath="//td[contains(.,'Sub Total')]/following-sibling::td[last()]")
	private WebElement lblSubTotal;
	
	@FindBy(xpath="//td[contains(.,'Discount')]/following-sibling::td[last()]")
	private WebElement lblDiscount;
	
	@FindBy(xpath="//td[contains(.,'Shipping')]/following-sibling::td[last()]")
	private WebElement lblShipping;
	
	@FindBy(xpath="//td[contains(.,'Tax')]/following-sibling::td[last()]")
	private WebElement lblTax;
	
	@FindBy(xpath="//td[contains(.,'TOTAL')]/following-sibling::td[last()]")
	private WebElement lblTotal;
	
	@FindBy(xpath="//td[contains(.,'Amount Paid')]/following-sibling::td[last()]")
	private WebElement lblAmountPaid;
	
	@FindBy(xpath="//td[contains(.,'ORDER BALANCE')]/following-sibling::td[last()]")
	private WebElement lblOrderBalance;

	@FindBy(xpath="//table[contains(@class,'GreyBox')]//td[contains(.,'Order Date')]//following-sibling::strong")
	private WebElement lblOrderDate;
	
	public ODS_ViewOrderPage verifyOrderNumber(String expectedOrderNumber ) {
			Assert.assertTrue(lblOrderNumber.getText().contains("#"+expectedOrderNumber), 
					"FAIL - The value " +expectedOrderNumber+ " is not being dispayed. The value being displayed is " + lblOrderNumber.getText());
		return this;
	}

	public ODS_ViewOrderPage verifySubTotal(String expectedSubTotal ) {
			Assert.assertTrue(lblSubTotal.getText().contains(expectedSubTotal), 
					"FAIL - The value " +expectedSubTotal+ " is not being dispayed. The value being displayed is " + lblSubTotal.getText());
		return this;
	}
	
	public ODS_ViewOrderPage verifyDiscount(String expectedDiscount) {
			Assert.assertTrue(lblDiscount.getText().contains(expectedDiscount), 
					"FAIL - The value " +expectedDiscount+ " is not being dispayed. The value being displayed is " +lblDiscount.getText());
		return this;
	}
	
	public ODS_ViewOrderPage verifyShipping(String expectedShipping) {
			Assert.assertTrue(lblShipping.getText().contains(expectedShipping), 
					"FAIL - The value " +expectedShipping+ " is not being dispayed. The value being displayed is " +lblShipping.getText());
		return this;
	}
	
	public ODS_ViewOrderPage verifyTax(String expectedTax) {
			Assert.assertTrue(lblTax.getText().contains(expectedTax), 
					"FAIL - The value " +expectedTax+ " is not being dispayed. The value being displayed is " +lblTax.getText());
		return this;
	}
	
	public ODS_ViewOrderPage verifyTotal(String expectedTotal) {
			Assert.assertTrue(lblTotal.getText().contains(expectedTotal), 
					"FAIL - The value " +expectedTotal+ " is not being dispayed. The value being displayed is " +lblTotal.getText());
		return this;
	}
	
	public ODS_ViewOrderPage verifyAmountPaid(String expectedAmountPaid) {
			Assert.assertTrue(lblAmountPaid.getText().contains(expectedAmountPaid), 
					"FAIL - The value " +expectedAmountPaid+ " is not being dispayed. The value being displayed is " +lblAmountPaid.getText());
		return this;
	}
	
	public ODS_ViewOrderPage verifyOrderBalance(String expectedOrderBalance) {
			Assert.assertTrue(lblOrderBalance.getText().contains(expectedOrderBalance), 
					"FAIL - The value " +expectedOrderBalance+ " is not being dispayed. The value being displayed is " +lblOrderBalance.getText());
		return this;
	}
	
	public ODS_ViewOrderPage verifyOrderDate(String expectedDate) {
			Assert.assertTrue(lblOrderDate.getText().contains(expectedDate), 
					"FAIL - The value " +expectedDate+ " is not being dispayed. The value being displayed is " +lblOrderDate.getText());
		return this;
	}
	
}
