package onedStore_tests.ui;

import logic.BaseTest;
import logic.Browser;
import logic.Environments;
import onedFullStore.ui.pages.ODFS_HomePage;
import onedFullStore.ui.pages.ODFS_ProductPage;
import onedStore.ui.ProductDataTesting_classes.ComplexProduct;
import onedStore.ui.ProductDataTesting_classes.ExcelExtraction;
import onedStore.ui.ProductDataTesting_classes.ProductOptions;
import onedStore.ui.ProductDataTesting_classes.Products;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.Utils;

import java.util.ArrayList;
import java.util.stream.IntStream;

public class ODFS_Sample_TestClass extends BaseTest {

    private WebDriver driver;
    private WebDriverWait wait;
    private ODFS_HomePage homePage;




    @BeforeClass(alwaysRun = true)
    private void beforeClass() {
//        driver = Browser.PROPERTY.getInstance();
//        wait = Utils.initWaits(driver, 5, 10, 1);
//        driver.get(Environments.ODFS_UI_URL);
//        logger.info("https://www.deluxe.com/products/custom/" + " URL is opened.");
//        homePage = new ODFS_HomePage(driver, wait);
    }

/*
    //    #### Stefanov Test #####


    @Test(groups = {"smoke"})
    public void test01() throws Exception {
//        homePage.header.openSignIn()
//                .login("stefan-171018@srb.hostopia.com", "Deluxe@123")
//                .header.openCategory("apparel");

        ArrayList<String> list = new ArrayList<>();
        list.add("stefan");
        list.add("marko");
        list.add("zeljko");
        list.add("filip");
        list.add("zoki");
        list.add("milos");

        list.stream()
                .findAny()
                .filter(x -> x.contains("z"));
    }

*/

//    @Test
//    public void test() throws Exception {
//        ArrayList<ComplexProduct> list = ExcelExtraction.getProducts();
//        for(ComplexProduct c: list){
//            System.out.println("Proizvod: " + c.getProduct().getProductId() + " - " + c.getProduct().getProductName());
//            System.out.println("<================================================>");
//            for(ProductOptions p : c.getProductOptions()){
//                System.out.println(p.getOptionId() + " - " + p.getOptionName());
//            }
//        }
//    }


    @Test
    public void test() throws Exception {
        ArrayList<ComplexProduct> listaProdukata = ExcelExtraction.getProducts();
        for (ComplexProduct c : listaProdukata) {
            System.out.println("===========================> " + c.getProduct().getProductId() + "  " + c.getProduct().getProductName());
            for (ProductOptions po : c.getProductOptions()) {
                System.out.println(po.getOptionId() + " | " + po.getOptionName());
            }
        }
    }



    @AfterClass(alwaysRun = true)
    private void tearDown() {
        //driver.quit();
    }
}
