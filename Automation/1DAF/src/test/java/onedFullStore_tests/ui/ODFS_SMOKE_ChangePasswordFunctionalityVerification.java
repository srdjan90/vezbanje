package onedFullStore_tests.ui;

import logic.Browser;
import logic.Environments;
import onedFullStore.ui.pages.ODFS_HomePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import utils.Utils;

public class ODFS_SMOKE_ChangePasswordFunctionalityVerification {

    private WebDriver driver;
    private WebDriverWait wait;
    private ODFS_HomePage homePage;

    @BeforeClass(alwaysRun = true)
    public void setUp(){
        driver = Browser.PROPERTY.getInstance();
        wait = Utils.initWaits(driver, 5, 10, 1);
        driver.get(Environments.ODFS_UI_URL);
        homePage = new ODFS_HomePage(driver, wait);
    }

    @Test(groups = {"smoke"})
    public void ForgotPasswordPage(){
        homePage.header.openSignIn()
                .login("stefan-test-011118@srb.hostopia.com", "Deluxe@123")
//                .header.openMyAccount()
//                .clickChangePassword()
//                .clickYourAccountLink()
//                .clickChangePassword().clickHomeLink()
                .header.openMyAccount().clickChangePassword().cancel()
                .clickChangePassword().saveChangesWithEmptyFields()
                .invalidCurrentPassword("Deluxe", "Deluxe@1234", "Deluxe@1234")
                .invalidNewPassword("Deluxe@123", "deluxe")
                .invalidConfirmPassword("Deluxe@123", "Deluxe@1234", "Deluxe@");


    }

    @AfterClass(alwaysRun = true)
    public void tearDown(){
        driver.quit();
    }

}
