package onedStore.ui.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import logic.BasePage;

public class ODS_DesignerPage extends BasePage{

	public ODS_DesignerPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
		// TODO Auto-generated constructor stub
	}
	
	@FindBy(xpath = "//button[contains(., 'Add to Cart')]")
	WebElement btnAddToCart;
	@FindBy(id ="summary_total")
	WebElement lblTotal;
	
	@FindBy(id = "comments_129_6603")
	WebElement txtSizeQtyXS;
	@FindBy(id ="comments_129_5125")
	WebElement txtSizeQtyS;
	@FindBy(id ="comments_129_5126")
	WebElement txtSizeQtyM;
	@FindBy(id ="comments_129_5127")
	WebElement txtSizeQtyL;
	@FindBy(id ="comments_129_5128")
	WebElement txtSizeQtyXL;
	@FindBy(id ="comments_129_5129")
	WebElement txtSizeQty2XL;
	
	@FindBy(id ="comments_13")
	WebElement txtImprintColorComment;
	@FindAll(@FindBy(xpath ="//div[contains(@class, 'bootstrap-select')]//li/a/div"))
	List<WebElement> ddlItemColors;
	@FindBy(xpath ="//div[@class='ihd-sidebar']//div[contains(@class, 'bootstrap-select ihd-items-wrapper') and not (contains(@style, 'none'))]")
	WebElement ddlItemColor;
	
	public ODS_DesignerPage selectItemColorByColorName (String colorName) {
		
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[contains(@class, 'bs-example-modal-sm in')]")));
		ddlItemColor.click();
		
		for (int i=0; i<2; i++) {
			try {
				ddlItemColors.stream()
					.filter(s->s.getText()
					.equalsIgnoreCase(colorName))
					.forEach(s->s.click());
				break;
			}
			catch (StaleElementReferenceException e) {
				ddlItemColors = driver.findElements(By.xpath("//div[contains(@class, 'bootstrap-select')]//li/a/div"));
			}
		}

		return this;
	}
	
	public ODS_DesignerPage setQtyPerSize (String size , String qty) {
		
		switch (size.toUpperCase()) {
			case "XS":
				typeIn(txtSizeQtyXS, qty);
				txtSizeQtyXS.sendKeys(Keys.TAB);
				break;
			case "S":
				typeIn(txtSizeQtyS, qty);
				txtSizeQtyS.sendKeys(Keys.TAB);
				break;
			case "M":
				typeIn(txtSizeQtyM, qty);
				txtSizeQtyM.sendKeys(Keys.TAB);
				break;
			case "L":
				typeIn(txtSizeQtyL, qty);
				txtSizeQtyL.sendKeys(Keys.TAB);
				break;
			case "XL":
				typeIn(txtSizeQtyXL, qty);
				txtSizeQtyXL.sendKeys(Keys.TAB);
				break;
			case "2XL":
				typeIn(txtSizeQty2XL, qty);
				txtSizeQty2XL.sendKeys(Keys.TAB);
				break;
			} 
		return this;
	}
	
	public ODS_DesignerPage enterImprintColorComments (String comment) {
		typeIn(txtImprintColorComment, comment);
		return this;
	}
	
	public ODS_DesignerPage clickAddToCart() {
		clickWhenReady(btnAddToCart);
		return this;
	}

}
