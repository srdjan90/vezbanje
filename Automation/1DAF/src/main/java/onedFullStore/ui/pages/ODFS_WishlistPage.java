package onedFullStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ODFS_WishlistPage extends BasePage {

    @FindAll(@FindBy(how = How.XPATH, using = "//ul[@class='nav nav-pills nav-stacked']/li/a"))
    private List<WebElement> listWishlistList;

    @FindBy(how = How.XPATH, using = "//button[@id='dropdownMenu1']")
    private WebElement btnSettings;

    @FindAll(@FindBy(how = How.XPATH, using = "//ul[@aria-labelledby='dropdownMenu1']/li/a"))
    private List<WebElement> ddlSettings;

    @FindBy(how = How.XPATH, using = "//button[contains(.,'Make this list public to share')]")
    private WebElement btnMakeThisListPublicToShare;

    @FindBy(how = How.XPATH, using = "//span[@class='glyphicon glyphicon-envelope']")
    private WebElement btnEmail;

    @FindBy(how = How.XPATH, using = "//div[contains(@class,'row wishlist')]//h5")
    private WebElement linkProductName;

    @FindBy(how = How.XPATH, using = "//button[contains(.,'Move to list')]")
    private WebElement btnMoveToList;

    @FindBy(how = How.XPATH, using = "//button[contains(.,'Compare product')]")
    private WebElement btnCompareProduct;

    @FindBy(how = How.XPATH, using = "//div[contains(@class,'row wishlist')]")
    private List<WebElement> listProducts;

    @FindBy(how = How.XPATH, using = "//input[@type='text' and @id='cartname']")
    private WebElement txtListName;

    @FindBy(how = How.XPATH, using = "//select[@id='default']")
    private WebElement ddlMakeDefaultList;

    @FindBy(how = How.XPATH, using = "//input[@id='public']")
    private WebElement rdoPublic;

    @FindBy(how = How.XPATH, using = "//input[@id='private']")
    private WebElement rdoPrivate;

    @FindBy(how = How.XPATH, using = "//form[@id='form_edit']//button[@type='button' and text()='Close']")
    private WebElement btnEditClose;

    @FindBy(how = How.XPATH, using = "//form[@id='form_edit']//input[@type='submit']")
    private WebElement btnEditSaveChanges;

    @FindBy(how = How.XPATH, using = "//input[@id='emailTo']")
    private WebElement txtEmailTo;

    @FindBy(how = How.XPATH, using = "//textarea[@name='emailBody']")
    private WebElement txtEmailBody;

    @FindAll(@FindBy(how = How.XPATH, using = "//table[@class='table']//tr"))
    private List<WebElement> tblWishlists;

    @FindBy(how = How.XPATH, using = "//button[contains(.,'View Comparisons')]")
    private WebElement btnViewComparisons;

    @FindBy(how = How.XPATH, using = "//button[contains(.,'Ok')]")
    private WebElement btnOk;


    public ODFS_WishlistPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    /**
     * METHODS
     */

    public ODFS_WishlistPage clickEditClose(){
        clickWhenReady(btnEditClose);
        return this;
    }

    public ODFS_WishlistPage clickEditSaveChanges(){
        clickWhenReady(btnEditSaveChanges);
        return this;
    }

    public ODFS_WishlistPage selectPublicListVisibility(){
        clickWhenReady(rdoPublic);
        return this;
    }

    public ODFS_WishlistPage selectPrivateListVisibility(){
        clickWhenReady(rdoPrivate);
        return this;
    }

    public ODFS_WishlistPage enterListName(String listName){
        typeIn(txtListName, listName);
        return this;
    }

    public ODFS_WishlistPage choseMakeDefaultList(String answer){
        chooseValueFromSelect(ddlMakeDefaultList, answer);
        return this;
    }

    public ODFS_WishlistPage selectWhishList(String wishlistName){
        for(WebElement w: listWishlistList){
            if(getTextFromElement(w).toLowerCase().contains(wishlistName.toLowerCase())){
                clickWhenReady(w);
                break;
            }
        }
        return this;
    }

    public ODFS_WishlistPage clickEditListName(){
        clickWhenReady(btnSettings);
        for(WebElement w : ddlSettings){
            if(getTextFromElement(w).toLowerCase().contains("Edit List Name".toLowerCase())){
                clickWhenReady(w);
                break;
            }
        }
        return this;
    }

    public ODFS_WishlistPage clickMakeDefault(){
        clickWhenReady(btnSettings);
        for(WebElement w : ddlSettings){
            if(getTextFromElement(w).toLowerCase().contains("Make Default".toLowerCase())){
                clickWhenReady(w);
                break;
            }
        }
        return this;
    }

    public ODFS_WishlistPage clickDelete(){
        clickWhenReady(btnSettings);
        for(WebElement w : ddlSettings){
            if(getTextFromElement(w).toLowerCase().contains("Delete".toLowerCase())){
                clickWhenReady(w);
                break;
            }
        }
        return this;
    }

    public ODFS_WishlistPage clickMakeThisPublicToShare(){
        clickWhenReady(btnMakeThisListPublicToShare);
        return this;
    }

    public ODFS_WishlistPage clickEmailThisList(){
        clickWhenReady(btnEmail);
        return this;
    }

    public ODFS_WishlistPage clickProductLink(){
        clickWhenReady(linkProductName);
        return this;
    }

    public ODFS_WishlistPage clickMoveToListonProduct(String productName){
        for(WebElement w : listProducts){
            if(getTextFromElement(w).contains(productName)){
                clickWhenReady(w.findElement(By.xpath("div[@class='col-sm-10 col-xs-8']/button[contains(.,'Move')]")));
                break;
            }
        }
        return this;
    }

    public ODFS_WishlistPage clickCompareProduct(){
        clickWhenReady(btnCompareProduct);
        return this;
    }

    public ODFS_WishlistPage moveProductToOtherWishlist(String wishlistName){
        for(WebElement w : tblWishlists){
            if(getTextFromElement(w).toLowerCase().contains(wishlistName.toLowerCase())){

                clickWhenReady(w.findElement(By.xpath("td[2]/button[contains(.,'Move')]")));
                break;
            }
        }
        return this;
    }

    public ODFS_WishlistPage copyProductToOtherWishlist(String wishlistName){
        for(WebElement w : tblWishlists){
            if(getTextFromElement(w).toLowerCase().contains(wishlistName.toLowerCase())){

                clickWhenReady(w.findElement(By.xpath("td[2]/button[contains(.,'Copy')]")));
                break;
            }
        }
        return this;
    }

    public ODFS_WishlistPage removeProductFromWishlist(String productName){
        for(WebElement w : listProducts){
            if(getTextFromElement(w).contains(productName)){
                clickWhenReady(w.findElement(By.xpath("div[@class='col-sm-10 col-xs-8']/button[contains(.,'Remove')]")));
                break;
            }
        }
        return this;
    }

    public ODFS_WishlistPage clickViewComparisons(){
        clickWhenReady(btnViewComparisons);
        return this;
    }

    public ODFS_WishlistPage clickOk(){
        clickWhenReady(btnOk);
        return this;
    }


}
