/***************************************************************************************************
 Created by vskyba on 9/26/18 7:06 PM
 **************************************************************************************************/

package onedStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ODS_SignInPage extends BasePage {

    @FindBy(css = "[name='email']") private WebElement txtEmail;
    @FindBy(css = "[type='password']") private WebElement txtPassword;
    @FindBy(css = "[type='submit']") private WebElement btnSignIn;
    @FindBy(css = "#frmlogin a") private WebElement btnCreateAccount;
    @FindBy(css = ".center-block .text-center a") private WebElement btnPasswordReset;

    public ODS_SignInPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

//    Module methods

    private ODS_SignInPage typeEmail(String inputText) {
        typeIn(txtEmail, inputText);
        return this;
    }

    private ODS_SignInPage typePassword(String inputText) {
        typeIn(txtPassword, inputText);
        return this;
    }

    private ODS_MyAccountPage clickBtnSignIn() {
        clickWhenReady(btnSignIn);
        return new ODS_MyAccountPage(driver, wait);
    }

    private ODS_CreateAccountPage clickBtnCreateAccount() {
        clickWhenReady(btnCreateAccount);
        return new ODS_CreateAccountPage(driver, wait);
    }

    private ODS_ResetPasswordPage clickBtnPasswordReset() {
        clickWhenReady(btnPasswordReset);
        return new ODS_ResetPasswordPage(driver, wait);
    }

//    Methods

    public ODS_MyAccountPage signIn(String email, String password) {
        typeEmail(email).typePassword(password)
                        .clickBtnSignIn();
        return new ODS_MyAccountPage(driver, wait);
    }

}
