package MyStore_Tests;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Base.Browser;
import Base.GlobalVariables;
import Base.Report;
import MyStore_Pages.HomePage;
import MyStore_Pages.MyStore_GlobalVariables;
import MyStore_Pages.ProductDetailsPage;
import MyStore_Pages.SearchResultPage;
import TheNinjaStore_Pages.NinjaStore_GlobalVariables;

public class FirstTest {

	private static String productName = "Printed Summer Dress";


	@BeforeMethod(alwaysRun = true)
	public void setUp() throws Throwable {
		Browser.url = MyStore_GlobalVariables.URL;
		Report.passed = true;
		GlobalVariables.siteName = "MyStore";
		GlobalVariables.testName = "MyStore_"+this.getClass().getSimpleName();
	}

    @Test
    public static void start() throws Throwable {
        Browser.openBrowser();

        HomePage.verifyHeaderPhoneNumber();
        //HomePage.clickWomanMegaLink();
//        HomePage.hoverWomanMegaLink();
//        Thread.sleep(2000);
//        HomePage.clickWomanTshirtsSubcategoryLink();
        HomePage.hoverWomanMegaLink();
        Thread.sleep(2000);
        HomePage.clickWomanBlousesSubcategoryLink();
        Thread.sleep(2000);
        HomePage.selectFromSortByDropdown("Price: Highest first");
        HomePage.enterTextIntoSearchField("dress");
        HomePage.clickSearchButton();
        SearchResultPage.hoverSearchResult(productName);
        SearchResultPage.selectColor("blue", productName);
        ProductDetailsPage.clickProductImage(productName);
        Thread.sleep(2000);
        ProductDetailsPage.verifyLargeProductImageIsPresent();
        

    }
    
    @AfterMethod(alwaysRun = true)
	public void tearDown() throws IOException {
		Report.writeReport(GlobalVariables.testName);
		Browser.closeBrowser();
	}
}
