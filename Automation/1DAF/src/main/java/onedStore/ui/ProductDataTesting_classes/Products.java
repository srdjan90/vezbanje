package onedStore.ui.ProductDataTesting_classes;

public class Products {

    private String productId;
    private String productName;
    private String productNumber;
    private String productWeight;

    public Products(String productId, String productName, String productNumber, String productWeight) {
        this.productId = productId;
        this.productName = productName;
        this.productNumber = productNumber;
        this.productWeight = productWeight;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public String getProductWeight() {
        return productWeight;
    }

    public void setProductWeight(String productWeight) {
        this.productWeight = productWeight;
    }
}
