package onedStore.ui.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import junit.framework.Assert;
import logic.BasePage;

public class ODS_CartPage extends BasePage{

	public ODS_CartPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
		// TODO Auto-generated constructor stub
	}

	/*
	 * GLOBAL CART ELEMENTS
	 */
	@FindBy(css = "#productTotal")
	private WebElement lblTotal;
	
	@FindBy(xpath = "//a[contains(., 'Checkout')]")
	private WebElement btnCheckout;
	
	@FindBy(partialLinkText = "Continue Shopping")
	private WebElement linkContinueShopping;
	
	@FindBy(id = "savedForLaterTitle")
	private WebElement lblSavedForLaterTitle;
	
	@FindBy(id = "cart_noitems")
	private WebElement lblEmptyCartMessage;
	
	/*
	 * CART_ITEM ELEMENTS
	 */
	@FindBy(id = "salesCart")
	private WebElement pnlCartItems;
	
	@FindBy(xpath = "//div[@id='salesCart']/div[contains(@class, 'panel-default') and not (contains(@style, 'none'))]//a[contains(., 'Remove')]")
	private WebElement btnRemoveCartItem;
	
	@FindBy(xpath = "//a[contains(., 'Modify')]")
	private WebElement btnModifyItem;
	
	@FindBy(xpath = "//button[contains(text(), 'Save for later')]")
	private WebElement linkSaveForLater;
	
	@FindBy(css = ".subTotal")
	private WebElement lblSubTotal;
	
	@FindBy(xpath = "//button[contains(.,'OK')]")
	private WebElement btnRemoveOk;
	
	/*
	 * SAVED_ITEM ELEMENTS
	 */	
	@FindBy(id = "savedCart")
	private WebElement pnlSavedCartItems;
	
	@FindBy(xpath = "//div[@id='savedCart']/div[contains(@class, 'panel-default') and not (contains(@style, 'none'))]//a[contains(., 'Remove')]")
	private WebElement btnRemoveSavedCartItem;
	
	@FindBy(xpath = "//button[contains(., 'Move to Cart')]")
	private WebElement btnMoveToCartItem;
	
	/*
	 * GLOBAL CART METHODS
	 */	
	public ODS_CartPage clickContinueShopping() {
		clickWhenReady(linkContinueShopping);
		return this;
	}
	
	/*
	 * CART_ITEM METHODS
	 */
	public ODS_CartPage clickRemoveFirstProduct() {
		clickWhenReady(btnRemoveCartItem);
		clickWhenReady(btnRemoveOk);
		return this;
	}
	
	public ODS_CartPage clickModifyFirstProduct() {
		clickWhenReady(btnModifyItem);
		return this;
	}

	public ODS_CartPage clickSaveForLaterFirstProduct() {
		clickWhenReady(linkSaveForLater);
		return this;
	}

	public ODS_CartPage deleteAllCartItems() throws InterruptedException {	
			int counter = driver.findElements(By.xpath("//div[@id='salesCart']/div[contains(@class, 'panel-default') and not (contains(@style, 'none'))]//a[contains(., 'Remove')]")).size();
			for (int i=0; i<counter; i++) {
				for (int j=0; j<2; j++) {
					try {
						clickWhenReady(btnRemoveCartItem);
						break;
					} catch (StaleElementReferenceException ex) {
						btnRemoveCartItem = driver.findElement(By.xpath("//div[@id='salesCart']/div[contains(@class, 'panel-default') and not (contains(@style, 'none'))]//a[contains(., 'Remove')]"));
					} 
				}
				clickWhenReady(btnRemoveOk);
				wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[contains(@class, 'modal-backdrop fade in')]")));
			}
			verifyEmptyCart();
			return this;
		}
	
	public ODS_CartPage verifyEmptyCart() {
		Assert.assertEquals(true, lblEmptyCartMessage.isDisplayed());
		return this;
	}
	
	/*
	 * SAVED_ITEM METHODS
	 */
	public ODS_CartPage clickRemoveSavedFirstProduct() {
		clickWhenReady(btnRemoveCartItem);
		return this;
	}
	
	public ODS_CartPage clickMoveToCartSavedFirstProduct() {
		clickWhenReady(btnMoveToCartItem);
		return this;
	}
	
	public ODS_CartPage deleteAllSavedItems() {
		int counter = driver.findElements(By.xpath("//div[@id='savedCart']/div[contains(@class, 'panel-default') and not (contains(@style, 'none'))]//a[contains(., 'Remove')]")).size();
		for (int i=0; i<counter; i++) {
			for (int j=0; j<2; j++) {
				try {
					clickWhenReady(btnRemoveSavedCartItem);
					break;
				} catch (StaleElementReferenceException ex) {
					btnRemoveSavedCartItem = driver.findElement(By.xpath("//div[@id='savedCart']/div[contains(@class, 'panel-default') and not (contains(@style, 'none'))]//a[contains(., 'Remove')]"));
				} 
			}
			clickWhenReady(btnRemoveOk);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[contains(@class, 'modal-backdrop fade in')]")));
		}
		return this;
	}
}
