package onedStore.ui.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import logic.BasePage;

public class ODS_CartInterstitialPage extends BasePage{

	public ODS_CartInterstitialPage(WebDriver driver, WebDriverWait wait) {
		super(driver, wait);
		
	}

	/**
	 * Elements
	 */
	
	@FindBy(xpath = "//a[contains(@href,'checkout')]") private WebElement btnCheckout;
	@FindBy(xpath = "//a[contains(.,'View Your Cart')]") private WebElement btnViewYourCart;
	@FindBy(xpath = "//a[contains(.,'Continue Shopping')]") private WebElement btnContinueShopping;
	@FindBy(xpath = "//img[@class='img-thumbnail']") private WebElement imgProductImage;
	@FindBy(xpath = "//div[@class='col-sm-6']/h2/a") private WebElement linkProductName;
	@FindBy(xpath = "//div[@class='col-sm-6']/span[1]") private WebElement lblQuantity;
	@FindBy(xpath = "//div[@class='col-sm-6']/span[2]") private WebElement lblProductSubtotal;
	@FindBy(xpath = "//div[@class='col-sm-6']/strong") private WebElement lblReadyToCheckout;
	
	
	/**
	 * Methods
	 */
	
	public ODS_CheckoutPage clickCheckout() {
		clickWhenReady(btnCheckout);
		return new ODS_CheckoutPage(driver, wait);
	}
	
	public ODS_CartPage clickViewYourCart() {
		clickWhenReady(btnViewYourCart);
		return new ODS_CartPage(driver, wait);
	}
	
	public ODS_HomePage clickContinueShopping() {
		clickWhenReady(btnContinueShopping);
		return new ODS_HomePage(driver, wait);
	}
	
	public ODS_ProductPage clickProductImage() {
		clickWhenReady(imgProductImage);
		return new ODS_ProductPage(driver, wait);
	}
	
	public ODS_ProductPage clickProducNameLink() {
		clickWhenReady(linkProductName);
		return new ODS_ProductPage(driver, wait);
	}
	
	public ODS_CartInterstitialPage verifyProductQuantity(String productQuantity) {
		Assert.assertEquals(getTextFromElement(lblQuantity), productQuantity, "Product quantity is not correct. Test failed!");
		return this;
		
	}
	
	public ODS_CartInterstitialPage verifyProductSubtotal(String productSubtotal) {
		Assert.assertEquals(getTextFromElement(lblProductSubtotal).substring(1), productSubtotal, "Product subtotal is not correct. Test failed!");
		return this;
	}
	
	public ODS_CartInterstitialPage verifyReadyToCheckout(String numberOfProductsInCart, String cartAmount) {
		String readyToCheckout = getTextFromElement(lblReadyToCheckout);
		String[] split = readyToCheckout.split(" ");
		String numberOfProductInCartActual = split[0];
		String cartAmountActual = split[2].substring(1, split[2].length());
		Assert.assertEquals(numberOfProductInCartActual, numberOfProductsInCart, "Number of products in cart are not correct. Test failed!");
		Assert.assertEquals(cartAmountActual, cartAmount, "Cart amount is not correct. Test failed!");
		return this;
	}
	
	
	
	
}
