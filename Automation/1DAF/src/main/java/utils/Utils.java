package utils;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.NoSuchElementException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Screen;

import static utils.Consts.EMPTY_STRING;

public final class Utils {
	
	private Utils() {}

    public static WebDriverWait initWaits (WebDriver driver, int iWait, int eWait, int pollingTime) {
        driver.manage().timeouts().implicitlyWait(iWait, TimeUnit.SECONDS);
        WebDriverWait wait = new WebDriverWait(driver, eWait);
        wait.pollingEvery(pollingTime, TimeUnit.SECONDS);
        return wait;
    }

    public static String getCurrentLocalDateTimeStamp() {
        return LocalDateTime.now()
                .format(DateTimeFormatter.ofPattern("ddMMyy.HHmmSS"));
    }

    public static int getRandomNumber(int min, int max){
        return new Random().nextInt((max - min) + 1) +min;
    }

    /**
     * @param pattern {@link Consts#ALPHANUMERIC_STRING} / {@link Consts#ALPHA_STRING_UPPERCASE} / {@link Consts#ALPHA_STRING_LOWERCASE} / {@link Consts#NUMERIC_STRING}
     */
    public static String getRandomString(String pattern, int length){
        StringBuilder builder = new StringBuilder();
        while (length-- !=0){
            int character = (int)(Math.random()*pattern.length());
            builder.append(pattern.charAt(character));
        }
        return builder.toString();
    }

    public static void captureScreenshot (String fileName, WebDriver driver){
    try {
        WebDriver augmentedDriver = new Augmenter().augment(driver);
        File source = ((TakesScreenshot)augmentedDriver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(source, new File(".//src/main/resources/screenshots/" + fileName + ".png"));
    }
    catch(IOException e) {
        e.printStackTrace();
    }
}

    public static void clickBySikuli(String patternPath) {
        Screen s = new Screen();
        org.sikuli.script.Pattern button = new org.sikuli.script.Pattern(
                ".//src/main/resources/sikuliFiles" + "/" + patternPath);
        try {
            s.wait(button.similar(0.5F), 10);
            s.click(button.similar(0.5F));
        } catch (FindFailed ex) {
            throw new NoSuchElementException(ex.getMessage());
        }
    }
} 
