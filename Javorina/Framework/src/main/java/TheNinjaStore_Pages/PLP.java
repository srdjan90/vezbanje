package TheNinjaStore_Pages;

import static org.testng.Assert.fail;

import java.io.IOException;

import org.openqa.selenium.WebElement;

import Base.UI;

public class PLP {
	
	public static void verifyProductImageIsPresent(String productName) throws IOException {
		UI.verifyElementIsPresent(productName, "//div[@class='product-thumb']//img[@alt='"+productName+"']");
	}
	
	public static void clickProductImage(String productName) throws IOException {
		UI.clickElement(productName, "//div[@class='product-thumb']//img[@alt='"+productName+"']");
	}
	
	public static void clickProductName(String productName) throws IOException {
		UI.clickElement(productName, "//div[@class='product-thumb']//h4/a[normalize-space()='"+productName+"']");
	}
	
	public static void verifyProductPrice(String productName, String expectedPrice) throws IOException {
		WebElement temp = UI.findElementByXpath("Product Price","//div[@class='product-thumb']//h4[contains(normalize-space(),'"+productName+"')]/following-sibling::p[@class='price']");
		if (temp.getText().contains(expectedPrice)) {
			System.out.println("Pass - "+productName+" product total is " + expectedPrice);
		} else {
			System.out.println("Fail - expected "+productName+" product total was " + expectedPrice + " but was -- " + temp.getText() + " --");
			fail("Fail - expected "+productName+" product total was " + expectedPrice + " but was -- " + temp.getText() + " --");
		}
	}
	
	public static void verifyProductExTax(String productName, String expectedExTax) throws IOException {
		WebElement temp = UI.findElementByXpath("Product ext price","//div[@class='product-thumb']//h4[contains(normalize-space(),'"+productName+"')]/following-sibling::p[@class='price']");
		if (temp.getText().contains("Ex Tax: "+expectedExTax)) {
			System.out.println("Pass - product "+productName+" Ex Tax is " + expectedExTax);
		} else {
			System.out.println("Fail - expected product "+productName+" Ex Tax was " + expectedExTax + " but was -- " + temp.getText() + " --");
			fail("Fail - expected product "+productName+" Ex Tax was " + expectedExTax + " but was -- " + temp.getText() + " --");
		}
	}
	
	public static void clickAddToCartButton(String productName) throws IOException {
		UI.clickElement("Add to Cart Button", "//div[@class='product-thumb' and contains(normalize-space(),'"+productName+"')]//button[contains(.,'Add to Cart')]");
	}

}
