package TheNinjaStore_Pages;

import java.io.IOException;

import Base.UI;
import TheNinjaStore_Repository.Repository;

public class LoginPage {
	
	public static void clickContinueButton() throws IOException {
		UI.clickElement("Continue Button", Repository.loginPage_ContinueButton);
	}
	
	public static void clickLoginButton() throws IOException {
		UI.clickElement("Login Button", Repository.loginPage_LoginButton);
	}
	
	public static void enterTextIntoEmailAddressField(String valueToEnter) throws IOException {
		UI.enterTextIntoElement("Email Address Field", Repository.loginPage_EmailAddressField, valueToEnter);
	}

	public static void enterTextIntoPasswordField(String valueToEnter) throws IOException {
		UI.enterTextIntoElement("Password Field", Repository.loginPage_PasswordField, valueToEnter);
	}
	
	public static void clickForgottenPasswordLink() throws IOException {
		UI.clickElement("Forgotten Password Link", Repository.loginPage_ForgottenPasswordLink);
	}
	
	public static void enterTextIntoEmailField(String email) throws IOException {
		UI.enterTextIntoElement("Email Address Field", Repository.loginPage_EmailAddressField, email);
	}
}
