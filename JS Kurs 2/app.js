function sabiranje() {
    let a = document.mojaForma1.vrednost1.value;
    let b = document.mojaForma1.vrednost2.value;
    let checkForErrors = false;

    // Ispitujemo samo za prvi input

    if (isNaN(a) == true) {
        checkForErrors = true;
        error = 'Greska u polju!'; 
        document.getElementById('greska1').innerHTML = error;
    } else if (a == '') {
        checkForErrors = true;
        error = 'Polje je obavezno!'; 
        document.getElementById('greska1').innerHTML = error;
    } else {
        checkForErrors = false;
        document.getElementById('greska1').innerHTML = '';
    }
    // Ispitujemo samo za drugi input
    if (isNaN(b) == true) {
        checkForErrors = true;
        error = 'Greska u polju!';
        document.getElementById('greska2').innerHTML = error;
    } else if (b == '') {
        checkForErrors = true;
        error = 'Polje je obavezno!'; 
        document.getElementById('greska2').innerHTML = error;
    } else {
        checkForErrors = false;
        document.getElementById('greska2').innerHTML = '';
    }

    // #####################################################

    if (document.getElementById('greska1').innerHTML === '' && document.getElementById('greska2').innerHTML === '') {
        let c = parseFloat(a) + parseFloat(b);
        document.mojaForma1.rezultat1.value = c.toFixed(2);
    } else if (document.getElementById('greska1').innerHTML !== '' || document.getElementById('greska2').innerHTML !== '') {
        noResults = ' ';
        document.mojaForma1.rezultat1.value = noResults;
    }
}

function oduzimanje() {
    let x = document.mojaForma2.vrednost3.value;
    let y = document.mojaForma2.vrednost4.value;
    let checkForErrors2 = false;

    // Ispitujemo samo za prvi input

    if (isNaN(x) == true) {
        checkForErrors2 = true;
        error = 'Greska u polju!'; 
        document.getElementById('greska3').innerHTML = error;
    } else if (x == '') {
        checkForErrors2 = true;
        error = 'Polje je obavezno!'; 
        document.getElementById('greska3').innerHTML = error;
    } else {
        checkForErrors2 = false;
        document.getElementById('greska3').innerHTML = '';
    }
    // Ispitujemo samo za drugi input
    if (isNaN(y) == true) {
        checkForErrors2 = true;
        error = 'Greska u polju!';
        document.getElementById('greska4').innerHTML = error;
    } else if (y == '') {
        checkForErrors2 = true;
        error = 'Polje je obavezno!'; 
        document.getElementById('greska4').innerHTML = error;
    } else {
        checkForErrors2 = false;
        document.getElementById('greska4').innerHTML = '';
    }

    // #####################################################

    if (document.getElementById('greska3').innerHTML === '' && document.getElementById('greska4').innerHTML === '') {
        let z = parseFloat(x) - parseFloat(y);
        document.mojaForma2.rezultat2.value = z.toFixed(2);
    } else if (document.getElementById('greska3').innerHTML !== '' || document.getElementById('greska4').innerHTML !== '') {
        noResults = ' ';
        document.mojaForma2.rezultat2.value = noResults;
    }
}

