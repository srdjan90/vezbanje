/*
 * Created by ekarliuchenko on 9/26/18 7:20 PM
 */

package onedStore.ui.widgets;

import onedStore.ui.pages.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import logic.BasePage;

public class ODS_Header extends BasePage {

    //region HeaderGeneral
    @FindBy(css = ".navbar-brand") private WebElement imgLogo;
    @FindBy(css = "#nav_header #ihsinput") private WebElement txtSearch;
    @FindBy(css = "#btnSearch") private WebElement btnSearch;
    @FindBy(css = "#nav_header a[href='/store/']") private WebElement linkProducts;
    @FindBy(css = "#nav_header a[href='/store/Help.cshtml']") private WebElement linkHelp;
    @FindBy(css = "#nav_header a[href='/store/secure/myaccount/']") private WebElement linkSignIn;
    @FindBy(css = "#nav_header a[href='/store/cart.cshtml']") private WebElement linkCart;
    @FindBy(css = "#nav_header .dropdown-toggle") private WebElement ddMyAccount;
    @FindBy(css = "#nav_header .dropdown-menu a[href='/store/secure/myaccount/']") private WebElement linkMyAccount;
    @FindBy(css = "#nav_header .dropdown-menu a[href*='signout']") private WebElement linkSignOut;
    @FindBy(css = ".badge.cartcount") private WebElement cntrCartItems;
    //endregion

    //region ProductsMenu
    @FindBy(css = ".navbar a[href='/store/apparel/']") private WebElement linkApparel;
    @FindBy(css = ".navbar a[href='/store/bags/']") private WebElement linkBags;
    @FindBy(css = ".navbar a[href='/store/drinkware/']") private WebElement linkDrinkware;
    @FindBy(css = ".navbar a[href='/store/electronics-computers/']") private WebElement linkElectronics;
    @FindBy(css = ".navbar a[href='/store/corporate-gifts/']") private WebElement linkCorporateGifts;
    @FindBy(css = ".navbar a[href='/store/writing-instruments/']") private WebElement linkWriting;
    @FindBy(css = ".navbar a[href='/store/automotive-items/']") private WebElement linkAutomotive;
    @FindBy(css = ".navbar a[href='/store/banners-signs/']") private WebElement linkBannerMatSign;
    @FindBy(css = ".navbar a[href='/store/buttons-stickers/']") private WebElement linkButtonStickerPatch;
    @FindBy(css = ".navbar a[href='/store/clocks-watches/']") private WebElement linkClockWatch;
    @FindBy(css = ".navbar a[href='/store/fans/']") private WebElement linkFans;
    @FindBy(css = ".navbar a[href='/store/flashlights-knives-tools/']") private WebElement linkFlashlightTool;
    @FindBy(css = ".navbar a[href='/store/food-drink/']") private WebElement linkFoodBeverage;
    @FindBy(css = ".navbar a[href='/store/golf-products/']") private WebElement linkGolfProducts;
    @FindBy(css = ".navbar a[href='/store/household/']") private WebElement linkHouseholdItems;
    @FindBy(css = ".navbar a[href='/store/journals-organizers-portfolios/']") private WebElement linkJournals;
    @FindBy(css = ".navbar a[href='/store/keychains/']") private WebElement linkKeychains;
    @FindBy(css = ".navbar a[href='/store/lanyards-badges-bracelets/']") private WebElement linkLanyardBadge;
    @FindBy(css = ".navbar a[href='/store/magnets/']") private WebElement linkMagnets;
    @FindBy(css = ".navbar a[href='/store/notepads/']") private WebElement linkNotepads;
    @FindBy(css = ".navbar a[href='/store/office/']") private WebElement linkOffice;
    @FindBy(css = ".navbar a[href='/store/outdoor-sporting-goods/']") private WebElement linkOutdoor;
    @FindBy(css = ".navbar a[href='/store/party-supplies-toys-games/']") private WebElement linkPartyGames;
    @FindBy(css = ".navbar a[href='/store/school-spirit-items/']") private WebElement linkSchoolSpirit;
    @FindBy(css = ".navbar a[href='/store/stress-balls/']") private WebElement linkStreetBalls;
    @FindBy(css = ".navbar a[href='/store/travel-personal-care-items/']") private WebElement linkTravelHealth;
    //endregion

    public ODS_Breadcrumb breadcrumb;

    public ODS_Header(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
        breadcrumb = new ODS_Breadcrumb(driver, wait);
    }

    public ODS_SearchPage search(String searchText) {
        typeIn(txtSearch, searchText);
        clickWhenReady(btnSearch);
        return new ODS_SearchPage(driver, wait);
    }

    public ODS_SignInPage clickSignIn() {
        clickWhenReady(linkSignIn);
        return new ODS_SignInPage(driver, wait);
    }

    public ODS_Header expandMyAccount() {
        clickWhenReady(ddMyAccount);
        return this;
    }

    public ODS_MyAccountPage goToMyAccount() {
        expandMyAccount().clickWhenReady(linkMyAccount);
        return new ODS_MyAccountPage(driver, wait);
    }

    public ODS_HomePage signOut() {
        expandMyAccount().clickWhenReady(linkSignOut);
        return new ODS_HomePage(driver, wait);
    }

    public ODS_HelpPage clickHelp() {
        clickWhenReady(linkHelp);
        return new ODS_HelpPage(driver, wait);
    }

    public ODS_CartPage clickCart() {
        clickWhenReady(linkCart);
        return new ODS_CartPage(driver, wait);
    }

    public int getCartItemsAmount() {
        return Integer.parseInt(waitForVisible(cntrCartItems).getText());
    }

    public boolean isCartAmountMatched(int expAmount) {
        return expAmount == getCartItemsAmount();
    }

    public ODS_Header hoverOverProducts() {
        hoverOver(linkProducts);
        return this;
    }

    public ODS_ProductsListingPage openApparel() {
        hoverOverProducts().clickWhenReady(linkApparel);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openBags() {
        hoverOverProducts().clickWhenReady(linkBags);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openDrinkware() {
        hoverOverProducts().clickWhenReady(linkDrinkware);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openElectronics() {
        hoverOverProducts().clickWhenReady(linkElectronics);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openCorporateGifts() {
        hoverOverProducts().clickWhenReady(linkCorporateGifts);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openWriting() {
        hoverOverProducts().clickWhenReady(linkWriting);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openAutomotive() {
        hoverOverProducts().clickWhenReady(linkAutomotive);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openBannerMatSign() {
        hoverOverProducts().clickWhenReady(linkBannerMatSign);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openButtonStickerPatch() {
        hoverOverProducts().clickWhenReady(linkButtonStickerPatch);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openClockWatch() {
        hoverOverProducts().clickWhenReady(linkClockWatch);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openFans() {
        hoverOverProducts().clickWhenReady(linkFans);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openFlashlightTool() {
        hoverOverProducts().clickWhenReady(linkFlashlightTool);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openFoodBeverage() {
        hoverOverProducts().clickWhenReady(linkFoodBeverage);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openGolfProducts() {
        hoverOverProducts().clickWhenReady(linkGolfProducts);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openHouseholdItems() {
        hoverOverProducts().clickWhenReady(linkHouseholdItems);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openJournals() {
        hoverOverProducts().clickWhenReady(linkJournals);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openKeychains() {
        hoverOverProducts().clickWhenReady(linkKeychains);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openLanyardBadge() {
        hoverOverProducts().clickWhenReady(linkLanyardBadge);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openMagnets() {
        hoverOverProducts().clickWhenReady(linkMagnets);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openNotepads() {
        hoverOverProducts().clickWhenReady(linkNotepads);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openOffice() {
        hoverOverProducts().clickWhenReady(linkOffice);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openOutdoor() {
        hoverOverProducts().clickWhenReady(linkOutdoor);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openPartyGames() {
        hoverOverProducts().clickWhenReady(linkPartyGames);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openSchoolSpirit() {
        hoverOverProducts().clickWhenReady(linkSchoolSpirit);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openStreetBalls() {
        hoverOverProducts().clickWhenReady(linkStreetBalls);
        return new ODS_ProductsListingPage(driver, wait);
    }

    public ODS_ProductsListingPage openTravelHealth() {
        hoverOverProducts().clickWhenReady(linkTravelHealth);
        return new ODS_ProductsListingPage(driver, wait);
    }
}
