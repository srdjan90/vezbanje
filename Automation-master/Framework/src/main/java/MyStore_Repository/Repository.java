package MyStore_Repository;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import Base.Browser;



public class Repository {
	
	 public static WebElement findElementByXpath(String xPath) {
	    	WebElement element = Browser.driver.findElement(By.xpath(xPath));
	    	return element;
	    }
	 
	// Home Page 
    public static String signInButton = "//a[@class='login']";
    public static String contactUsButton = "//a[@title='Contact Us']";
    public static String callUsNowHeader = "//span[@class='shop-phone']";
    public static String womanMegaLink = "//ul[@class='sf-menu clearfix menu-content sf-js-enabled sf-arrows']/li/a[@title='Women']";
    public static String womanTshirtsSubcategory = "//ul[@class='submenu-container clearfix first-in-line-xs']/li[contains(.,'Tops')]//a[@title='T-shirts']";
    public static String womanBlousesSubcategory =  "//ul[@class='submenu-container clearfix first-in-line-xs']/li[contains(.,'Tops')]//a[@title='Blouses']";
    public static String sortByDropdown =  "//select[@id='selectProductSort']";
    public static String searchField = "//input[@id='search_query_top']";
    public static String searchButton = "//button[@name='submit_search']";
    
    // Search Result page
   
    
    //Product Details Page
    public static String productImageLarge = "//div[@class='fancybox-outer']/div/img";
    
   

}
