package onedFullStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ODFS_YourImagesAndProofsPage extends BasePage {


    @FindBy(how = How.XPATH, using = "//a[contains(.,'Your Images')]")
    private WebElement tabYourImages;

    @FindBy(how = How.XPATH, using = "//a[contains(.,'Your Proofs')]")
    private WebElement tabYourProofs;

    @FindBy(how = How.XPATH, using = "//select[@name='customer_images_table_length']")
    private WebElement ddlTableEntriesNumber;

    @FindBy(how = How.XPATH, using = "//input[@type='search' and @aria-controls='customer_images_table']")
    private WebElement txtSearch;

    @FindBy(how = How.XPATH, using = "//li[@id='customer_images_table_previous']/a[contains(.,'Previous')]")
    private WebElement btnPrevious;

    @FindBy(how = How.XPATH, using = "//li[@id='customer_images_table_next']/a[contains(.,'Next')]")
    private WebElement btnNext;


    public ODFS_YourImagesAndProofsPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }


    public ODFS_YourImagesAndProofsPage clickYourImages(){
        clickWhenReady(tabYourImages);
        return this;
    }

    public ODFS_YourImagesAndProofsPage clickYourProofs(){
        clickWhenReady(tabYourProofs);
        return this;
    }

    public ODFS_YourImagesAndProofsPage selectNumberOfEntries(String value){
        chooseValueFromSelect(ddlTableEntriesNumber, value);
        return this;
    }

    public ODFS_YourImagesAndProofsPage enterSearchTerm(String term){
        typeIn(txtSearch, term);
        return this;
    }

    public ODFS_YourImagesAndProofsPage clickPrevious(){
        clickWhenReady(btnPrevious);
        return this;
    }

    public ODFS_YourImagesAndProofsPage clickNext(){
        clickWhenReady(btnNext);
        return this;
    }
}
