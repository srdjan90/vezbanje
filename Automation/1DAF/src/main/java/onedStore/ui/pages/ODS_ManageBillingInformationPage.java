/***************************************************************************************************
 Created by vskyba on 9/27/18 6:39 PM
 **************************************************************************************************/

package onedStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ODS_ManageBillingInformationPage extends BasePage {

    @FindBy(css = "#sel_billing_country") private WebElement ddlCountry;
    @FindBy(css = "#txt_billing_firstname") private WebElement txtFirstName;
    @FindBy(css = "#txt_billing_lastname") private WebElement txtLastName;
    @FindBy(css = "#txt_billing_company") private WebElement txtCompany;
    @FindBy(css = "#txt_billing_address1") private WebElement txtAddress1;
    @FindBy(css = "#txt_billing_address2") private WebElement txtAddress2;
    @FindBy(css = "#txt_billing_address3") private WebElement txtAddress3;
    @FindBy(css = "#txt_billing_city") private WebElement txtCity;
    @FindBy(css = "#sel_billing_state") private WebElement ddlState;
    @FindBy(css = "#txt_billing_zip") private WebElement txtZip;
    @FindBy(css = "#txt_billing_phone") private WebElement txtPhone;
    @FindBy(css = "[value='Save Changes']") private WebElement btnSaveChanges;
    @FindBy(css = "a.btn") private WebElement btnCancel;

    public ODS_ManageBillingInformationPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }


//    Module methods

    private ODS_ManageBillingInformationPage selectCountry(String country) {
        Select countryDropDown = new Select(ddlCountry);
        countryDropDown.selectByValue(country);
        return this;
    }

    private ODS_ManageBillingInformationPage typeFirstName(String inputText) {
        typeIn(txtFirstName, inputText);
        return this;
    }

    private ODS_ManageBillingInformationPage typeLastName(String inputText) {
        typeIn(txtLastName, inputText);
        return this;
    }

    private ODS_ManageBillingInformationPage typeCompany(String inputText) {
        typeIn(txtCompany, inputText);
        return this;
    }

    private ODS_ManageBillingInformationPage typeAddress1(String inputText) {
        typeIn(txtAddress1, inputText);
        return this;
    }

    private ODS_ManageBillingInformationPage typeAddress2(String inputText) {
        typeIn(txtAddress2, inputText);
        return this;
    }

    private ODS_ManageBillingInformationPage typeAddress3(String inputText) {
        typeIn(txtAddress3, inputText);
        return this;
    }

    private ODS_ManageBillingInformationPage typeCity(String inputText) {
        typeIn(txtCity, inputText);
        return this;
    }

    private ODS_ManageBillingInformationPage selectState(String state) {
        Select stateDropDown = new Select(ddlState);
        stateDropDown.selectByValue(state);
        return this;
    }

    private ODS_ManageBillingInformationPage typeZip(String inputText) {
        typeIn(txtZip, inputText);
        return this;
    }

    private ODS_ManageBillingInformationPage typePhone(String inputText) {
        typeIn(txtPhone, inputText);
        return this;
    }

    private ODS_ManageBillingInformationPage clickSaveChanges() {
        clickWhenReady(btnSaveChanges);
        return new ODS_ManageBillingInformationPage(driver, wait);
    }

    private ODS_ManageBillingInformationPage clickCancel() {
        clickWhenReady(btnCancel);
        return new ODS_ManageBillingInformationPage(driver, wait);
    }

//    Methods

    public ODS_ManageBillingInformationPage changeContactInformation(String country, String firstName, String lastName, String company, String address1, String address2,
            String address3, String city, String state, String zip, String phone) {
        selectCountry(country).typeFirstName(firstName)
                              .typeLastName(lastName)
                              .typeCompany(company)
                              .typeAddress1(address1)
                              .typeAddress2(address2)
                              .typeAddress3(address3)
                              .typeCity(city)
                              .selectState(state)
                              .typeZip(zip)
                              .typePhone(phone)
                              .clickSaveChanges();
        return this;
    }

}
