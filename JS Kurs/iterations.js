
// for (i = 0; i < 10; i++) {
//     console.log(i);
// }


/*

let a = [2, 35, 17, 42, 8];
for (i = 0; i < a.length; i++) {
    console.log(a[i]);
}

*/


/*

let a = [2, 35, 17, 42, 8];
for (let b = 0; b < a.length; b++) {
    const c = a[b];
    console.log(c);
}

*/


/*

let x = 1;
while (x < 10) {
    console.log(x++);
}

*/



let x = 1;
while (x < 10) {
    console.log(x++);

    if (x == 7) break;      // if we have only 1 line of code

    /*
    if ( x == 7) {          // This works like this above.
        break;
    }
    */
}