/*
 * Created by ekarliuchenko on 10/8/18 9:21 PM
 */

package onedFullStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class ODFS_ChangeYourPasswordPage extends BasePage {

    @FindBy(css = "#existingPassword") private WebElement txtExistingPassword;
    @FindBy(css = "#newPassword") private WebElement txtNewPassword;
    @FindBy(css = "#confirmPassword") private WebElement txtConfirmPassword;
    @FindBy(css = "#form_profile input.btn-primary") private WebElement btnSaveChanges;
    @FindBy(css = "a.btn-default") private WebElement btnCancel;
    @FindBy(css = "#newPassword-error") private WebElement txtNewPasswordError;


    @FindBy(css = ".bootbox-body") private WebElement msgbPasswordChangeMessage;
    @FindBy(css = ".modal-footer .btn-success") private WebElement btnBackToMyAcc;
    @FindBy(css = ".modal-footer .btn-default") private WebElement btnClose;
    @FindBy(css = ".modal-header .bootbox-close-button") private WebElement btnCloseXtype;
    @FindBy(css = ".modal-footer button[data-bb-handler='ok']") private WebElement btnOk;

    @FindBy(how = How.XPATH, using = "//a[contains(.,'Your Account')]")
    private WebElement linkYourAccount;
    @FindBy(how = How.XPATH, using = "//a[contains(.,'Home')]")
    private WebElement linkHome;
    @FindBy(how = How.XPATH, using = "//input[@id='existingPassword']")
    private WebElement txtCurrentPassword;
    @FindBy(how = How.XPATH, using = "//div[contains(.,'Changes to your account have not been saved.')]")
    private WebElement messageOnPopUp;
    @FindBy(how = How.XPATH, using = "//button[@class='bootbox-close-button close' and contains(.,'×')]")
    private WebElement btnClosePopUp;
    @FindBy(how = How.XPATH, using = "//button[@class='btn btn-primary' and contains(.,'OK')]")
    private WebElement btnClosePopUp2;



    public ODFS_ChangeYourPasswordPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    private ODFS_ChangeYourPasswordPage enterCurrentPassword(String currentPassword) {
        typeIn(txtExistingPassword, currentPassword);
        return this;
    }

    private ODFS_ChangeYourPasswordPage enterNewPassword(String newPassword) {
        typeIn(txtNewPassword, newPassword);
        return this;
    }

    private ODFS_ChangeYourPasswordPage enterConfirmPassword(String confirmNewPassword) {
        typeIn(txtConfirmPassword, confirmNewPassword);
        return this;
    }

    private ODFS_ChangeYourPasswordPage saveChanges() {
        waitForSpinners();
        clickWhenReady(btnSaveChanges);
        return this;
    }

    public ODFS_MyAccountPage cancel() {
        clickWhenReady(btnCancel);
        return new ODFS_MyAccountPage(driver, wait);
    }

    public ODFS_MyAccountPage clickBackToMyAccount() {
        clickWhenReady(btnBackToMyAcc);
        return new ODFS_MyAccountPage(driver, wait);
    }

    public ODFS_ChangeYourPasswordPage closePopUp(){
        waitForModals();
        clickWhenReady(btnClosePopUp);
        waitForSpinners();
        return this;
    }

    public ODFS_ChangeYourPasswordPage closePopUp2(){
        clickWhenReady(btnClosePopUp2);
        return this;
    }

    public ODFS_ChangeYourPasswordPage waitForInvisibilityOfModalWindow () {
        WebElement element = driver.findElement(By.xpath("//div[@class='modal-backdrop fade']"));
        wait.until(ExpectedConditions.invisibilityOf(element));
        return this;
    }

    public ODFS_MyAccountPage changePassword(String existingPassword, String newPassword) {
        enterCurrentPassword(existingPassword).
                enterNewPassword(newPassword).
                enterConfirmPassword(newPassword).
                saveChanges().
                clickBackToMyAccount();
        return new ODFS_MyAccountPage(driver, wait);
    }

    public ODFS_MyAccountPage clickYourAccountLink () {
        clickWhenReady(linkYourAccount);
        return new ODFS_MyAccountPage(driver, wait);
    }

    public ODFS_HomePage clickHomeLink () {
        clickWhenReady(linkHome);
        return new ODFS_HomePage(driver, wait);
    }

    public ODFS_ChangeYourPasswordPage verifyErrorMessageIvalidPassword () {
        if (!getTextFromElement(txtNewPasswordError).contains("Please enter a password with between 8 and 30 characters, at least one capital letter, one special character and one number.")) {
            Assert.fail("Error message isn't appeared after entering invalid password.");
        }
        return this;
    }

    public ODFS_ChangeYourPasswordPage saveChangesWithEmptyFields () {
        txtCurrentPassword.clear();
        txtNewPassword.clear();
        txtConfirmPassword.clear();
        saveChanges();
        if (!txtCurrentPassword.getAttribute("class").contains("error")) {
            System.out.println("Current Password polje nema error u klasi");
        }
        if (!txtNewPassword.getAttribute("class").contains("error")) {
            System.out.println("New Password polje nema error u klasi");
        }
        if (!txtConfirmPassword.getAttribute("class").contains("error")) {
            System.out.println("Confirm Password polje nema error u klasi");
        }
        return this;
    }

    public ODFS_ChangeYourPasswordPage verifyPopUp(){
        waitForModals();
        if(!getTextFromElement(messageOnPopUp).contains("Provided credentials could not be authenticated. Changes to your account have not been saved.")){
            Assert.fail("Error message isn't appeared after entering invalid password.");
        }
        return this;
    }

    public ODFS_ChangeYourPasswordPage invalidCurrentPassword (String invalidPassword, String newPassword, String confirmPassword) {
        enterCurrentPassword(invalidPassword)
                .enterNewPassword(newPassword)
                .enterConfirmPassword(confirmPassword)
                .saveChanges()
                .verifyPopUp()
                .closePopUp()
                .waitForInvisibilityOfModalWindow()
                .saveChanges()
                .verifyPopUp().closePopUp2();
        return this;
    }

    // waitFor1Second ne bi trebalo da se koristi!

    public ODFS_ChangeYourPasswordPage waitFor1Second () {
    try {
        Thread.sleep(1000);
    } catch (InterruptedException e) {
    }
    return this;
    }

    public ODFS_ChangeYourPasswordPage invalidNewPassword (String validCurrentPassword, String invalidNewPassword) {
        this.txtCurrentPassword.clear();
        this.enterCurrentPassword(validCurrentPassword);
        this.enterNewPassword(invalidNewPassword)
                .verifyErrorMessageIvalidPassword();
        return this;
    }

    public ODFS_ChangeYourPasswordPage invalidConfirmPassword (String validCurrentPassword, String validNewPassword, String invalidConfirmPassword) {
        this.txtCurrentPassword.clear();
        this.txtNewPassword.clear();
        this.txtConfirmPassword.clear();
        this.enterCurrentPassword(validCurrentPassword);
        this.enterNewPassword(validNewPassword);
        this.enterConfirmPassword(invalidConfirmPassword);
        if (txtConfirmPassword.getAttribute("class").contains("error")) {
            System.out.println("Confirm Password polje nema error u klasi");
        }
        return this;
    }
}
