/***************************************************************************************************
 Created by vskyba on 10/9/18 4:07 PM
 **************************************************************************************************/

package utils;

import businessObjects.Product;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.IOException;
import java.util.ArrayList;

import static utils.POIUtils.getCellValueAsString;

public class ProductReader {

    /*
     OBJECTS STRUCTURE

     ProductComplex = {
                         Product product = {
                                             String productId;
                                             String productNumber;
                                             String productName;
                                             String vendorId;
                                             String productDescription;
                                             String productKeywords;
                                             String isProduction;
                                             String productNameSEO;
                                             String vendorProductURL;
                                             String vendorSKU;
                                             String isProductActive;
                                             String absoluteMinimum;
                                             String options;
                                             String defaultSKU;
                                             String productTaxCode;
                                             String productOrigin;
                                             String productType;
                                             String weight;
                                             String standardProductionTime;
                                             String priceIncludesDescription;
                                             String imprintAreaDescription;
                                             String isIndexable;
                                             String minimumQuantity;
                                             String maximumQuantity;
                                          }
                         ArrayList<SkuComplex> skus = {
                                                         Sku sku = {
                                                                     String productId;
                                                                     String skuId;
                                                                     String skuName;
                                                                     String skuDescription;
                                                                     String isActive;
                                                                     String skuAttribute;
                                                                     String options;
                                                                     String defaultImpritnOption;
                                                                     String pricingSheme;
                                                                     String priceType;
                                                                     String inBetween;
                                                                     String imageId;
                                                                     String skipFrom;
                                                                     String designId;
                                                                     String themeId;
                                                                     String skuWeight;
                                                                   }
                                                         ArrayList<Option> options = {
                                                                                         String optionId;
                                                                                         String optionName;
                                                                                         String optionValue;
                                                                                         String groupId;
                                                                                         String groupName;
                                                                                         String extraCharge;
                                                                                         String groupType;
                                                                                         String parentOpitonId;
                                                                                         String childOption;
                                                                                    }
                                                         ArrayList<Price> prices = {
                                                                                     String SkuId;
                                                                                     String qty;
                                                                                     String qtyPrice;
                                                                                     String priceFlat;
                                                                                     String priceRun;
                                                                                    }
                    }
           }
     */

    public static final String PRODUCTS_DATA_FILE_PATH = ".//src/main/resources/test_data/ProductTestingTemplate.xlsx";

    private static ArrayList<Product> productsList = new ArrayList<>();


    /**
     * @return List of {@link Product}
     */
    public static ArrayList<Product> extractProducts() {
        long startTime = System.nanoTime();
        ArrayList<Product> productsList = new ArrayList<Product>();
        Workbook wb = null;
        try {
            wb = POIUtils.getWorkBook(PRODUCTS_DATA_FILE_PATH);
            Sheet productsSheet = wb.getSheetAt(0);
            for (Row row : productsSheet) {
                if (row == null || row.getRowNum() == 0) {
                    System.out.println("Skipping the first fow in the table OR the entire row is empty");
                    continue;
                }
                Product product = new Product(
                        getCellValueAsString(row.getCell(0)),   //productID
                        getCellValueAsString(row.getCell(1)),   //productNumber
                        getCellValueAsString(row.getCell(2)),   //productName
                        getCellValueAsString(row.getCell(3)),   //vendorId
                        getCellValueAsString(row.getCell(4)),   //productDesc
                        getCellValueAsString(row.getCell(5)),   //productKeywords
                        getCellValueAsString(row.getCell(6)),   //isProduction
                        getCellValueAsString(row.getCell(7)),   //productNameSeo
                        getCellValueAsString(row.getCell(8)),   //vendorProductUrl
                        getCellValueAsString(row.getCell(9)),   //vendorSku
                        getCellValueAsString(row.getCell(10)),  //isProductActive
                        getCellValueAsString(row.getCell(11)),  //absoluteMinimum
                        getCellValueAsString(row.getCell(12)),  //options
                        getCellValueAsString(row.getCell(13)),  //defaultSku
                        getCellValueAsString(row.getCell(14)),  //productTaxCode
                        getCellValueAsString(row.getCell(15)),  //productOrigin
                        getCellValueAsString(row.getCell(16)),  //productType
                        getCellValueAsString(row.getCell(17)),  //weight
                        getCellValueAsString(row.getCell(18)),  //standardProductionTime
                        getCellValueAsString(row.getCell(19)),  //priceIncludesDescription
                        getCellValueAsString(row.getCell(20)),  //imprintAreaDescription
                        getCellValueAsString(row.getCell(21)),  //isIndexable
                        getCellValueAsString(row.getCell(22)),  //minimumQty
                        getCellValueAsString(row.getCell(23))   //maximumQty
                );
                productsList.add(product);
            }
        } finally {
            if (wb != null) {
                try {
                    wb.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        long endTime = System.nanoTime();
        long duration = (endTime - startTime) / 1000000000;
        System.out.println("Collecting Product data took " + duration + " seconds.");

        return productsList;
    }

}