
let car = {
    make: 'bmw',
    model: '745li',
    year: 2010,
    getPrice: function () {     // methoda
        // perform some calculating
        return 5000;
    },
    printDescription: function () {        //methoda
        console.log(this.make + ' ' + this.model);
    }
}

car.printDescription();
console.log(car.year);
// console.log(car['year']);  // same as this above just a bit different, but the first case is more better!


// var anotherCar = {}
// anotherCar.whatever = 'srki';   // adding properties into object like this
// console.log(anotherCar.whatever);


// let a = {
//     myProperty: {
//         b: 'Hi',
//     },
// };
// console.log(a.myProperty.b);

/*

let c = {
    myProperty: [
        {d: 'this' },
        {e: 'can' },
        {f: 'get' },
        {g: 'crazy' },        
    ]
};

*/



