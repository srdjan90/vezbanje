package onedFullStore.ui.pages;

import logic.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class ODFS_ReviewsPage extends BasePage {

    @FindBy(how = How.XPATH, using = "//a[contains(.,'Continue Shopping')]")
    private WebElement btnContinueShopping;

    @FindAll(@FindBy(how = How.XPATH, using = "//div[@class='container-fluid container-content']/div[@class='row']"))
    private List<WebElement> listProductsList;


    public ODFS_ReviewsPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    public ODFS_ProductPage clickProductOnProductNameLink(String productName){
        for(WebElement w : listProductsList){
            if(getTextFromElement(w).contains(productName)){
                clickWhenReady(w.findElement(By.xpath("div/h4/a")));
                break;
            }
        }
        return new ODFS_ProductPage(driver, wait);
    }

    public ODFS_ReviewProductsPage clickWriteAReview(String productName){
        for(WebElement w : listProductsList){
            if(getTextFromElement(w).contains(productName)){
                clickWhenReady(w.findElement(By.xpath("div/a[contains(@href,'reviewproduct')]")));
                break;
            }
        }
        return new ODFS_ReviewProductsPage(driver, wait);
    }

    public ODFS_HomePage clickContinueShopping(){
        clickWhenReady(btnContinueShopping);
        return new ODFS_HomePage(driver, wait);
    }
}
