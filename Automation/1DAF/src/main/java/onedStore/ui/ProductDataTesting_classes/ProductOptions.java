package onedStore.ui.ProductDataTesting_classes;

public class ProductOptions {

    private String productId;
    private String productName;
    private String productNumber;
    private String groupName;
    private String optionId;
    private String optionName;

    public ProductOptions(String productId, String productName, String productNumber, String groupName, String optionId, String optionName) {
        this.productId = productId;
        this.productName = productName;
        this.productNumber = productNumber;
        this.groupName = groupName;
        this.optionId = optionId;
        this.optionName = optionName;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getOptionId() {
        return optionId;
    }

    public void setOptionId(String optionId) {
        this.optionId = optionId;
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }
}
