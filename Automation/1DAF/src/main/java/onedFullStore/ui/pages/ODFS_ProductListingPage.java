package onedFullStore.ui.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.WebDriverWait;

import logic.BasePage;

public class ODFS_ProductListingPage extends BasePage {
    
    @FindBy(css = ".resultsImage img") private List<WebElement> lstProductsImages;
    
    @FindAll(@FindBy(className = "resultsGrid"))
    private List<WebElement> items;

    
    public ODFS_ProductListingPage(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }
    
    public ODFS_ProductPage clickOnProductImgByOrdinalValue(int ordinal) {
        clickWhenReady(lstProductsImages.get(ordinal));
    		return new ODFS_ProductPage (driver, wait);
    	}

    public ODFS_ProductPage clickProductByProductId(String productId) {
    	items.stream().filter(item -> item.findElement(By.xpath(".//span[@class='productID']")).getText().equals(productId))
		    	  .findFirst()
		    	  .ifPresent(WebElement::click);
    	return new ODFS_ProductPage (driver, wait);
    }
    
    public ODFS_ProductListingPage clickAddToWhishListByProductId(String productId) {
    	items.stream().filter(item -> item.findElement(By.xpath(".//span[@class='productID']"))
					.getText().equals(productId))
					.findFirst().get()
					.findElement(By.xpath(".//a[contains(@id, 'button-wishlist')]")).click();
    	return this;
    }
    
    public ODFS_ProductListingPage clickCompareProductByProductId(String productId) {
    	items.stream().filter(item -> item.findElement(By.xpath(".//span[@class='productID']"))
					.getText().equals(productId))
					.findFirst().get()
					.findElement(By.xpath(".//button[contains(@onclick, 'AddToComparisons')]")).click();
    	return this;
    }
    
}
