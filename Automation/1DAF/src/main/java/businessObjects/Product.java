/***************************************************************************************************
 Created by vskyba on 10/9/18 4:10 PM
 **************************************************************************************************/

package businessObjects;

public class Product {

    private String productId;
    private String productNumber;
    private String productName;
    private String vendorId;
    private String productDescription;
    private String productKeywords;
    private String isProduction;
    private String productNameSEO;
    private String vendorProductURL;
    private String vendorSKU;
    private String isProductActive;
    private String absoluteMinimum;
    private String options;
    private String defaultSKU;
    private String productTaxCode;
    private String productOrigin;
    private String productType;
    private String weight;
    private String standardProductionTime;
    private String priceIncludesDescription;
    private String imprintAreaDescription;
    private String isIndexable;
    private String minimumQuantity;
    private String maximumQuantity;

    public Product() {

    }

    public Product(String productId, String productNumber, String productName, String vendorId, String productDescription, String productKeywords, String isProduction, String productNameSEO, String vendorProductURL, String vendorSKU, String isProductActive, String absoluteMinimum, String options, String defaultSKU, String productTaxCode, String productOrigin, String productType, String weight, String standardProductionTime, String priceIncludesDescription, String imprintAreaDescription, String isIndexable, String minimumQuantity, String maximumQuantity) {
        this.productId = productId;
        this.productNumber = productNumber;
        this.productName = productName;
        this.vendorId = vendorId;
        this.productDescription = productDescription;
        this.productKeywords = productKeywords;
        this.isProduction = isProduction;
        this.productNameSEO = productNameSEO;
        this.vendorProductURL = vendorProductURL;
        this.vendorSKU = vendorSKU;
        this.isProductActive = isProductActive;
        this.absoluteMinimum = absoluteMinimum;
        this.options = options;
        this.defaultSKU = defaultSKU;
        this.productTaxCode = productTaxCode;
        this.productOrigin = productOrigin;
        this.productType = productType;
        this.weight = weight;
        this.standardProductionTime = standardProductionTime;
        this.priceIncludesDescription = priceIncludesDescription;
        this.imprintAreaDescription = imprintAreaDescription;
        this.isIndexable = isIndexable;
        this.minimumQuantity = minimumQuantity;
        this.maximumQuantity = maximumQuantity;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductNumber() {
        return productNumber;
    }

    public void setProductNumber(String productNumber) {
        this.productNumber = productNumber;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getProductKeywords() {
        return productKeywords;
    }

    public void setProductKeywords(String productKeywords) {
        this.productKeywords = productKeywords;
    }

    public String getIsProduction() {
        return isProduction;
    }

    public void setIsProduction(String isProduction) {
        this.isProduction = isProduction;
    }

    public String getProductNameSEO() {
        return productNameSEO;
    }

    public void setProductNameSEO(String productNameSEO) {
        this.productNameSEO = productNameSEO;
    }

    public String getVendorProductURL() {
        return vendorProductURL;
    }

    public void setVendorProductURL(String vendorProductURL) {
        this.vendorProductURL = vendorProductURL;
    }

    public String getVendorSKU() {
        return vendorSKU;
    }

    public void setVendorSKU(String vendorSKU) {
        this.vendorSKU = vendorSKU;
    }

    public String getIsProductActive() {
        return isProductActive;
    }

    public void setIsProductActive(String isProductActive) {
        this.isProductActive = isProductActive;
    }

    public String getAbsoluteMinimum() {
        return absoluteMinimum;
    }

    public void setAbsoluteMinimum(String absoluteMinimum) {
        this.absoluteMinimum = absoluteMinimum;
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getDefaultSKU() {
        return defaultSKU;
    }

    public void setDefaultSKU(String defaultSKU) {
        this.defaultSKU = defaultSKU;
    }

    public String getProductTaxCode() {
        return productTaxCode;
    }

    public void setProductTaxCode(String productTaxCode) {
        this.productTaxCode = productTaxCode;
    }

    public String getProductOrigin() {
        return productOrigin;
    }

    public void setProductOrigin(String productOrigin) {
        this.productOrigin = productOrigin;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getStandardProductionTime() {
        return standardProductionTime;
    }

    public void setStandardProductionTime(String standardProductionTime) {
        this.standardProductionTime = standardProductionTime;
    }

    public String getPriceIncludesDescription() {
        return priceIncludesDescription;
    }

    public void setPriceIncludesDescription(String priceIncludesDescription) {
        this.priceIncludesDescription = priceIncludesDescription;
    }

    public String getImprintAreaDescription() {
        return imprintAreaDescription;
    }

    public void setImprintAreaDescription(String imprintAreaDescription) {
        this.imprintAreaDescription = imprintAreaDescription;
    }

    public String getIsIndexable() {
        return isIndexable;
    }

    public void setIsIndexable(String isIndexable) {
        this.isIndexable = isIndexable;
    }

    public String getMinimumQuantity() {
        return minimumQuantity;
    }

    public void setMinimumQuantity(String minimumQuantity) {
        this.minimumQuantity = minimumQuantity;
    }

    public String getMaximumQuantity() {
        return maximumQuantity;
    }

    public void setMaximumQuantity(String maximumQuantity) {
        this.maximumQuantity = maximumQuantity;
    }
}
