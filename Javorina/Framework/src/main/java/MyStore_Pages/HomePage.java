package MyStore_Pages;


import Base.Browser;
import Base.UI;
import MyStore_Repository.Repository;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;


public class HomePage {

    // Header
    public static void clickSignInButton() throws IOException{
        UI.clickElement("Sign In button",Repository.signInButton);
    }

    public static void clickContactUsButton() throws IOException{
        UI.clickElement("Contact Us button",Repository.contactUsButton);
    }

    public static void verifyHeaderPhoneNumber() throws Throwable{
        UI.verifyElementContainsPartialText("Header Phone Number", Repository.callUsNowHeader, "Call us now: 0123-456-789");
    }
    
    public static void enterTextIntoSearchField(String valueToEnter) throws IOException {
    	UI.enterTextIntoElement("Search Field", Repository.searchField, valueToEnter);
    }
    
    public static void clickSearchButton() throws IOException {
    	UI.clickElement("Search Button", Repository.searchButton);
    }

    public static void clickWomanMegaLink() throws IOException{
        UI.clickElement("Woman Mega Link", Repository.womanMegaLink);
    }

    public static void hoverWomanMegaLink() throws IOException{
        UI.hoverElement("Woman Mega Link", Repository.womanMegaLink);
    }

    public static void verifyWomanTshirtsSubcategoryLinkIsPresent() throws IOException{
        UI.verifyElementIsPresent("T-Shirts subcategory link", Repository.womanTshirtsSubcategory);
    }

    public static void clickWomanTshirtsSubcategoryLink() throws IOException{
        UI.clickElement("T-Shirts subcategory link", Repository.womanTshirtsSubcategory);
    }

    public static void verifyWomanBlousesSubcategoryLinkIsPresent() throws IOException{
        UI.verifyElementIsPresent("Blouses subcategory link", Repository.womanBlousesSubcategory);
    }

    public static void clickWomanBlousesSubcategoryLink() throws IOException{
        UI.clickElement("Blouses subcategory link", Repository.womanBlousesSubcategory);
    }

    public static void selectFromSortByDropdown(String valueToSelect){
        WebElement element = Browser.driver.findElement(By.xpath(Repository.sortByDropdown));
        UI.selectValueFromDropDown("Sort By Dropdown", element, valueToSelect);
    }
}
