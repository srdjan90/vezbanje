/***************************************************************************************************
 Created by vskyba on 10/9/18 4:12 PM
 **************************************************************************************************/

package utils;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class POIWriter {
    private static int rowNumber = 1;

    private static final String EXCEL_FILE_PATH = ".//src/main/resources/test_data/products_test_results.xlsx";

    private static String[] columns = {"ProductID", "OrderNumber", "TEST STATUS", "PLP IMG", "minQtyPresent", "Price Calculation", "Product name matches", "QtyBtnS work",
                                       "Description", "Add to cart works", "Colors are active"};

    public enum existingCell {
        PRODUCT_ID, ORDER_NUMBER, TEST_STATUS, PLP_IMAGE, QTY, PRICE_CALCULATION, PRODUCT_NAME_MATCHES, QTY_BTNS_WORK, DESCRIPTION, ADD_TO_CART, ACTIVE_COLORS
    }

    public static void createFile() { // Create a Workbook
        Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file
        /* CreationHelper helps us create instances of various things like DataFormat,
           Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */

        CreationHelper createHelper = workbook.getCreationHelper();
        // Create a Sheet
        Sheet sheet = workbook.createSheet("Products");
        // Create a Font for styling header cells
        Font headerFont = workbook.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 14);
        headerFont.setColor(IndexedColors.RED.getIndex());
        // Create a CellStyle with the font
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(headerFont);
        // Create a Row
        Row headerRow = sheet.createRow(0);
        // Create cells
        for (int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }
        // Resize all columns to fit the content size
        for (int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }
        Row newRow = sheet.createRow(1);
        // Write the output to a file
        try {
            FileOutputStream fileOut = new FileOutputStream(EXCEL_FILE_PATH);
            workbook.write(fileOut);
            fileOut.close();

            // Closing the workbook
            workbook.close();
        } catch (IOException ex) {}

    }

    public static void writeToFile(String productID, String orderNumber, String error) {
        Map<Integer, String> params = new HashMap();
        params.put(0, productID);
        params.put(1, orderNumber);
        params.put(2, error);

        try {
            //Read the spreadsheet that needs to be updated
            FileInputStream fsIP = new FileInputStream(new File(EXCEL_FILE_PATH));
//Access the workbook
            XSSFWorkbook workbook = new XSSFWorkbook(fsIP);
//Access the worksheet, so that we can update / modify it.
            XSSFSheet sheet = workbook.getSheetAt(0);
            // Get Sheet at index
            int lastRow = sheet.getLastRowNum();
            // Get Row at index
            Row row = sheet.getRow(rowNumber);
            for (int i = 0; i < params.size(); i++) {
                row.createCell(i)
                   .setCellValue(params.get(i));
            }
            // Resize all columns to fit the content size
            for (int i = 0; i < columns.length; i++) {
                sheet.autoSizeColumn(i);
            }
            rowNumber = lastRow + 1;
            Row newRow = sheet.createRow(rowNumber);
            fsIP.close();
//Open FileOutputStream to write updates
            FileOutputStream output_file = new FileOutputStream(new File(EXCEL_FILE_PATH));
            //write changes
            workbook.write(output_file);
//close the stream
            output_file.close();
        } catch (IOException ex) {}
    }

    public static void writeToExactCell(String text, int cell) {
        try {
            FileInputStream fsIP = new FileInputStream(new File(EXCEL_FILE_PATH));
            XSSFWorkbook workbook = new XSSFWorkbook(fsIP);
            XSSFSheet sheet = workbook.getSheetAt(0);
//             Get Row at index
            Row row = sheet.getRow(rowNumber);
            row.createCell(cell)
               .setCellValue(text);
//             Resize all columns to fit the content size
            for (int i = 0; i < columns.length; i++) {
                sheet.autoSizeColumn(i);
            }
            fsIP.close();
//             Open FileOutputStream to write updates
            FileOutputStream output_file = new FileOutputStream(new File(EXCEL_FILE_PATH));
//             Write changes
            workbook.write(output_file);
//             Close the stream
            output_file.close();
        } catch (IOException ex) {}
    }
}
